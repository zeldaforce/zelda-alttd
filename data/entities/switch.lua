----------------------------------
--
-- Walkable switch allowing a different behavior than the built-in one.
-- The activable part of the switch is defined by its size.
--
----------------------------------

local switch = ...
local game = switch:get_game()
local map = switch:get_map()
local hero = map:get_hero()
local sprite = switch:get_sprite()
local is_activated = false
local is_locked = false

-- Returns whether the switch is walkable.
switch:register_event("is_walkable", function(switch)
  return true
end)

-- Returns whether the switch is activated.
switch:register_event("is_activated", function(switch)
  return is_activated
end)

-- Returns whether the switch is locked.
switch:register_event("is_locked", function(switch)
  return is_locked
end)

-- Lock or unlock the switch.
switch:register_event("set_locked", function(switch, locked)
  is_locked = locked or true
end)

-- Make the switch activated.
switch:register_event("set_activated", function(switch, activated)

  activated = activated or activated == nil
  local is_state_changing = is_activated ~= activated

  if is_state_changing then
    is_activated = activated
    if is_activated then
      sprite:set_animation("activated")
      if switch.on_activated then
        switch:on_activated()
      end
    else
      sprite:set_animation("inactivated")
      if switch.on_inactivated then
        switch:on_inactivated()
      end
    end
  end
end)

-- Initialization.
switch:register_event("on_created", function(switch)

  switch:add_collision_test("origin", function(switch, entity)
    if entity == hero and not is_locked then
      switch:set_activated(true)
    end
  end)

  -- TODO on_left
end)
