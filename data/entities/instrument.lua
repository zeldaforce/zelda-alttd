local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local opacity = 0
local sprite = entity:get_sprite()
local effect_enable = false
local entity_appear = true

-- Event called when the custom entity is initialized.
function entity:on_created()
  
  local num = entity:get_property("num")
  sol.timer.start(map, 800 * num, function()
    effect_enable = true
  end)
 
end

map:register_event("on_draw", function(map, dst_surface)

  if effect_enable then
    sprite:set_opacity(opacity)
    if entity_appear then
      opacity = opacity + 5
      if opacity > 255 then
        opacity = 255
        entity_appear = false
      end
    else
      opacity = opacity - 5
      if opacity <= 0 then
        opacity = 0
        entity_appear = true
      end
    end
  end
end)
