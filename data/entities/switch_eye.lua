-- Variables
local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local sprite = entity:get_sprite()
local state = entity:get_property("state")
local group = entity:get_property("group")
local count_states = tonumber(entity:get_property("states"))
local count_entities = map:get_entities_count(group)
local number = tonumber(entity:get_property("number"))
local lock_entity = false
local grid = {}

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Event called when the custom entity is initialized.
entity:register_event("on_created", function()
  
  entity:set_traversable_by(false)
  entity:build_grid()
  entity:update_sprite()
  entity:add_collision_test("sprite", function(entity, other_entity, entity_sprite, other_entity_sprite)
    if other_entity:get_type()== "hero" and other_entity_sprite == other_entity:get_sprite("sword") and not lock_entity  then
      lock_entity = true
      entity:change_state()
      entity:change_siblings_switchs()
      if entity.on_activated then
        entity:on_activated(entity)
      end
      audio_manager:play_sound("enemies/enemy_hit")
      sol.timer.start(entity, 500, function()
        lock_entity = false
      end)
    else
    end
  end)
  
  function sprite:on_animation_finished(animation)
    if animation == "turning" then
      sprite:set_animation("stopped")
      entity:get_sprite():set_direction(state - 1)
    end
  end

end)

-- Update the entity sprite from state property
 function entity:update_sprite(animation_active)

  if animation_active then
     entity:get_sprite():set_animation("turning")
  else
    entity:get_sprite():set_direction(state - 1)
  end
  
end

-- Change state (increment to 1)
function entity:change_state()
  state = state + 1
  if state > count_states then
    state = 1
  end
  entity:set_property("state", state)
  entity:update_sprite(true)
end

-- Enable all switch eye siblings
function entity:change_siblings_switchs()

  local coordinates = entity:get_coordinates_grid_from_index(number)
  local index_column_prev = entity:get_index_grid_from_coordinates(coordinates["row"], (coordinates["column"] - 1))
  local index_column_next = entity:get_index_grid_from_coordinates(coordinates["row"], (coordinates["column"] + 1))
  local index_row_prev = entity:get_index_grid_from_coordinates((coordinates["row"] - 1), coordinates["column"])
  local index_row_next = entity:get_index_grid_from_coordinates((coordinates["row"] + 1), coordinates["column"])
  if index_column_prev then
    map:get_entity(group .. '_' ..index_column_prev):change_state()
  end
  if index_column_next then
    map:get_entity(group .. '_' ..index_column_next):change_state()
  end
  if index_row_prev then
    map:get_entity(group .. '_' ..index_row_prev):change_state()
  end
  if index_row_next then
    map:get_entity(group .. '_' ..index_row_next):change_state()
  end
  
end

-- Build initial grid
function entity:build_grid()

  for switch in map:get_entities(group) do
    local switch_number = tonumber(switch:get_property("number"))
      local column = math.ceil(switch_number / math.sqrt(count_entities)) - 1
      local row = switch_number - (column * math.sqrt(count_entities)) - 1
      if grid[row] == nil then
        grid[row] = {}
      end
      grid[row][column] = switch_number

  end

end

-- Retrieve coordinates grid from index
function entity:get_coordinates_grid_from_index(index)

   local coordinates = {}
   for row_key, row in pairs(grid) do
       for column_key, column in pairs(row) do
        if index == column then
         coordinates['row'] = row_key
         coordinates['column'] = column_key
        end
      end
   end

  return coordinates

end

-- Retrieve index gid from coordinates
function entity:get_index_grid_from_coordinates(row, column)

  if grid[row] == nil then
     return nil
  end
  if grid[row][column] == nil then
     return nil
  end

  return grid[row][column]

end


-- Check if all switch eye group is activated
function entity:check_if_switch_eye_activated()

  is_enable = true
  for switch in map:get_entities(group) do
    if tonumber(switch:get_property("state")) ~= 1 then
      is_enable = false
    end

  end

  return is_enable

end

