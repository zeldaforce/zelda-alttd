----------------------------------
--
-- Stairs that don't behave like built-in ones. The goal is bascially to not changes the hero status while taken.
-- Just slow down the hero walk and update the layer.
-- The entity direction has to be set from the bottom to the upper part of stairs.
--
-- Method : stairs:get_speed()
--          stairs:set_speed(speed)
--
----------------------------------

local stairs = ...
local game = stairs:get_game()
local map = stairs:get_map()
local hero = game:get_hero()
local direction = stairs:get_direction()
local is_on_stairs = false

-- Configuration variable.
local stairs_speed = stairs:get_property("speed") or 35

-- Get the stairs speed.
function stairs:get_speed()
  return stairs_speed
end

-- Set the stairs speed.
function stairs:set_speed(speed)
  stairs_speed = speed
end

-- Initialize stairs.
function stairs:on_created()

  stairs:set_layer_independent_collisions()
  stairs:set_traversable_by("enemy", false)
  stairs:set_traversable_by("custom_entity", false)
  stairs:set_traversable_by("npc", false)
  stairs:set_traversable_by("bomb", false)
  stairs:set_traversable_by("destructible", false)
  stairs:set_traversable_by("block", false)
  stairs:set_traversable_by("fire", false)
  stairs:set_traversable_by("arrow", false)
  stairs:set_traversable_by("hookshot", false)
  stairs:set_traversable_by("boomerang", false)

  -- Create an invisible other customer entity over the stairs to keep the hero on upper layer.
  local x, y, layer = stairs:get_position()
  local width, height = stairs:get_size()
  local origin_x, origin_y = stairs:get_origin()
  local upper_floor = map:create_custom_entity({
    name = "stairs_upper_floor",
    x = x - origin_x,
    y = y - origin_y,
    layer = layer + 1,
    width = width,
    height = height,
    direction = direction,
    model = ""
  })
  upper_floor:set_modified_ground("traversable")

  -- Handle hero speed and layer during the collision.
  local hero_speed = hero:get_walking_speed()
  stairs:add_collision_test("origin", function(stairs, entity)
    if entity == hero and not is_on_stairs then
      is_on_stairs = true

      -- Update the hero speed.
      hero:set_walking_speed(stairs_speed)
      sol.timer.start(stairs, 10, function()
        local hero_x, hero_y = hero:get_position()
        if not stairs:overlaps(hero_x, hero_y) then
          is_on_stairs = false
          hero:set_walking_speed(hero_speed)
          return
        end
        return true
      end)

      -- Move the hero to the upper floor layer.
      hero:set_layer(layer + 1)
    end
  end)
end
