--[[
Custom crystal block.

unlike the built-in crystal block, this model allows to :
  - jump between two of them when they are raised,
  - Pass through in debug mode (!)

--]]
local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local crystal_block_meta = require("scripts/meta/crystal_block")
require("scripts/multi_events")

local initially_raised
local raised
local sprite

function entity:setup_sprite()
  local suffix = raised and "raised" or "lowered"
  local prefix = initially_raised and "blue" or "orange"
  sprite:set_animation (prefix .. "_" .. suffix)
end

entity:register_event("on_created", function(entity)
  initially_raised = entity:get_property("initially_raised") == "true"
  raised = initially_raised ~= map:get_crystal_state()
  sprite = entity:get_sprite()
  entity:setup_sprite()
  sprite:set_frame(sprite:get_num_frames() - 1)
end)

-- Traversibility test that can be used by various entities, like the hero and enemies.
local function generic_is_traversable_by(crystal_block, other)

  if not crystal_block:is_raised() then
    return true
  end

  if other.ignore_crystal_block then
    return true
  end

  if other.is_on_raised_crystal_block ~= nil and other:is_on_raised_crystal_block() then
    return true
  end

  return false
end

entity:set_traversable_by(function(other) 
  return not raised 
end)

entity:set_traversable_by("bomb", true)
entity:set_traversable_by("camera", true)

entity:set_traversable_by("hero", generic_is_traversable_by)

entity:set_traversable_by("enemy", function(entity, other)
  if other:get_obstacle_behavior()=="flying" then
    return true
  else
    return generic_is_traversable_by(entity, other)
  end
end)

entity:set_traversable_by("custom_entity", function(entity, other)

  local model = other:get_model()

  -- Let arrows, bomb arrows and any carriable entity traverse a raised crystal block.
  if model == "arrow" or model == "bomb_arrow" or other.throw then
    return true
  else
    return generic_is_traversable_by(entity, other)
  end
end)

entity:add_collision_test("overlapping", function(entity, other)

  if not entity:is_raised() then
    return
  end

  if other:get_type() == "hero" then
    entity:check_falling(other)
  end
end)

function entity:is_raised()
  return raised
end

function entity:notity_other_left(other)
  --TODO allow to play a custom animation
end

function entity:switch()
  raised = not raised
  entity:setup_sprite()
end

function entity:notify_crystal_state_changed()
  self:switch()
end

local function try_jump(hero, candidate_x, candidate_y, jump_direction8, jump_distance)

  local hero_x, hero_y, hero_width, hero_height = hero:get_bounding_box()
  if hero:test_obstacles(candidate_x - hero_x, candidate_y - hero_y) then
    -- Don't fall when it would lead to an obstacle.
    return false
  end

  if hero:get_map():is_rectangle_on_raised_crystal_block(candidate_x, candidate_y, hero_width, hero_height) then
    -- Don't fall, there is another crystal block nearby.
    return false
  end

  hero:start_jumping(jump_direction8, jump_distance)
  hero:set_animation("stopped")
  return true
end

-- Makes the hero fall from this crystal block if he is too close to the bounds.
function entity:check_falling(hero)

  -- Similar to the engine code of built-in crystal blocks.
  local can_fall_in_states = {
    ["carrying"] = true,
    ["free"] = true,
    ["sword loading"] = true,
    ["running"] = true,
    ["custom"] = true,
  }
  local state = hero:get_state()
  if can_fall_in_states[state] == nil then
    return
  end

  if state == "custom" then
    local cstate = hero:get_state_object()
    if not cstate:get_can_control_movement() or not cstate:is_gravity_enabled() then
      return
    end
  end

  local x1, y1, width, height = entity:get_bounding_box()
  local x2, y2 = x1 + width, y1 + height
  local hero_x, hero_y, hero_width, hero_height = hero:get_bounding_box()
  local candidate_x, candidate_y = hero_x, hero_y
  local jump_direction8 = 0
  local jump_distance = 0
  local jumped = false

  if hero_y + hero_height / 2 < y1 then
    -- Fall to the North.
    candidate_y = y1 - hero_height
    jump_direction8 = 2
    jump_distance = hero_y + hero_height - y1
    jumped = try_jump(hero, candidate_x, candidate_y, jump_direction8, jump_distance)
  elseif hero_y + hero_height / 2 >= y2 then
    -- Fall to the South.
    candidate_y = y2
    jump_direction8 = 6
    jump_distance = y2 - hero_y
    jumped = try_jump(hero, candidate_x, candidate_y, jump_direction8, jump_distance)
  end

  if jumped then
    return
  end

  if hero_x + hero_width / 2 >= x2 then
    -- Fall to the East.
    candidate_x = x2
    jump_direction8 = 0
    jump_distance = x2 - hero_x
    try_jump(hero, candidate_x, candidate_y, jump_direction8, jump_distance)
  elseif hero_x + hero_width / 2 < x1 then
    -- Fall to the West.
    candidate_x = x1 - hero_width
    jump_direction8 = 4
    jump_distance = hero_x + hero_width - x1
    try_jump(hero, candidate_x, candidate_y, jump_direction8, jump_distance)
  end
end
