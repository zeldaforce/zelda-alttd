----------------------------------
--
-- Spike that also hurts but allow to be traversed.
--
-- Custom properties : damage
--
----------------------------------

local spike = ...
local map = spike:get_map()
local hero = map:get_hero()

-- Configuration variables.
local damage = tonumber(spike:get_property("damage")) or 1

-- Initialization.
spike:register_event("on_created", function(spike)

  spike:set_drawn_in_y_order(false) -- Display the spike as a flat entity.
  spike:add_collision_test("origin", function(spike, entity)
    if entity ~= hero or hero:is_blinking() or hero:is_invincible() or hero:is_jumping() then
      return
    end
    local x, y = hero:get_position()
    local direction4 = hero:get_direction()
    hero:start_hurt(x + (direction4 == 0 and 1 or direction4 == 2 and -1 or 0), y + (direction4 == 1 and -1 or direction4 == 3 and 1 or 0), damage)
  end)
end)
