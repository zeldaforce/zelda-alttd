local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()

  local radius = tonumber(entity:get_property("radius")) or 150
  entity:set_emissive{
      radius = radius,
      color = "255,255,255"
    }

end
