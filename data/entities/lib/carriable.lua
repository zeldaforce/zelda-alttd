----------------------------------
--
-- Undestructible destructible entity, behaving the same way than a liftable built-in destructible except it bounces on obstacle reached instead of breaking.
-- A hit may happen when the entity reaches an obstacle or when the carriable sprite overlaps another entity sprite while the throw is running.
-- An entity can only be hit once in a throw, however the throw will still bonk on obstacles entites without triggering the hit behavior if already triggered.
-- 
-- Methods : carriable:throw(direction)
--
-- Events :  carriable:on_carrying()
--           carriable:on_thrown(direction)
--           carriable:on_throw_finished()
--           carriable:on_bounced(bounce_number)
--           carriable:on_hit(entity)
--           entity:on_hit_by_carriable(carriable)
--
-- Usage : 
-- local my_entity = ...
-- local carriable_behavior = require("entities/lib/carriable")
-- carriable_behavior.apply(my_entity, { --[[ Custom properties --]] } )
--
----------------------------------

local carriable_behavior = {}
local carrying_state = require("scripts/states/carrying.lua")

local default_properties = {

  bounce_distances = {80, 16, 4}, -- Distances for each bounce.
  bounce_durations = {400, 160, 70}, -- Duration for each bounce.
  bounce_heights = {nil, 4, 2}, -- Heights for each bounce. Nil means sprite position.
  bounce_sound = nil, -- Default id of the bouncing sound. Nil means no sound.
  respawn_delay = nil, -- Time before respawn when removed by bad grounds. Nil means no respawn.
  slowdown_ratio = 0.5, -- Speed and distance decrease ratio at each obstacle hit.
  is_bounding_box_collision_sensitive = true, -- Trigger a hit on bounding box collision.
  is_sprite_collision_sensitive = true, -- Trigger a hit on sprite collision.
  is_offensive = true -- True if the carriable has the offensive behavior on thrown, such as hitting enemies or crystals.
}

-- Returns whether there is at least one obstacle in given entities.
local function is_obstacle_in(entities)
  for _, entity in pairs(entities) do
    -- Workaround: No way to get traversable entities, hardcode ones that will have a triggered behavior or have the on_hit_by_carriable event defined.
    local type = entity:get_type()
    if (type == "enemy" and entity:get_attack_consequence("thrown_item") ~= "ignored") or type == "crystal" or entity.on_hit_by_carriable then
      return true
    end
  end
  return false
end

-- Return the value if not nil, else return default.
local function get_existing(value, default)

  if value ~= nil then
    return value
  end
  return default
end

function carriable_behavior.apply(carriable, properties)

  local game = carriable:get_game()
  local map = carriable:get_map()
  local hero = map:get_hero()
  local sprite = carriable:get_sprite()
  local shadow = nil

  -- Add a shadow below the carriable, as an sub entity to not conflict with a possible collision test from outside.
  if not shadow and not map:is_sideview() then
    local x, y, layer = carriable:get_position()
    shadow = map:create_custom_entity({
      direction = 0,
      x = x,
      y = y,
      layer = layer,
      width = 0,
      height = 0,
      sprite = "entities/shadows/shadow"
    })
    shadow:set_weight(-1)
    shadow:set_traversable_by(true)
    shadow:set_drawn_in_y_order(false) -- Display the shadow as a flat entity.
    shadow:bring_to_back()

    -- Make the shadow not visible on lifted and carried.
    carriable:register_event("on_interaction", function(carriable)
      shadow:set_visible(false)
    end)
    carriable:register_event("on_thrown", function(carriable, direction)
      shadow:set_visible(true)
    end)

    -- Propagate a few carriable events to the shadow.
    carriable:register_event("on_position_changed", function(carriable, x, y, layer)
      shadow:set_position(x, y, layer)
    end)
    carriable:register_event("on_removed", function(carriable)
      if shadow:exists() then
        shadow:remove()
      end
    end)
    carriable:register_event("on_enabled", function(carriable)
      shadow:set_enabled()
    end)
    carriable:register_event("on_disabled", function(carriable)
      shadow:set_enabled(false)
    end)
    carriable:register_event("set_visible", function(carriable, visible)
      shadow:set_visible(visible)
    end)
  end

  -- Function to set the main sprite animation if it exists.
  local function set_animation_if_exists(animation)
    if sprite:has_animation(animation) and sprite:get_animation(animation) ~= animation then
      sprite:set_animation(animation)
    end
  end

  -- Function to call hit events, the entity parameter may be nil.
  local function call_hit_events(entity)
    if entity and entity.on_hit_by_carriable then
      entity:on_hit_by_carriable(carriable)
    end
    if carriable.on_hit then
      carriable:on_hit(entity)
    end
  end

  -- Throwing method, define the behavior for the thrown carriable.
  carriable:register_event("throw", function(carriable, direction)

    -- Properties.
    local bounce_distances = properties.bounce_distances or default_properties.bounce_distances
    local bounce_durations = properties.bounce_durations or default_properties.bounce_durations
    local bounce_heights = properties.bounce_heights or default_properties.bounce_heights
    local bounce_sound = properties.bounce_sound or default_properties.bounce_sound
    local respawn_delay = properties.respawn_delay or default_properties.respawn_delay
    local slowdown_ratio = properties.slowdown_ratio or default_properties.slowdown_ratio
    local is_bounding_box_collision_sensitive = get_existing(properties.is_bounding_box_collision_sensitive, default_properties.is_bounding_box_collision_sensitive)
    local is_sprite_collision_sensitive = get_existing(properties.is_sprite_collision_sensitive, default_properties.is_sprite_collision_sensitive)
    local is_offensive = get_existing(properties.is_offensive, default_properties.is_offensive)

    -- Initialize throwing state.
    local num_bounces = #bounce_distances
    local current_bounce = 0
    local is_bounce_movement_starting = true -- True when the carriable is not moving, but about to.
    local dx, dy = math.cos(direction * math.pi / 2), -math.sin(direction * math.pi / 2)
    local _, hero_height = map:get_entity("hero"):get_size()
    local unhittable_entities = {}

    carriable:set_follow_streams(false)
    carriable:set_direction(direction)
    carriable:bring_to_back() -- Workaround : Ensure the created destructible is below a possible invisible entity such as lights, to let it liftable again after thrown.
    sprite:set_xy(0, -hero_height - 6)
    set_animation_if_exists("thrown")

    -- Callback function for bad ground bounce.
    -- Remove the carriable and respawn it after a delay if the property is set.
    local function on_bad_ground_bounce()
      local initial_properties = {
        name = carriable:get_name(),
        model = carriable:get_model(),
        properties = carriable:get_properties(),
        x = carriable.respawn_position.x,
        y = carriable.respawn_position.y,
        layer = carriable.respawn_position.layer, 
        direction = carriable:get_direction(),
        sprite = sprite:get_animation_set(),
        width = 16,
        height = 16}
      carriable:remove()
      if respawn_delay then
        sol.timer.start(map, respawn_delay, function()
          map:create_custom_entity(initial_properties)
        end)
      end
    end

    -- Return whether the entity has not already been hit during this throw.
    local function is_hittable(entity)
      for _, unhittable_entity in pairs(unhittable_entities) do
        if unhittable_entity == entity then
          return false
        end
      end
      return true
    end

    -- Simulate the movement that hasn't been commited yet and returns a table filled with overlapping entities bounding boxes.
    -- Workaround function to know what are obstacle entities reached during movement:on_obstacle_reached()
    local function get_overlapping_entities_on_obstacle_reached(movement)
      local overlapping_entities = {}
      local speed = movement:get_speed()
      local angle = movement:get_angle()
      local movement_x = speed / 100 * math.cos(angle)
      local movement_y = speed / 100 * math.sin(angle)
      local x, y, width, height = carriable:get_max_bounding_box()
      for entity in map:get_entities_in_rectangle(x + movement_x, y + movement_y, width, height) do
        if entity ~= carriable and is_hittable(entity) then
          table.insert(overlapping_entities, entity)
        end
      end
      return overlapping_entities
    end

    -- Returns entites the have a sprite or bounding box collision with the carriable.
    local function get_overlapping_entities()
      local overlapping_entities = {}
      for entity in map:get_entities_in_region(carriable) do
        local is_bounding_box_collision = is_bounding_box_collision_sensitive and carriable:overlaps(entity)
        local is_sprite_collision = is_sprite_collision_sensitive and carriable:overlaps(entity, "sprite")
        if entity ~= carriable and is_hittable(entity) and (is_bounding_box_collision or is_sprite_collision) then
          table.insert(overlapping_entities, entity)
        end
      end
      return overlapping_entities
    end

    -- Reverse throwing direction and slow down all bounces including the current movement.
    local function reverse_direction(slowdown_ratio)
      local movement = carriable:get_movement()
      direction = (direction + 2) % 4
      if movement then 
        local slowed_distances = {} -- New table to not override default properties.
        movement:set_angle(movement:get_angle() + math.pi)
        movement:set_max_distance(movement:get_max_distance() * slowdown_ratio)
        movement:set_speed(movement:get_speed() * slowdown_ratio)
        for _, distance in ipairs(bounce_distances) do
          table.insert(slowed_distances, math.floor(distance * slowdown_ratio))
        end
        bounce_distances = slowed_distances
      end
    end

    -- Trigger entities hit behavior.
    local function hit(entities)
      for _, entity in pairs(entities) do
        if entity and entity:is_enabled() then
          table.insert(unhittable_entities, entity) -- Avoid the entity being hit twice in a throw.

          if is_offensive then
            if entity:get_type() == "enemy" then
              entity:receive_attack_consequence("thrown_item", entity:get_attack_consequence("thrown_item"))
            elseif entity:get_type() == "crystal" then
              map:set_crystal_state(not map:get_crystal_state())
            end
          end

          call_hit_events(entity)
        end
      end
    end

    -- Function called when the carriable has fallen.
    local function finish_bounce()
      carriable:stop_movement()
      carriable:set_follow_streams(true)
      carriable:bring_to_back() -- Workaround : Ensure the created destructible is below a possible invisible entity such as lights, to let it liftable.
      if map:is_sideview() then
        map:stop_gravity(carriable)
      end
      if carriable.on_throw_finished then
        carriable:on_throw_finished() -- Call event
      end
    end

    -- Function to bounce when carriable is thrown.
    local function bounce()
      current_bounce = current_bounce + 1

      -- Start this bounce movement if the previous one ended normally or if the carriable is still moving.
      local function start_bounce_movement(speed, distance)
        if not is_bounce_movement_starting and not carriable:get_movement() then
          return nil
        end

        local movement = sol.movement.create("straight")
        movement:set_angle(direction * math.pi / 2)
        movement:set_speed(speed)
        movement:set_smooth(false)
        if distance then
          movement:set_max_distance(distance)
        end
        function movement:on_finished()
          is_bounce_movement_starting = true -- The movement ended without being stopped by an obstacle or from another script.
        end
        -- Hit on obstacle reached or sprite collision.
        function movement:on_obstacle_reached()
          local entities = get_overlapping_entities_on_obstacle_reached(movement)
          reverse_direction(slowdown_ratio)
          if #entities > 0 then
            hit(entities)
          else 
            call_hit_events(nil) -- Call hit events even if the obstacle is not an entity.
          end
        end
        function movement:on_position_changed(x, y, layer)
          local entities = get_overlapping_entities()
          if #entities > 0 then
            if is_offensive and is_obstacle_in(entities) then -- Only reverse the move if at least one entity is an obstacle.
              reverse_direction(slowdown_ratio)
              hit(entities)
            end
          end
        end
        is_bounce_movement_starting = false
        movement:start(carriable)

        return movement
      end

      local sprite_x, sprite_y = sprite:get_xy()

      -- Start the throw movement and update the height at each frames.
      if not map:is_sideview() then

        -- Initialize parameters for the bounce.
        local t = 0
        local distance = bounce_distances[current_bounce]
        local duration = bounce_durations[current_bounce]
        local height = bounce_heights[current_bounce] or -sprite_y
        local speed = 1000 * distance / duration
        
        -- Start the bounce movement.
        start_bounce_movement(speed, distance)

        -- Function to get the height for each bounce.
        local function current_height()
          local progress = t / duration
          if current_bounce == 1 then
            return 2 * height * (progress ^ 2 - progress) - (height * (1.0 - progress))
          end
          return 4 * height * (progress ^ 2 - progress)
        end

        -- Shift height of the sprite at each instant for the current bounce.
        sol.timer.start(carriable, 10, function()
          t = t + 10
          -- Update shift of sprite.
          if t <= duration then 
            sprite:set_xy(0, current_height())
          -- Stop the timer. Start next bounce or finish bounces. 
          else -- The carriable hits the ground.
            map:ground_collision(carriable, false, bounce_sound, on_bad_ground_bounce)
            -- Check if the carriable still exists.
            if carriable:exists() then
              if current_bounce >= num_bounces then 
                finish_bounce()
                return false
              end
              bounce() -- Start next bounce.
              if carriable.on_bounced then
                carriable:on_bounced(current_bounce) -- Call event
              end
            end
            return false
          end
          return true
        end)
      else

        -- Use gravity instead of shifting the sprite position to simulate the height on sideview maps.
        local x, y = carriable:get_position()
        carriable:set_position(x + sprite_x, y + sprite_y)
        sprite:set_xy(0, 0)
        map:start_gravity(carriable)

        -- Start the bounce movement if thrown to the left or right.
        local direction4 = carriable:get_direction()
        if direction4 == 0 or direction4 == 2 then
          start_bounce_movement(1000 * bounce_distances[current_bounce] / bounce_durations[current_bounce])
        end
    
        -- Set the carriable gravity vspeed.
        if current_bounce == 1 then
          if direction4 == 0 or direction4 == 2 then
            carriable.vspeed = -0.4
          elseif direction4 == 1 then
            carriable.vspeed = -2.0
          end
        else
          carriable.vspeed = bounce_heights[current_bounce] * -0.2
        end

        -- Start the next bounce on touching ground.
        sol.timer.start(carriable, 10, function()
          if carriable.vspeed == nil then
            if current_bounce >= num_bounces then 
              finish_bounce()
              return false
            end
            bounce() -- Start next bounce.
            if carriable.on_bounced then
              carriable:on_bounced(current_bounce) -- Call event
            end
            return false
          end

          return true
        end)
      end
    end

    -- Start the first bounce if the carriable was not immediately removed from outside.
    if carriable:exists() then
      bounce()
    end

    if carriable.on_thrown then
      carriable:on_thrown() -- Call event
    end
  end)

  -- Apply default properties before a possible on_created event is called.
  local x, y, layer = carriable:get_position()
  carriable.respawn_position = {x = x, y = y, layer = layer}
  carriable:set_follow_streams(true)
  carriable:set_drawn_in_y_order()
  carriable:set_weight(0)
  carriable:bring_to_back() -- Workaround : Ensure the created destructible is below a possible invisible entity such as lights, to let it liftable.
  set_animation_if_exists("stopped")

  carriable:set_traversable_by(true)
  carriable:set_can_traverse_ground("deep_water", true)
  carriable:set_can_traverse_ground("grass", true)
  carriable:set_can_traverse_ground("hole", true)
  carriable:set_can_traverse_ground("lava", true)
  carriable:set_can_traverse_ground("low_wall", true)
  carriable:set_can_traverse_ground("prickles", true)
  carriable:set_can_traverse_ground("shallow_water", true)
  carriable:set_can_traverse(true)
  carriable:set_can_traverse("separator", false)

  -- Use gravity on sideview maps.
  if map:is_sideview() then
    map:start_gravity(carriable)
  end

  -- Start a custom lifting on interaction to not destroy the carriable entity and then keep its registered events alive.
  carriable:register_event("on_interaction", function(carriable)
    if game:get_ability("lift") >= carriable:get_weight() and carriable:get_weight() ~= -1 then
      carrying_state.start(hero, carriable, sprite)
      if map:is_sideview() then
        map:stop_gravity(carriable)
      end
      if carriable.on_carrying then
        carriable:on_carrying() -- Call event
      end
    end
  end)
end

return carriable_behavior