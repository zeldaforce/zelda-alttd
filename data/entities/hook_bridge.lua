-- Variables
local entity = ...
local game = entity:get_game()
local map = entity:get_map()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Event called when the custom entity is initialized.
entity:register_event("on_created", function()
  
  entity:set_traversable_by(false)

  local direction = entity:get_sprite():get_direction()
  local other_direction
  if direction == 3 then other_direction = 1
  elseif direction == 1 then other_direction = 3
  end

  entity:add_collision_test("facing", function(entity, other_entity)
    if other_entity:get_type()== "custom_entity" then
      if map:get_hero():get_sprite():get_animation() == "hookshot" and map:get_hero():get_direction() == other_direction then
        local i = 1
        sol.timer.start(map,10,function()
          map:get_entity(entity:get_name()):remove()
          map:set_entities_enabled(entity:get_name().."_deployed_"..i,true)
          sol.timer.start(map,45,function()
            i = i + 1
            map:set_entities_enabled(entity:get_name().."_deployed_"..i,true)
            if i < 10 then return true end
          end)
        end)
      end
    end
  end)

end)