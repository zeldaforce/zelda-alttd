----------------------------------
--
-- Stream entity that behaves like the built-in one but allowing more flexibility (and doesn't crash with custom entities on 1.6.X).
-- The movement starts when the origin of an entity touches the stream, and stops when their whole bounding boxes are not in collision anymore.
-- Each entities can only be controlled by one stream at the same time.
--
-- Method : stream:get_speed()
--          stream:set_speed(speed)
--          stream:get_angle()
--          stream:set_angle(angle)
--
----------------------------------

-- Global variables.
local stream = ...
local game = stream:get_game()
local map = game:get_map()
local hero = game:get_hero()
local camera = map:get_camera()
local trigonometric_functions = { math.cos, math.sin }
local axis_pixel_moves = { { 1, -1 }, { -1, 1 } }
local quarter = 0.5 * math.pi
local circle = 2.0 * math.pi

-- Configuration variables.
local is_inactive_when_out_of_sight = true -- Make only streams over camera as active, necessary optimization on maps with a lot of streams.
local stream_speed = stream:get_property("speed") or 40
local stream_angle = (stream:get_property("angle") or (stream:get_direction() * quarter)) % circle

-- Returns whether the entity is affected by stream.
local function is_affected(entity)
  local type = entity:get_type()
  return type == "hero" and not hero:is_jumping()
    or type == "pickable"
    or type == "destructible"
    or type == "block"
    or type == "bomb"
    or type == "custom_entity" and entity:get_follow_streams()
end

-- Stop moving the entity.
local function stop_moving(entity)

  entity.controlling_stream = nil
  for i = 1, 2 do
    if entity.controlling_stream_timers[i] then
      entity.controlling_stream_timers[i]:stop()
    end
  end
end

-- Start moving the entity, not using built-in movement to be able to start several movements at the same time
local function start_moving(entity)

  if not entity.controlling_stream_timers then
    entity.controlling_stream_timers = {}
  end
  stop_moving(entity)
  entity.controlling_stream = stream

  local function move_on_axis(axis)

    -- Clean timers if the entity was removed from outside or not affected by stream anymore.
    if not entity:exists() or not is_affected(entity) then
      stop_moving(entity)
      return
    end

    -- Stop the movement if the entity and the stream bounding boxes don't overlap anymore, in case something external happened between two calls of this function.
    if not stream:overlaps(entity, "overlapping") then
      stop_moving(entity)
      return
    end
    
    local entity_position = { entity:get_position() }
    local axis_move = { 0, 0 }
    local axis_move_delay = nil

    -- Always move pixel by pixel.
    axis_move[axis] = axis_pixel_moves[axis][trigonometric_functions[axis](stream_angle) > 0 and 1 or 2]
    if axis_move[axis] ~= 0 then

      -- Move the entity.
      if ignore_obstacles or not entity:test_obstacles(axis_move[1], axis_move[2]) then
        entity:set_position(entity_position[1] + axis_move[1], entity_position[2] + axis_move[2], entity_position[3])
      end

      -- Also stop the movement right now if the entity and the stream bounding boxes don't overlap anymore, to let another stream control the hero immediately if needed.
      if not stream:overlaps(entity, "overlapping") then
        stop_moving(entity)
        return
      end
    end

    return true
  end

  -- Start the pixel move schedule.
  for i = 1, 2 do
    local delay = 1000.0 / math.max(0.1, math.min(100, math.abs(stream_speed * trigonometric_functions[i](stream_angle)))) 
    if delay < 10000.0 then -- Workaround: Consider too low timer as a one-axis move

      -- Delay the first move to avoid being ejected immediately when entering by the stream opposite direction.
      entity.controlling_stream_timers[i] = sol.timer.start(stream, delay, function()
        return move_on_axis(i)
      end)
    end
  end
end

-- Get the stream speed.
function stream:get_speed()
  return stream_speed
end

-- Set the stream speed.
function stream:set_speed(speed)
  stream_speed = speed
end

-- Get the stream angle.
function stream:get_angle()
  return stream_angle
end

-- Set the stream angle.
function stream:set_angle(angle)
  stream_angle = angle
end

-- Periodically check if the stream should control an entity.
function stream:on_update()

  -- Workaround: Do this check in the on_update() event instead of a add_collision_test() method because it wouldn't check for immobile entities created after the map starts.
  -- Also when going from one stream to another, the collision test of the second one would be triggered before the movement of the first one actually stops,
  -- and would never be triggered again until the entity moves, causing the second movement to never starts for immobile entities without other tricks.

  if not is_inactive_when_out_of_sight or stream:overlaps(camera:get_bounding_box()) then
    for entity in map:get_entities_in_rectangle(stream:get_bounding_box()) do
      if is_affected(entity) and not entity.controlling_stream and stream:overlaps(entity, "origin") then
        start_moving(entity)
      end
    end
  end
end

-- Initialize the stream.
function stream:on_created()

  stream:set_follow_streams(false)
  stream:set_drawn_in_y_order(false)
end
