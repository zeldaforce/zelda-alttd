----------------------------------
--
-- Flame fountain machine spitting fire to the south.
--
----------------------------------

local flame_fountain = ...
require("scripts/multi_events")

local game = flame_fountain:get_game()
local map = flame_fountain:get_map()
local sprite = flame_fountain:get_sprite()

-- Configuration variables.
local between_flames_duration = 300

-- Initialization.
flame_fountain:register_event("on_created", function()

  flame_fountain:set_traversable_by("hero", false)
  flame_fountain:set_drawn_in_y_order(true)
  sprite:set_animation("default")

  -- Throw a flame periodically.
  sol.timer.start(flame_fountain, between_flames_duration, function()

    local x, y, layer = flame_fountain:get_position()
    map:create_enemy({
      name = "flame",
      x = x,
      y = y,
      layer = layer,
      direction = 3,
      breed = "projectiles/flame",
    })

    sprite:set_animation("throwing", function()
      sprite:set_animation("default")
    end)

    return true
  end)
end)