-- Variables
local entity = ...
local game = entity:get_game()
local hero = game:get_hero()
local map = entity:get_map()
local sprite = entity:get_sprite()
local is_falling = false

-- Include scripts
require("scripts/multi_events")
local audio_manager=require("scripts/audio_manager")
-- Event called when the custom entity is initialized.
entity:register_event("on_created", function()

  entity:set_traversable_by("hero", false)
  entity:set_traversable_by("enemy", false)

end)

entity:add_collision_test("facing", function(crystal, other, crystal_sprite, other_sprite)

  if is_falling == false and other:get_type() =="hero" and hero:get_state() == "custom" and hero:is_running() then
    sprite:set_animation('falling')
    map:start_coroutine(function()
      local options = {
        entities_ignore_suspend = {entity}
      }
      wait(500)
      map:start_gravity(entity)
    end)
  end

end)