----------------------------------
--
-- Raft entity that wait for the hero to control it, then allow to walk on water.
-- Stays on the water if the hero go back to dry land.
--
-- Methods : start_riding()
--
-- Events : raft:on_riding()
--
----------------------------------

local raft = ...
local game = raft:get_game()
local map = raft:get_map()
local hero = game:get_hero()
local is_ridden = false

-- Configuration variables.
local size = { x = 16, y = 16 } -- Size of the walkable part of the raft.

-- Start controlling the raft while the hero is over deep water.
function raft:start_riding()

  if is_ridden then
    return
  end
  is_ridden = true
  hero.ridden_raft = raft -- To handle transition between maps

  -- Clip the position of the raft if the hero is not fully contained in it anymore, as long as the raft is fully on water.
  sol.timer.start(raft, 10, function()
    if not raft:overlaps(hero, "containing") then
      local x, y, width, height = raft:get_bounding_box()
      local hero_x, hero_y, hero_width, hero_height = hero:get_bounding_box()
      local layer = raft:get_layer()

      if hero_x < x then
        x = hero_x
      elseif hero_x + hero_width > x + width then
        x = hero_x + hero_width - width
      end
      if hero_y < y then
        y = hero_y
      elseif hero_y + hero_height > y + height then
        y = hero_y + hero_height - height
      end

      -- Stop controlling the raft if the new position would make at least two corners go out of the water.
      local corner_over_water_count = (map:get_ground(x, y, layer) ~= "deep_water" and not raft:overlaps(x, y) and 1 or 0)
          + (map:get_ground(x + width - 1, y, layer) ~= "deep_water" and not raft:overlaps(x + width - 1, y) and 1 or 0)
          + (map:get_ground(x, y + height - 1, layer) ~= "deep_water" and not raft:overlaps(x, y + height - 1) and 1 or 0)
          + (map:get_ground(x + width - 1, y + height - 1, layer) ~= "deep_water" and not raft:overlaps(x + width - 1, y + height - 1) and 1 or 0)
      if corner_over_water_count > 1 then
        is_ridden = false
        hero.ridden_raft = nil
        return
      end

      raft:set_position(x + size.x * 0.5, y + size.y - 3)
    end

    return true
  end)

  -- Call event.
  if raft.on_riding then
    raft:on_riding()
  end
end

-- Initialize the raft.
function raft:on_created()

  raft:set_modified_ground("traversable")
  raft:set_size(size.x, size.y)
  raft:set_origin(size.x * 0.5, size.y - 3)

  -- Wait for the hero to control the raft.
  raft:add_collision_test("containing", function(raft, entity)
    if entity:get_type() == "hero" then
      raft:start_riding()
    end
  end)
end

-- Use meta to handle transition between maps.
local function initialize_meta()

  if is_raft_meta_initialized then
    return
  end
  local map_meta = sol.main.get_metatable("map")
  local hero_meta = sol.main.get_metatable("hero")

  -- Recreate a raft on the new map if leaving the precedent one while riding it.
  map_meta:register_event("on_started", function(map, destination)
    local hero = map:get_hero()
    if hero.ridden_raft then
      local x, y, layer = hero:get_position()
      local new_raft = map:create_custom_entity({
        model = "raft",
        direction = 0,
        x = x,
        y = y,
        layer = layer,
        width = size.x,
        height = size.y,
        sprite = hero.ridden_raft:get_sprite():get_animation_set()
      })
      new_raft:start_riding()
    end
  end)

  -- Prevent to recreate the raft on manual teleport (such as teleport songs).
  hero_meta:register_event("teleport", function(hero, map_id, destination_name, transition_style)
    hero.ridden_raft = nil
  end)

  is_raft_meta_initialized = true
end
initialize_meta()
