-- Variable
local follower = ...
local game = follower:get_game()
local map = follower:get_map()
local sprite = follower:get_sprite()
local hero = game:get_hero()
local state = "following"

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

local function follow_hero()

  local movement_hero = hero:get_movement()
  local speed = 100
  if movement_hero and movement_hero.get_speed and movement_hero:get_speed() > speed then
    speed = movement_hero:get_speed()
  end
  local movement = sol.movement.create("target")
  movement:set_speed(speed)
  movement:set_ignore_obstacles(true)
  movement:start(follower)
  sprite:set_animation("walking")
  follower:set_state("following")
  follower:set_layer(hero:get_layer())

end

local function stop_walking()
  
  if follower:get_state() ~= "following" then
    return false
  end
  follower:stop_movement()
  sprite:set_animation("stopped")
  follower:set_state("stopped")
    
end

-- Event called when the custom entity is initialized.
follower:register_event("on_created", function()

  follower:set_optimization_distance(0)
  follower:set_drawn_in_y_order(true)
  follower:set_traversable_by(true)
  follower:set_can_traverse_ground("hole", true)
  follower:set_can_traverse_ground("grass", true)
  follower:set_can_traverse_ground("deep_water", true)
  follower:set_can_traverse_ground("shallow_water", true)
  follower:set_can_traverse_ground("ladder", true)
  follower:set_position(hero:get_position())
  follower:get_sprite():set_direction(hero:get_direction())
  follow_hero()

end)

follower:register_event("on_obstacle_reached", function()

  if follower:get_movement():get_ignore_obstacles() then
    return
  end
  sprite:set_animation("stopped")
  follower:stop_movement()

end)

follower:register_event("on_movement_finished", function()

  sprite:set_animation("stopped")
  follower:stop_movement()

end)

follower:register_event("on_position_changed", function()

  if follower:get_state() == "following" and follower:is_very_close_to_hero() then
    stop_walking()
  end

end)

follower:register_event("on_movement_changed", function()

  local movement = follower:get_movement()
  if movement:get_speed() > 0 then
    sprite:set_direction(movement:get_direction4())
  end

end)

-- Get current follower state
function follower:get_state()

  return state
  
end

-- Set current follower state
-- following is default state
function follower:set_state(new_state)

  if new_state == nil then
    new_state = "following"
  end

  state = new_state
  
end

-- Check if follower is so close to hero
function follower:is_very_close_to_hero()

  local distance = follower:get_distance(hero)
  return distance < 24
  
end

-- Create an exclamation symbol near follower
function follower:create_symbol_exclamation(sound)
  
  local map = self:get_map()
  local x, y, layer = self:get_position()
  local sprite = follower:get_sprite()
  y = y - 16
  if sprite:get_direction() == 2 then
    x = x + 16
  else
    x = x - 16
  end
  if sound then
    audio_manager:play_sound("menus/menu_select")
  end
  local symbol = map:create_custom_entity({
    sprite = "entities/symbols/exclamation",
    x = x,
    y = y,
    width = 16,
    height = 16,
    layer = layer + 1,
    direction = 0
  })

  return symbol
  
end  

-- Launch timer on follower
sol.timer.start(follower, 50, function()

  if follower:get_movement() == nil and not follower:is_very_close_to_hero() and follower:get_state() == "stopped" then
    follow_hero()
  end

  return true

end)