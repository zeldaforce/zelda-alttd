-- Global variables
local entity = ...
require("scripts/multi_events")

local game = entity:get_game()
local map = entity:get_map()
local sprite = entity:get_sprite()
local quarter = math.pi * 0.5
local circle = math.pi * 2.0
local jumping_target_x, jumping_target_y, jumping_angle
local is_jumping = false

-- Configuration variables.
local waiting_minimum_duration = 100
local waiting_maximum_duration = 200
local jumping_speed = 100
local jumping_maximum_duration = 350
local jumping_maximum_height = 12

-- Create the shadow sprite below the enemy.
local function create_shadow()

  local shadow = entity:create_sprite("entities/shadows/shadow", "shadow")
  entity:bring_sprite_to_back(shadow)
end

-- Start the entity jump movement.
local function start_jumping()

  sprite:set_animation("landing")
  sol.timer.start(entity, math.random(waiting_minimum_duration, waiting_maximum_duration), function()
    local direction4 = math.random(8)
    local movement = sol.movement.create("straight")
    local x, y = entity:get_position()
    local angle = jumping_angle or math.atan2(y - jumping_target_y, jumping_target_x - x)
    movement:set_speed(jumping_speed)
    movement:set_angle(angle)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)
    movement:set_ignore_suspend(true)
    movement:start(entity)
    sprite:set_animation("jumping")
    sprite:set_direction(angle > quarter and angle < 3.0 * quarter and 2 or 0)

    -- Schedule an update of the sprite vertical offset by frame.
    local elapsed_time = 0
    local jumping_strength = math.random() % 0.5 + 0.5 -- The jump is on a random strength, use the same ratio for height and duration.
    local duration = jumping_maximum_duration * jumping_strength
    local height = jumping_maximum_height * jumping_strength
    sol.timer.start(entity, 10, function()

      elapsed_time = elapsed_time + 10
      if elapsed_time < duration then
        sprite:set_xy(0, -math.sqrt(math.sin(elapsed_time / duration * math.pi)) * height)
        return true
      else
        sprite:set_xy(0, 0)
        movement:stop()
        sprite:set_direction(math.floor(angle / quarter) % 4)
        if is_jumping then
          start_jumping(target_x, target_y)
        else
          sprite:set_animation("landing")
        end
      end
    end)
  end)
end

-- Start a target jumping movement.
entity:register_event("start_target_jumping", function(entity, x, y)

  jumping_angle = nil
  jumping_target_x = x
  jumping_target_y = y
  if not is_jumping then
    is_jumping = true
    start_jumping()
  end
end)

-- Start a straight jumping movement.
entity:register_event("start_straight_jumping", function(entity, angle)

  jumping_angle = angle % circle
  jumping_target_x = nil
  jumping_target_y = nil
  if not is_jumping then
    is_jumping = true
    start_jumping()
  end
end)

-- Let a possible running jump finish, then stop jumping.
entity:register_event("stop_jumping", function(entity, x, y)

  is_jumping = false
end)

-- Initialize the entity.
entity:register_event("on_started", function()

  create_shadow()
end)

-- Initialize the entity.
entity:register_event("on_created", function()

  entity:set_drawn_in_y_order()
end)
