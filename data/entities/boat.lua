local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local fishman = map:get_entity("fishman")

local function update_hero_position()
  local x,y,w,h=entity:get_bounding_box()
  local hx, hy, hw ,hh=hero:get_bounding_box()
  if hx<x+w and hx+hw>x and hy<=y+1 and hy+hh>=y-1 then
    local xx= hero:get_position()
    hero:set_position(xx, y-3)
  end
end

local function update_fishman_position()
  local x,y,w,h=entity:get_bounding_box()
  local fx, fy, fw ,ff=fishman:get_bounding_box()
  if fx<x+w and fx+fw>x and fy<=y+1 and fy+ff>=y-1 then
    local xx= fishman:get_position()
    fishman:set_position(xx, y-3)
  end
end


local function launch_movement(angle)

    local movement = sol.movement.create("straight")
    movement:set_speed(1)
    movement:set_angle(angle)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)
    movement:set_ignore_suspend(true)
    movement:set_max_distance(1)
    movement:start(entity)
    function movement:on_finished()
        angle = angle + math.pi
        launch_movement(angle)
    end
end

function entity:on_created()

  entity:set_traversable_by(false)
  entity:set_can_traverse(false)

  launch_movement(math.pi /2)

end

function entity:on_position_changed()
  update_hero_position()
  if fishman then
    update_fishman_position()
  end
end