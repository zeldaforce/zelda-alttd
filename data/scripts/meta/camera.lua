-- Provides additional camera features for this quest.

-- Variables
local camera_meta = sol.main.get_metatable("camera")
local is_shaking = false

function camera_meta:is_shaking()
  return is_shaking
end

function camera_meta:shake(config, callback)

  local duration = config ~= nil and config.duration or 600
  local amplitude = config ~= nil and config.amplitude or 4
  local speed = config ~= nil and config.speed or 60

  local camera = self
  local map = camera:get_map()
  local hero = map:get_hero()
  local movement

  local shaking_to_right = true
  is_shaking = true

  local function shake_step()

    movement = sol.movement.create("straight")
    movement:set_speed(speed)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)

    -- Determine direction.
    if shaking_to_right then
      movement:set_angle(0)  -- Right.
    else
      movement:set_angle(math.pi)  -- Left.
    end

    -- Max distance.
    movement:set_max_distance(amplitude)

    -- Inverse direction for next time.
    shaking_to_right = not shaking_to_right

    -- Launch the movement and repeat it.
    movement:start(camera, function()
      if is_shaking then
        shake_step()
      end
    end)
  end
  shake_step()

  sol.timer.start(camera, duration, function()
    is_shaking = false
    movement:stop()
    camera:start_tracking(hero)
    if callback ~= nil then
      callback()
    end
  end)
end


-- Shake while following an entity
function camera_meta:dynamic_shake(config, callback)

  local duration = config ~= nil and config.duration or 600
  local amplitude = config ~= nil and config.amplitude or 8
  local entity_to_track = config ~= nil and config.entity or self:get_map():get_hero()

  local camera = self
  local map = camera:get_map()
  local camera_width, camera_height = camera:get_size()

  local shaking_to_right = true
  local offset_x = 0
  local offset_y = 0

  is_shaking = true

  local function clamp(val, min, max)
    return math.max(min, math.min(val, max))
  end

  local function get_region_bounding_box(map,x,y)
    --By default, make the region bounding box match the map's
    local boundary_xmin=0
    local boundary_ymin=0
    local boundary_xmax, boundary_ymax=map:get_size()
    -- Then, shrink bounding box to actual region size by comparing with the separators; if any.
    for e in map:get_entities_in_region(x,y) do
      if e:get_type()=="separator" then
        local separator_x,separator_y, separator_w, separator_h=e:get_bounding_box()
        if separator_w>separator_h then --Horizontal separator
          if y>separator_y then
            boundary_ymin=math.max(boundary_ymin, separator_y+8)
          else
            boundary_ymax=math.min(boundary_ymax, separator_y+8)  
          end
        else
          if x>separator_x then
            boundary_xmin=math.max(boundary_xmin, separator_x+8)
          else
            boundary_xmax=math.min(boundary_xmax, separator_x+8)
          end
        end
      end
    end
    return boundary_xmin, boundary_ymin, boundary_xmax, boundary_ymax
  end
  local region_xmin, region_ymin, region_xmax, region_ymax=get_region_bounding_box(map, entity_to_track:get_position())

  local function shake_step()

    -- Determine the shifting offset (maybe shift according to a given direction in the future?)
    if shaking_to_right then
      offset_x = amplitude/2 -- Right.
      offset_y = amplitude/2 
    else
      offset_x = -amplitude/2 -- Left.
      offset_y = -amplitude/2
    end

    local entity_x, entity_y, entity_w, entity_h=entity_to_track:get_bounding_box()

    camera:set_position(clamp(entity_x+entity_w/2-camera_width/2+offset_x, region_xmin+offset_x, region_xmax-camera_width+offset_y),
                        clamp(entity_y+entity_w/2-camera_height/2+offset_x, region_ymin+offset_y, region_ymax-camera_height+offset_y))

    -- Inverse direction for next time.
    shaking_to_right = not shaking_to_right

    -- Repeat shaking until duration reached.
    if is_shaking then
      return true
    end
  end
  local movement_timer = sol.timer.start(camera, 10, shake_step)

  sol.timer.start(camera, duration, function()
    is_shaking = false
    movement_timer:stop()
    camera:start_tracking(entity_to_track)
    if callback ~= nil then
      callback()
    end
  end)
end

-- Out of bound parts of the map may be displayed during camera shaking effects.
-- Make sure out of bounds parts of the map are displayed with the correct color
-- and not the background color of the tileset.
function camera_meta:on_pre_draw(camera)
  local map = camera:get_map()
  local map_width, map_height = map:get_size()
  local camera_x, camera_y, camera_width, camera_height = camera:get_bounding_box()
  local oob_color = { 0, 0, 0 }
  if camera_x < 0 then
    camera:get_surface():fill_color(oob_color, 0, 0, -camera_x, camera_height)
  end
  if camera_x + camera_width > map_width then
    local x = map_width - camera_x
    camera:get_surface():fill_color(oob_color, x, 0, camera_width - x, camera_height)
  end
  if camera_y < 0 then
    camera:get_surface():fill_color(oob_color, 0, 0, camera_width, -camera_y)
  end
  if camera_y + camera_height > map_height then
    local y = map_height - camera_y
    camera:get_surface():fill_color(oob_color, 0, y, camera_width, camera_height - y)
  end
end

return true
