-- Initialize block behavior specific to this quest.

-- Variables
local game_meta = sol.main.get_metatable("game")
local combo_timer_duration = 50

-- Include scripts
local audio_manager = require("scripts/audio_manager")
require("scripts/sword_states_manager")
require("scripts/multi_events")

function game_meta:get_sword_ability()
  local ab=self:get_ability("sword")
  return ab>0  and ab or self:get_item("sword"):get_variant()
end

game_meta:register_event("on_world_changed", function(game)
    local hero = game:get_hero()  
    hero:remove_charm()
    if hero:is_running() then
      game.prevent_running_restoration=true
    end
  end)    


game_meta:register_event("on_map_changed", function(game, map)

    -- Init infinite timer and check if sound is played
    local crystal_state = map:get_crystal_state() 
    local timer = sol.timer.start(map, 50, function()  
        local changed = map:get_crystal_state() ~= crystal_state
        crystal_state = map:get_crystal_state()
        if changed and not map:get_game():is_suspended() then
          for e in map:get_entities_by_type("custom_entity") do
            if e:get_model()=="crystal_block" then
              e:notify_crystal_state_changed()
            end
          end
          --audio_manager:play_sound("misc/dungeon_crystal")
          audio_manager:play_sound("misc/dungeon_switch") --Temporary, remove me when xwe have an actual sound for crystal switches
        end
        return true
      end)
    timer:set_suspended_with_map(false)

  end)

--This function blacks out the screen during map loading time while having custom transitions active
--Note: this was supposed to be temporary until the engine-side map loader was optimized, 
--  but since it doesn't take any extra resource, it can be kept permanently
game_meta:register_event("on_draw", function(game, dst_surface)

    if game.map_in_transition then
      dst_surface:fill_color({0,0,0})
    end

  end)

---------------------------------
--                             --
--        DANGER ZONE !!!      --
--                             --
---------------------------------
--game_meta.start_attack=function(game)
--  print "huh ?"
--  local hero=game:get_hero()
--  local state, cstate=hero:get_state()
--  if state=="free" or state=="custom" and game:get_sword_ability()>0 and cstate:get_can_use_sword() then
--    hero:swing_sword()
--  end
--end

--[[
 Global override for item use that completely avoids triggering the "item" state and allows full control over item behavior. 
 To enable it, simply define item:start_using() in your item script.
 It also handles a combo system which allows to have a special behavior for both assigned items.
 To enable it, simply define item:start_combo(other) in your script.
  That way, the other item implied in the combo will be passed automatically and you will be able to test for compatibility between both items.
 Notes: 
  if a combo was triggered but no special case was set, then it will first try to fall back to individual item override.
  in your combo override, you wll want to keep it's normal behavior (especially for items with limited amounts) so it doesn"t break them if you ran out of usage of the other item)
  Similarily, if no override was set to single item behavior, then it will fall back to default behavior (item:on_using)
   then it will just be ignored and trigger item:on_using as usual.
  The sword, being an built-in equipment item with it's own command, is not concerned by this system by default.
   However, you can still do combinations by testing for the ability in your item script itself, or even make it an assignable item.
--]]

game_meta:register_event("on_command_pressed", function(game, command)

    local hero = game:get_hero()
    local state, cstate = hero:get_state()

    if command == "attack" and state ~= "frozen" and not game:is_suspended() then
--      if state=="free" or state=="custom" and game:get_sword_ability()>0 and cstate:get_can_use_sword() then
--        hero:swing_sword()
--        return true -- TODO find a clean way to prevent build-in sword to trigger while still letting sword command to pass through...
--      end

    elseif command == "item_1" or command =="item_2" then

      local allowed_states = {
        free = true,
        custom = true,
      }

      if not game:is_suspended() and allowed_states[state] ~= nil and hero:get_ground_below() ~= "hole" then

        -- Get assigned items.
        local assigned_items = {
          item_1 = game:get_item_assigned("1"),
          item_2 = game:get_item_assigned("2")
        }
        if not assigned_items[command] then
          return
        end

        local item = assigned_items[command]
        local item_name = item:get_name() or ""

        -- Prevent item to start if the current context forbid it.
        if not game:get_map():is_sideview() and hero:get_state() == "swimming" then
          return true
        elseif state == "custom" and not cstate:get_can_use_item(item_name) then
          return true
        elseif hero.passive_states then
          for _, passive_state in pairs(hero.passive_states) do
            if not passive_state.allowed_items[item_name] then
              return true
            end
          end
        end

        -- Use the item normally if no possible combo to start.
        if not item.start_combo then
          if item.start_using then
            item:start_using()
            return true
          else
            return
          end
        end

        -- Else try starting the combo.
        local item_1 = assigned_items.item_1
        local item_2 = assigned_items.item_2
        local handled = item.start_using ~= nil or item.start_combo ~= nil

        if not item_1 or not item_2 then
          return
        end

        game["last_" .. command] = item_name
        sol.timer.start(game, combo_timer_duration + 10, function()
          game["last_" .. command] = nil -- Delay resetting combo register for next cycle after combo checking.
        end)
        if game["last_" .. (command == "item_1" and "item_2" or "item_1")] ~= nil then
          if game.item_combo ~= true and game.last_item_1 ~= nil and game.last_item_2 ~= nil then

            sol.timer.start(game, combo_timer_duration + 10, function()
              game.item_combo = nil
            end)
      
            -- Both items are trying to be used at the same time, so try to start the combo for both of them
            if item_1.start_combo then
              game.item_combo = true
              item_1:start_combo(item_2)
              return true
            elseif item_2.start_combo then
              game.item_combo = true
              item_2:start_combo(item_1)
              return true
            end
          end
        end

        -- At this point, no combo was triggered, so we start the combo cancelling timer
        -- This timer ensures we have enough time to press the other command before falling back to single-item behavior.
        sol.timer.start(game, combo_timer_duration, function()

          -- At this point, the combo was not triggered at all
          -- or has already been handled in a previous cycle and not been cleaned yet
          -- so we try using the normal override on each item instead.
          if game.item_combo == nil then
            if game.last_item_1 and item_1.start_using ~= nil then
              item_1:start_using()
            elseif game.last_item_2 and item_2.start_using ~= nil then
              item_2:start_using()
            end
          end
          -- If we reached this point then it means that the item had no override (and the execution will now default to the built-in bahavior)
        end)
        return handled
      end
    end
  end)
