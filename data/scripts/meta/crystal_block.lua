local crystal_block_meta = sol.main.get_metatable("crystal_block")
local hero_meta = sol.main.get_metatable("hero")
local enemy_meta = sol.main.get_metatable("enemy")
local map_meta = sol.main.get_metatable("map")

-- Returns whether a rectangle overlaps any raised crystal block.
function map_meta:is_rectangle_on_raised_crystal_block(x, y, width, height)
  local map = self
  for e in map:get_entities_in_rectangle(x, y, width, height) do
    if e:get_type() == "custom_entity" and e:get_model() == "crystal_block" then
      if e:is_enabled() and e:is_raised() then
        return true
      end
    end
  end
  return false
end

-- Returns whether an entity is currently on any raised crystal block.
local function is_on_raised_crystal_block(entity)

  return entity:get_map():is_rectangle_on_raised_crystal_block(entity:get_bounding_box())
end

hero_meta.is_on_raised_crystal_block = is_on_raised_crystal_block
enemy_meta.is_on_raised_crystal_block = is_on_raised_crystal_block

-- Replace built-in crystal blocks by custom ones to make the feather work better with them.
function crystal_block_meta:on_created()

  local map = self:get_map()
  local animation = self:get_sprite():get_animation()
  local raised_by_default = (animation == "blue_lowered" or animation == "blue_raised")

  local x, y, layer = self:get_position()
  local width, height = self:get_size()
  map:create_custom_entity({
    direction = 0,
    layer = layer,
    x = x,
    y = y,
    width = width,
    height = height,
    origin_x = 0,
    origin_y = 0,
    sprite = "entities/crystal_block",
    tiled = true,
    model = "crystal_block",
    properties = {
      {
        key = "initially_raised",
        value = raised_by_default and "true" or "false"
      }
    }
  })
  self:remove()
end

return crystal_block_meta
