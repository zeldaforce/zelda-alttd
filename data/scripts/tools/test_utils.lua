local function format_value(value)
  if type(value) == "string" then
    return "'" .. value .. "'"
  end

  if type(value) == "userdata" then
    return tostring(value):gsub("userdata", sol.main.get_type(value))
  end

  return tostring(value)
end

function assert_equal(actual, expected, msg)

  if actual ~= expected then
    local err = string.format("equality assertion failed: expected %s, got %s",
                              format_value(expected),
                              format_value(actual))
    if msg then
      err = string.format("%s (%s)",msg,err)
    end
    error(err)
  end
end
