-- Custom flying script.
local audio_manager = require("scripts/audio_manager")

-- Variables
local hero_meta = sol.main.get_metatable("hero")
local map_meta = sol.main.get_metatable("map")
local game_meta = sol.main.get_metatable("game")

local state = sol.state.create("flying")
state:set_description("flying")
state:set_carried_object_action("remove")
state:set_gravity_enabled(false)
state:set_can_use_item(false)
state:set_can_be_hurt(true)
state:set_can_control_direction(true)
state:set_can_control_movement(true)
state:set_can_use_sword(false)
state:set_can_use_jumper(false)
state:set_can_use_shield(false)
state:set_can_use_stairs(false)
state:set_can_pick_treasure(false)
state:set_can_use_teletransporter(true)
state:set_can_interact(false)
state:set_can_push(false)
state:set_can_grab(false)
state:set_can_traverse("teletransporter", false)
state:set_affected_by_ground("hole", false)
state:set_affected_by_ground("grass", false)
state:set_affected_by_ground("ladder", false)
state:set_affected_by_ground("shallow_water", false)
state:set_affected_by_ground("deep_water", false)

function state:on_started()

  local map = state:get_map()
  local hero = map:get_hero()
  hero:set_animation("flying")
  hero:create_sprite("npc/animals/flying_rooster", "flying_rooster")
  -- Flying rooster sprite init
  local sprite_flying_rooster = hero:get_sprite("flying_rooster")
  sprite_flying_rooster:set_animation("holding")
  sprite_flying_rooster:set_xy(0, -24)
  -- Tunic hero sprite init
  local sprite_tunic = hero:get_sprite("tunic")
  sprite_tunic:set_xy(0, -8)
  sprite_tunic:register_event("on_direction_changed", function(sprite, animation, direction)
    local custom_state = map:get_hero():get_state_object()
    if custom_state and custom_state:get_description() == "flying" then
      sprite_flying_rooster:set_direction(direction)
    end
  end)
end

function state:on_finished()

  local map = state:get_map()
  local game = map:get_game()
  local hero = map:get_hero()
  local sprite_flying_rooster = hero:get_sprite("flying_rooster")
  hero:remove_sprite(sprite_flying_rooster)
  local sprite_tunic = hero:get_sprite("tunic")
  sprite_tunic:set_xy(0, 0)

  -- Recreate the flying rooster in following mode
  sol.timer.start(map, 10, function()
    game:init_companions(map)
  end)
end

local function current_height(current_time, total_time, height)
  local progress = current_time / total_time
  return 2 * height * (progress ^ 2 - progress) - (height * (1.0 - progress))
end

function state:throw_flying_rooster()

  local map = state:get_map()
  local game = map:get_game()
  local hero = map:get_hero()
  local direction4 = hero:get_direction()

  hero:unfreeze()
  game:init_companions(map)

  local flying_rooster = map:get_entity("companion_flying_rooster")
  local flying_rooster_sprite = flying_rooster:get_sprite()
  flying_rooster:set_state("stopped")
  audio_manager:play_sound("hero/throw")

  local movement = sol.movement.create("straight")
  movement:set_angle(direction4 * math.pi / 2)
  movement:set_speed(200)
  movement:set_max_distance(80)
  movement:set_smooth(false)

  local t = 0
  local refresh_delay = 10
  local height_timer = sol.timer.start(flying_rooster, refresh_delay, function()
    if flying_rooster:get_state() ~= "stopped" then
      return false
    end
    t = t + refresh_delay
    local x, y = flying_rooster_sprite:get_xy()
    flying_rooster_sprite:set_xy(x, current_height(t, 400, 24))
    return true
  end)

  local function restore_following()
    height_timer:stop()
    flying_rooster_sprite:set_xy(0, 0)
    hero:set_position(hero:get_position())  -- Workaround to update the facing entity and remove the lift action in the HUD.
    sol.timer.start(flying_rooster, 500, function()
      if flying_rooster:get_movement() == movement then
        flying_rooster:stop_movement()
      end
    end)
  end

  movement:start(flying_rooster, restore_following)
  function movement:on_obstacle_reached()
    restore_following()
  end
end

function state:on_command_pressed(command)

  local hero = state:get_entity()
  if command == "action" then
    state:throw_flying_rooster()
  end
end

function state:on_map_started()

  local map = state:get_map()
  local hero = map:get_hero()

  -- Restore sprite offsets as they are reset due to side view management when the map changes.
  sol.timer.start(map, 10, function()
    local sprite_flying_rooster = hero:get_sprite("flying_rooster")
    sprite_flying_rooster:set_xy(0, -24)
    local sprite_tunic = hero:get_sprite("tunic")
    sprite_tunic:set_xy(0, -8)
  end)
end

-- Start flying
function hero_meta:start_flying()

  local hero = self
  hero:start_state(state)

end

return state