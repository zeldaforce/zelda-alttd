----------------------------------
--
-- Sideview swimming feature.
--
-- Kind of passive state running on the top of regular engine states without stopping them when the hero is touching deep water.
-- Basically just force a custom inertia movement, switch regular sprite animations by corresponding swimming ones when needed,
-- force direction2 from direction4 or commands when needed, and handle allowed items.
--
-- Functions : is_in_water(entity)
--             start_swimming(hero, on_stopped_callback)
--
----------------------------------
local swimming_manager = {}
local map_tools = require("scripts/maps/map_tools")

-- Global variables.
local game_meta = sol.main.get_metatable("game")
local hero_meta = sol.main.get_metatable("hero")

local quarter = 0.5 * math.pi
local circle = 2.0 * math.pi
local skip_next_splash_effect = false

-- Configuration variables.
local speed_maximum = 88 * 0.66 -- ps/s
local acceleration = 200 -- ps/s²
local deceleration = 120 -- ps/s²
local vspeed_to_standard = 36 -- Ratio to apply on the sideview's vspeed property to get the water pixel by second speed.
local vspeed_on_jumping_out = -3.0 -- Reference vspeed to apply when jumping out the water.

-- Start the splash effect on the entity position.
local function start_splash_effect(entity)

  if skip_next_splash_effect then
    skip_next_splash_effect = false
    return
  end

  local map = entity:get_map()
  local x, y, layer = entity:get_position()
  local effect = map:create_custom_entity({
      sprite = "entities/effects/sideview_splash",
      x = x,
      y = y,
      layer = layer,
      width = 32,
      height = 32,
      direction = 0
  })
  effect:set_drawn_in_y_order()

  -- Remove the effect entity once animation finished.
  local sprite = effect:get_sprite()
  sprite:set_animation(sprite:get_animation(), function()
    if effect:exists() then
      effect:remove()
    end
  end)

  return effect
end

-- Round the given number using the FPU's current rounding mode, which is usually round to nearest, ties to even.
local function round(num)
  return num + (2^52 + 2^51) - (2^52 + 2^51)
end

-- Add the second straight movement to the first one.
local function add_straight_movement(movement1, movement2)

  local x = movement1.speed * math.cos(movement1.angle) + movement2.speed * math.cos(movement2.angle)
  local y = movement1.speed * math.sin(movement1.angle) + movement2.speed * math.sin(movement2.angle)

  movement1.speed = math.sqrt(x^2 + y^2)
  movement1.angle = math.atan2(y, x) % circle
end

-- Update hero movement while swimming.
local function update_movement(hero)

  local game = hero:get_game()
  local state = hero:get_state_object() and hero:get_state_object():get_description() or hero:get_state()
  local movement = hero.water.movement

  -- Don't move on stopping states.
  if state == "frozen" or state == "sword swinging" or state == "sword spin attack" then
    return
  end

  -- Workaround: Make sure no built-in movement is applied.
  if hero:get_movement() then
    hero:stop_movement()
  end

  -- Apply deceleration.
  add_straight_movement(movement, {
    speed = math.min(deceleration * 0.01, movement.speed), -- Set the speed back to 0 instead of applying deceleration if too slow.
    angle = movement.angle + math.pi
  })
  local decelerated_speed = movement.speed

  -- Apply possible acceleration from vspeed if it changed from outside (such as from the feather item).
  if hero.vspeed and hero.vspeed ~= 0 then
    add_straight_movement(movement, {
      speed = math.abs(hero.vspeed * vspeed_to_standard),
      angle = hero.vspeed < 0 and quarter or 3.0 * quarter
    })
    hero.vspeed = nil
  end

  -- Apply possible acceleration from direction commands.
  local is_direction_pressed = game:is_command_pressed("right") or game:is_command_pressed("up") or game:is_command_pressed("left") or game:is_command_pressed("down")
  if is_direction_pressed then
    add_straight_movement(movement, {
      speed = acceleration * 0.01,
      angle = math.atan2((game:is_command_pressed("up") and 1 or 0) + (game:is_command_pressed("down") and -1.0 or 0), (game:is_command_pressed("right") and 1 or 0) + (game:is_command_pressed("left") and -1 or 0))
    })
  end

  -- Only keep the new acceleration speed if it would not increase an already too high one.
  if movement.speed > speed_maximum and movement.speed > decelerated_speed then
    movement.speed = math.max(speed_maximum, decelerated_speed)
  end

  -- Get the closest accessible position from the target depending on the new momentum.
  local position_x, position_y = hero:get_position()
  local target_x = position_x + hero.water.decimal_position.x + movement.speed * math.cos(movement.angle) * 0.01
  local target_y = position_y + hero.water.decimal_position.y - movement.speed * math.sin(movement.angle) * 0.01
  local final_x, final_y = map_tools.get_closest_accessible_position(hero, round(target_x), round(target_y))

  -- Store the target decimal places to take care of a really slow acceleration.
  hero.water.decimal_position.x = target_x - round(target_x)
  hero.water.decimal_position.y = target_y - round(target_y)

  -- Move to the final position.
  hero:set_position(final_x, final_y)
end

-- Update the hero direction2 and 4 while swimming.
local function update_directions(hero)

  local state = hero:get_state_object() and hero:get_state_object():get_description() or hero:get_state()

  -- Only update direction on free state.
  if state ~= "free" then
    return
  end

  local game = hero:get_game()
  local direction = hero:get_direction()

  -- Store the hero direction4 that will possibly be set later when exiting the free state. Only consider vertical directions if the key is being pressed and without any horizontal one.
  hero.water.direction4 = game:is_command_pressed("right") and 0 or game:is_command_pressed("left") and 2 or game:is_command_pressed("up") and 1 or game:is_command_pressed("down") and 3 or hero.water.direction2

  -- Set direction2 if left or right command pressed, else keep it unchanged.
  local direction2 = game:is_command_pressed("right") and 0 or game:is_command_pressed("left") and 2 or nil
  if direction2 then
    hero.water.direction2 = direction2
  end
  if hero.water.direction2 ~= direction then
    hero:set_direction(hero.water.direction2)

    -- Workaround: unfreeze and starts an empty movement to take care of a strange engine behavior that constantly set back the direction until the state changes when unfreezing from outside.
    hero:unfreeze()
    local movement = sol.movement.create("straight")
    movement:set_speed(0)
    movement:set_angle(hero.water.direction2 * math.pi)
    movement:set_smooth(false)
    movement:start(hero)
  end
end

-- Switch to the swimming animation version of the engine state if needed.
local function update_animation(hero)

  local state = hero:get_state_object() and hero:get_state_object():get_description() or hero:get_state()
  local animation = hero:get_animation()

  if state == "free" or state == "bonking" then
    local game = hero:get_game()
    local is_direction_pressed = game:is_command_pressed("right") or game:is_command_pressed("up") or game:is_command_pressed("left") or game:is_command_pressed("down")
    local swimming_animation = is_direction_pressed and "swimming_scroll" or "stopped_swimming_scroll"
    if animation ~= swimming_animation then
      hero:set_animation(swimming_animation)
    end

  elseif state == "sword loading" then

    if animation ~= "swimming_scroll_loading" then
      hero:set_animation("swimming_scroll_loading")
    end
  end
end

-- Prepare the transition to the next engine state while swimming.
local function on_state_changing(hero, state, next_state)

  local direction = hero:get_direction()

  -- Stop the momentum if needed.
  if state == "sword swinging" or state == "sword spin attack" or state == "using item" or state == "frozen" then
    hero.water.movement.speed = 0
  end

  -- Restore the direction4 on sword swinging.
  if state == "free" and next_state == "sword swinging" then
    if hero.water.direction4 ~= direction then
      hero:set_direction(hero.water.direction4)
    end
  end

  -- Go back to the last direction2 on sword loading.
  if state == "sword swinging" and next_state == "sword loading" then
    if hero.water.direction2 ~= direction then
      hero:set_direction(hero.water.direction2)
    end
  end
end

-- Restore the out of water movement.
local function stop_swimming(hero)

  local game = hero:get_game()
  hero.passive_states.sideview_swimming = nil
  hero:unfreeze() -- Workaround: For some reason unfreezing the hero is needed when its movement is manually stopped.
  hero:set_walking_speed(hero.water.leaving_speed)
  hero.vspeed = vspeed_on_jumping_out * hero.water.movement.speed * math.sin(hero.water.movement.angle) / speed_maximum
  game:set_ability("lift", game:get_item("power_bracelet"):get_variant())

  -- Call stopped event.
  if hero.on_passive_state_stopped then
    hero:on_passive_state_stopped("sideview_swimming")
  end
end

-- Check for water at the entity position, whatever the layer is.
function swimming_manager.is_in_water(entity)

  local map = entity:get_map()
  local x, y = entity:get_position()

  for layer = map:get_max_layer(), map:get_min_layer(), -1 do
    if map:get_ground(x, y, layer) == "deep_water" then
      return true
    end
  end
  
  return false
end

-- Start the swimming passive state.
function swimming_manager.start_swimming(hero, on_stopped_callback)

  local game = hero:get_game()
  local map = hero:get_map()

  -- Entering effect.
  start_splash_effect(hero)

  -- Abort the swimming and start a drowning if no swim ability.
  if game:get_ability("swim") == 0 then
    hero:freeze()
    hero:set_animation("drowning", function()
      hero:set_position(hero:get_solid_ground_position())
      hero:unfreeze()
      if on_stopped_callback then
        on_stopped_callback()
      end
    end)
    return
  end

  -- Initialize water state properties depending on the current on-air movement.
  local movement = hero:get_movement()
  local hspeed = movement and movement:get_speed() * math.cos(movement:get_angle()) or 0.0
  local vspeed = (hero.vspeed or 0) * vspeed_to_standard -- Gravity doesn't use movement, get it through the vspeed

  if not hero.water then
    hero.water = {}
  end
  hero.water.movement = {
    speed = math.sqrt(hspeed^2 + vspeed^2),
    angle = -math.atan2(vspeed, hspeed)
  }
  hero.water.decimal_position = { -- Keep the swimming position decimal places, to take care of a really slow acceleration.
    x = 0.0,
    y = 0.0
  }
  hero.water.direction4 = hero:get_direction()
  hero.water.direction2 = hero.water.direction4 == 0 and 0 or hero.water.direction4 == 2 and 2 or hero.water.direction2 or hero.water.direction4 == 1 and 0 or 2 -- Try to go back to the last swimming direction2 if oriented vertically
  hero.water.leaving_speed = hero:get_walking_speed()

  if not hero.passive_states then
    hero.passive_states = {}
  end
  hero.passive_states.sideview_swimming = {
    allowed_items = {
      bombs_counter = true,
      boomerang = true,
      bow = true,
      feather = true,
      fire_rod = true,
      hammer = true,
      hookshot = true,
      magic_powder_counter = true,
      melody_1 = true,
      melody_2 = true,
      melody_3 = true,
      pegasus_shoes = false,
      shield = false,
      shovel = true
    }
  }
  game:set_ability("lift", -1)

  -- If currently using a non-allowed item, stop it.
  for item_name, is_allowed in pairs(hero.passive_states.sideview_swimming.allowed_items) do
    if not is_allowed then
      local item = game:get_item(item_name)
      if item:is_being_used() then
        if item.stop_using then
          item:stop_using()
        else
          print("The item " .. item_name .. " has no stop_using() method")
        end
      end
    end
  end

  -- Stop hero regular movement.
  hero:set_walking_speed(0) -- Immobilize instead of freezing cause we basically just want a custom movement and keep almost everything else unchanged.
  hero.vspeed = nil

  -- Start water movement loop.
  sol.timer.start(hero, 0, function()

    -- Stop the swimming if not on the map anymore.
    if map ~= hero:get_map() then
      stop_swimming(hero)
      return false
    end

    -- Update the swimming movement and stop it if not on the water anymore.
    update_movement(hero)
    if not swimming_manager.is_in_water(hero) then
      stop_swimming(hero)
      if on_stopped_callback then
        on_stopped_callback(hero)
      end
      return false
    end
    update_directions(hero)
    update_animation(hero)
    return 10
  end)

  -- Call started event.
  if hero.on_passive_state_started then
    hero:on_passive_state_started("sideview_swimming")
  end
end

-- Handle the transition between engine states while swimming.
hero_meta:register_event("on_state_changing", function(hero, state, next_state)

  if hero:get_map():is_sideview() and swimming_manager.is_in_water(hero) then
    on_state_changing(hero, state, next_state)
  end
end)

-- Set the swimming animation during the map transition if starting into water.
game_meta:register_event("on_map_changed", function(game, map)

  if map:is_sideview() then
    local hero = map:get_hero()
    if swimming_manager.is_in_water(hero) then
      skip_next_splash_effect = true
      hero:set_animation("stopped_swimming_scroll")
    end
  end
end)

return swimming_manager