--[[
Sideview manager

This script implements gravity and interaction of the hero with ladders, which are used in sideview maps.

To initialize, just require it in a game setup script, like features.lua, 
then call map:set_sidewiew(true) in the on_started event of each map you want to be in sideview mode.
   
If you need to make things jump like the hero when he uses the feather, then simply do <your_entity>.vspeed=<some_negative_number>, then the gravity will do the rest.

You can make any entity be affected by gravity when the map starts by adding "has_gravity" to "true" in its custom properties,
or control it dynamically with map:start_gravity(entity) and map:stop_gravity(entity)
--]]

local map_meta = sol.main.get_metatable("map")
local hero_meta = sol.main.get_metatable("hero")
local game_meta = sol.main.get_metatable("game")
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local swimming_manager = require("scripts/maps/sideview_swimming_manager")
local walking_speed = 88
local ladder_speed = 52
local gravity = 0.2
local max_vspeed = 2
local underwater_vspeed_ratio = 0.66

-- Returns whether the ground at given coordinates is a ladder.
local function is_position_on_ladder(map, x, y)
  
  for entity in map:get_entities_in_rectangle(x, y, 1, 1) do
    if entity:get_type() == "custom_entity" and entity:get_model() == "ladder" then
      return true
    end
  end

  return false
end

-- Set the sideview mode flag for the map to the given parameter.
function map_meta.set_sideview(map, enabled)
  map.sideview = enabled or false
end

-- Returns whether the current map is in sideview mode.
function map_meta.is_sideview(map)
  return map.sideview or false
end

-- Set the vertical speed on the entity, in pixels/frame.
function map_meta.set_vspeed(entity, vspeed)
  entity.vspeed = vspeed
end

-- Returns the current vertical speed of the entity, in pixels/frame.
function map_meta.get_vspeed(entity)
  return entity.vspeed or 0
end

-- Return whether the ground below the entity is a ladder.
local function is_on_ladder(entity)

  local map = entity:get_map()
  local x, y = entity:get_position() 
  return is_position_on_ladder(map, x, y - 2) or is_position_on_ladder(map, x, y + 2)
end

-- Return whether the hero is above a ladder.
local function is_above_ladder(entity)

  local x, y = entity:get_position()
  return entity:test_obstacles(0, 1) or not is_on_ladder(entity) and is_position_on_ladder(entity:get_map(), x, y + 3)
end

-- Check if an enemy sensible to jump is overlapping the hero, then hurt it and bounce.
local function on_bounce_possible(entity)

  local map = entity:get_map()
  local hero = map:get_hero()
  for enemy in map:get_entities_by_type("enemy") do
    if hero:overlaps(enemy, "overlapping") and enemy:get_life() > 0 and not enemy:is_immobilized() then
      local reaction = enemy:get_jump_reaction()
      if reaction ~= "ignored" then
        enemy:receive_attack_consequence("jump", reaction)
        entity.vspeed = 0 - math.abs(entity.vspeed)
      end
    end
  end
  return entity.vspeed or 0
end


-- Applies a semi-realistic gravity to the given entity, and resets the vertical speed if the reached a solid obstacle or is above a ladder
local function update_gravity(entity)

  local x, y, layer = entity:get_position()
  local map = entity:get_map()
  local w, h = map:get_size()

  -- Update the vertical speed.
  local vspeed = entity.vspeed or 0
  if vspeed > 0 then
    vspeed = on_bounce_possible(entity)

    -- Try to apply downwards movement.
    if entity.has_grabbed_ladder or is_above_ladder(entity) then

      -- We are on an obstacle, reset the speed and bail.
      if entity:get_type() == "hero" and not entity.landing_sound_played then
        entity.landing_sound_played = true
        audio_manager:play_sound("hero/land")
      end
      entity.vspeed = nil
      return false
    end
    entity:set_position(x, y + 1)
  elseif vspeed < 0 then

    -- Try to get up.
    if not entity:test_obstacles(0, -1) then
      entity:set_position(x, y - 1)
    end
  end

  -- Update the vertical speed.
  if (not entity:test_obstacles(0, 1) and not is_above_ladder(entity) and (not entity.has_grabbed_ladder or vspeed < 0)) or vspeed > 0 then
    entity.vspeed = math.min(vspeed + gravity, max_vspeed)
  end

  -- Slowdown the fall if underwater.
  if entity.vspeed and swimming_manager.is_in_water(entity) then
    entity.vspeed = entity.vspeed * underwater_vspeed_ratio
  end

  -- Set the new delay for the timer.
  return vspeed == 0 and 10 or math.min(math.floor(10 / math.abs(vspeed)), 100)
end

-- Updates the hero active movement and sprite.
local function update_hero(hero)

  local movement = hero:get_movement()
  local game = hero:get_game()

  local function command(id)
    return game:is_command_pressed(id)
  end
  local state, cstate = hero:get_state()
  local desc = cstate and cstate:get_description() or ""
  local x, y, layer = hero:get_position()
  local map = game:get_map()
  local speed = 88
  local wanted_angle
  local can_move_vertically = true
  local _left, _right, _up, _down
  local ladder_found = is_on_ladder(hero)

  --------------------
  -- Command inputs --
  --------------------

  if command("up") and not command("down") then
    _up = true
    if ladder_found then
      if not hero.has_grabbed_ladder then
        hero:unfreeze() -- Exit possible running states like bonking.
      end
      hero.has_grabbed_ladder = true
      if is_position_on_ladder(map, x, y) then
        speed = ladder_speed
      end
    else
      can_move_vertically = false
    end
  elseif command("down") and not command("up") then
    _down = true
    if ladder_found or is_position_on_ladder(map, x, y + 3) then
      if not hero.has_grabbed_ladder then
        hero:unfreeze() -- Exit possible running states like bonking.
      end
      hero.has_grabbed_ladder = true
      if is_position_on_ladder(map, x, y) then
        speed = ladder_speed
      end
    else
      can_move_vertically = false
    end
  end

  -- Check if we are on the top of a ladder
  if not (ladder_found or is_position_on_ladder(map, x, y + 3)) then
    hero.has_grabbed_ladder = false
  end

  if command("right") and not command("left") then
    _right = true
    wanted_angle = 0
    if not hero.has_grabbed_ladder then
      speed = walking_speed
    end

  elseif command("left") and not command("right") then
    _left = true
    wanted_angle = math.pi
    if not hero.has_grabbed_ladder then
      speed = walking_speed
    end
  end

  -- Force the hero on a ladder if we came from the side
  if hero:test_obstacles(0, 1) and is_on_ladder(hero) and is_position_on_ladder(map, x, y + 3) then
    hero.is_jumping = nil
    hero.has_grabbed_ladder = true
  end

  -- Handle movement for vertical and/or diagonal input
  if not can_move_vertically then

    if movement then
      local angle = movement:get_angle()
      if _up then
        if _left or _right then
          if wanted_angle ~= angle then 
            movement:set_angle(wanted_angle)
          end
        else
          speed = 0
        end
      elseif _down then
        if _left or _right then
          if wanted_angle ~= angle then 
            movement:set_angle(wanted_angle)
          end
        else
          speed = 0
        end
      end
    end
  end

  if speed and speed ~= hero:get_walking_speed() then
    hero:set_walking_speed(speed)
  end

  ----------------
  -- Animations --
  ----------------

  speed = movement and movement:get_speed() or 0
  local sprite = hero:get_sprite("tunic")
  local direction = sprite:get_direction()
  local new_animation

  if state == "lifting" then
    new_animation = "lifting_heavy"
  end

  if state == "free" and not hero.frozen then
    if hero.has_grabbed_ladder and is_on_ladder(hero) then
      new_animation = speed == 0 and "climbing_stopped" or "climbing_walking"
    elseif not is_above_ladder(hero) then
      new_animation = "jumping"
    else
      new_animation = speed == 0 and "stopped" or "walking" 
    end 
  end

  if new_animation and new_animation ~= hero:get_animation() then
    hero:set_animation(new_animation)
  end
end

-- Redeclaration of the "on map changed' event to take account of the sideview mode.
-- This override starts the routine which updates the gravity of the entitites for sideviews, and sets up the sprite of the hero by shifting it by 2 pixels when in sideviews.
game_meta:register_event("on_map_changed", function(game, map)

  local hero = map:get_hero()
  hero.vspeed = 0

  if map:is_sideview() then
    hero.landing_sound_played = true -- Don't play landing sound at the start of the map
    hero.has_grabbed_ladder = is_on_ladder(hero, -1) 
    if hero.has_grabbed_ladder then
      hero:set_direction(1)
      hero:set_animation("climbing_stopped")
      hero:set_walking_speed(ladder_speed)
    end

    -- Start gravity on entities that are affected by default or ones with the has_gravity property.
    for entity in map:get_entities() do
      local type = entity:get_type()
      if type == "hero" or type == "carried_object" or entity:get_property("has_gravity") == "true" then
        map:start_gravity(entity)
      end
    end
  else
    hero:set_walking_speed(88)
  end
end)

-- Stop gravity on the given entity.
map_meta:register_event("stop_gravity", function(map, entity)

  if entity.gravity_timer then
    entity.gravity_timer:stop()
  end
end)

-- Start gravity on the given entity.
map_meta:register_event("start_gravity", function(map, entity)

  show_hitbox(entity)

  -- Make sure gravity won't be started more than once
  map:stop_gravity(entity)

  -- Don't play landing sound if starting on a ladder.
  if entity:get_type() == "hero" then
    local x, y = entity:get_position()
    if not is_on_ladder(entity) and not is_position_on_ladder(map, x, y + 3) then
      entity.landing_sound_played = nil
    end
  end

  -- Start gravity effect timer loop
  entity.gravity_timer = sol.timer.start(entity, 10, function()

    -- Stop the gravity if not on the map anymore.
    if map ~= entity:get_map() then
      return false
    end

    -- Update gravity.
    local delay = update_gravity(entity)

    -- Make the hero swim if touching deep water and he has flippers, else let him drawn.
    if entity:get_type() == "hero" and swimming_manager.is_in_water(entity) then
      swimming_manager.start_swimming(entity, function()

        -- Grab the ladder if any
        if is_on_ladder(entity) then
          entity.has_grabbed_ladder = true
          entity.vspeed = nil
          entity:set_walking_speed(ladder_speed)
        end

        -- Restarts gravity.
        map:start_gravity(entity)
      end)
      return false
    end

    return delay or 10
  end)
end)

-- Passive behavior on position changes.
hero_meta:register_event("on_position_changed", function(hero, x, y, layer)

  local map = hero:get_map()
  if map:is_sideview() then
    local w, h = map:get_size()
    
    -- Respawn when falling into a pit
    if y + 3 >= h then
      hero:set_position(hero:get_solid_ground_position())
      hero:start_hurt(1)
    end
    
    -- Save last stable ground
    if y + 2 < h and hero:test_obstacles(0, 1) and map:get_ground(x, y + 3, layer) == "wall" and hero:get_ground_below() ~= "prickles" then
      hero:save_solid_ground(x, y, layer)
    end
  end
end)

-- Start the sideview passive behavior if needed on hero state changed.
hero_meta:register_event("on_state_changed", function(hero, state)

  local game = hero:get_game()
  local map = hero:get_map()

  if map:is_sideview() then
    if state == "free" or state == "carrying" or state == "sword loading" or state == "custom" then --TODO identify every applicable states

      sol.timer.start(hero, 10, function()

        -- Stop the timer if not on the same state or map anymore.
        if state ~= hero:get_state() or map ~= hero:get_map() then
          return false
        end

        -- Update hero animations if not on the water.
        if not swimming_manager.is_in_water(hero) then
          update_hero(hero)
        end
        return true
      end)
    elseif state == "grabbing" then -- Prevent the hero from pulling things in sideview mode.
      hero:unfreeze()
    end
    return
  end
end)