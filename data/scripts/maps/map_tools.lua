local map_tools = {}

local audio_manager = require("scripts/audio_manager")

-----------------------
-- Visual effects.
-----------------------

-- Shake the screen
function map_tools.start_earthquake(shake_config)

  local map = sol.main.get_game():get_map()
  local timer_sound = sol.timer.start(map, 0, function()
    audio_manager:play_sound("misc/dungeon_shake")
    return shake_config.sound_frequency or 450
  end)
  map:start_coroutine(function()
    local camera = map:get_camera()
    timer_sound:set_suspended_with_map(false)
    wait_for(camera.shake, camera, shake_config)
    timer_sound:stop()
  end)
end

-- Start a set of chained explosion placed randomly around the entity coordinates.
function map_tools.start_close_explosions(entity, duration, max_distance, callback)

  local map = entity:get_map()
  local x, y, layer = entity:get_position()
  local main_time = sol.main.get_elapsed_time()
  
  audio_manager:play_sound("items/bomb_explode")

  local explosion = map:create_explosion(
      {name = "chained_explosion", x = x + math.random(-max_distance, max_distance), y = y + math.random(-max_distance, max_distance), layer = map:get_max_layer()})
  if explosion ~= nil then -- Avoid Errors when closing the game while a chained explosion is running
    explosion:get_sprite():set_ignore_suspend(true)
    explosion:register_event("on_removed", function(explosion)
      local elapsed_time = sol.main.get_elapsed_time() - main_time
      if elapsed_time < duration then
        map_tools.start_close_explosions(entity, duration - elapsed_time, max_distance, callback)
      else
        callback()
      end
    end)
  end
end

-- Start a parallax effect on the given entity.
function map_tools.start_parallax_scrolling(entity, scrolling_ratio)

  if not scrolling_ratio then
    return
  end

  local map = entity:get_map()
  local camera = map:get_camera()
  local initial_x, initial_y = entity:get_position()

  map:register_event("on_update", function(map)
    entity:set_position(initial_x + camera:get_position() * (1.0 - scrolling_ratio), initial_y) -- 1 - X to compensate the engine scrolling.
  end)
end

-----------------------
-- Saving tools.
-----------------------

-- Save current entity position.
function map_tools.save_entity_position(entity)
  local game = sol.main.get_game()
  local world = entity:get_map():get_world()
  local entity_name = entity:get_name()
  local x, y, layer = entity:get_position()
  game:set_value(world .. "_" .. entity_name .. "_x", x)
  game:set_value(world .. "_" .. entity_name .. "_y", y)
  game:set_value(world .. "_" .. entity_name .. "_layer", layer)
end

-- Get saved position for the entity, or current position if nothing saved.
function map_tools.get_entity_saved_position(entity)
  local game = sol.main.get_game()
  local world = entity:get_map():get_world()
  local entity_name = entity:get_name()
  local x, y, layer = entity:get_position()
  x = game:get_value(world .. "_" .. entity_name .. "_x") or x
  y = game:get_value(world .. "_" .. entity_name .. "_y") or y
  layer = game:get_value(world .. "_" .. entity_name .. "_layer") or layer
  return x, y, layer
end

-----------------------
-- Movement tools.
-----------------------

local Node = {}
Node.__index = Node

function Node.new(x, y, parent)
  local self = setmetatable({}, Node)
  self.x = x
  self.y = y
  self.parent = parent
  return self
end

-- Get distance between two nodes
function Node.distance(node_a, node_b)
  return math.sqrt((node_a.x - node_b.x)^2 + (node_a.y - node_b.y)^2)
end

-- Returns whether nodes are equals
function Node.equals(node_a, node_b)
  return node_a.x == node_b.x and node_a.y == node_b.y
end

-- Get closest accessible position not exceeding the distance to target.
function map_tools.get_closest_accessible_position(entity, target_x, target_y)

  local open_list = {}
  local closed_list = {}

  local entity_x, entity_y = entity:get_position()
  local start_node = Node.new(entity_x, entity_y, nil)
  local target_node = Node.new(target_x, target_y, nil)
  local maximum_distance = Node.distance(start_node, target_node)
  local closest_node = start_node

  table.insert(open_list, start_node)

  while #open_list > 0 do

    local current_node = open_list[1]
    table.remove(open_list, 1)
    table.insert(closed_list, current_node)

    if Node.distance(start_node, current_node) <= maximum_distance then

      if Node.distance(current_node, target_node) < Node.distance(closest_node, target_node) then
        closest_node = current_node
      end

      if Node.equals(current_node, target_node) then
        break
      end

      -- Store accessible neighbors.
      local neighbors = {}
      for dx = -1, 1 do
        for dy = -1, 1 do
          if dx ~= 0 or dy ~= 0 then
            local neighbor_x = current_node.x + dx
            local neighbor_y = current_node.y + dy
            local neighbor = Node.new(neighbor_x, neighbor_y, current_node)

            if not entity:test_obstacles(neighbor_x - entity_x, neighbor_y - entity_y) then
              table.insert(neighbors, neighbor)
            end
          end
        end
      end

      for _, neighbor in ipairs(neighbors) do
        local already_in_closed_list = false
        for _, closed_node in ipairs(closed_list) do
          if Node.equals(neighbor, closed_node) then
            already_in_closed_list = true
            break
          end
        end

        if not already_in_closed_list then
          local found_in_open_list = false
          for _, open_node in ipairs(open_list) do
            if Node.equals(neighbor, open_node) then
              found_in_open_list = true
              break
            end
          end

          if not found_in_open_list then
            table.insert(open_list, neighbor)
          end
        end
      end
    end
  end

  return closest_node.x, closest_node.y
end

return map_tools