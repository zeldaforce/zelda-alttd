-- Configuration of the phone manager.
-- Feel free to change these values.
return {
  {
    message_key = 2,
    activation_condition = function(map)
      return
        map:get_game():get_value("first_phone_call")
    end
  },
  {
    message_key = 3,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_1_completed")
    end
  },
  {
    message_key = 4,
    activation_condition = function(map)
      return map:get_game():is_step_done("bowwow_joined")
    end
  },
  {
    message_key = 5,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_2_completed")
    end
  },
    {
    message_key = 6,
    activation_condition = function(map)
      local item = map:get_game():get_item("magnifying_lens")
      local variant = item:get_variant()
      return map:get_game():is_step_done("dungeon_2_completed") and variant <= 3
    end
  },
  {
    message_key = 7,
    activation_condition = function(map)
      local item = map:get_game():get_item("magnifying_lens")
      local variant = item:get_variant()
      return map:get_game():is_step_done("dungeon_2_completed") and variant > 3
    end
  },
  {
    message_key = 8,
    activation_condition = function(map)
      return map:get_game():is_step_done("castle_bridge_built")
    end
  },
  {
    message_key = 9,
    activation_condition = function(map)
      return map:get_game():is_step_done("golden_leaved_returned")
    end
  },
  {
    message_key = 10,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_3_key_obtained")
    end
  },
  {
    message_key = 11,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_3_completed")
    end
  },
  {
    message_key = 11,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_4_key_obtained")
    end
  },
  {
    message_key = 13,
    activation_condition = function(map)
      return map:get_game():is_step_done("dungeon_4_completed")
    end
  },
  {
    message_key = 14,
    activation_condition = function(map)
      return map:get_game():is_step_done("ghost_returned_to_tomb")
    end
  },
  {
    message_key = 15,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5")
    end
  },
  {
    message_key = 16,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6")
    end
  },
  {
    message_key = 17,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6")
        and map:get_game():get_value("possession_melody_3")
    end
  },
  {
    message_key = 18,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6")
        and map:get_game():get_value("possession_melody_3") 
        and map:get_game():get_value("flying_rooster_awakened")
    end
  },
  {
    message_key = 19,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6") 
        and map:get_game():get_value("possession_melody_3")
        and map:get_game():get_value("flying_rooster_awakened")
        and map:get_game():get_value("possession_bird_key") 
    end
  },
  {
    message_key = 20,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6") 
        and map:get_game():get_value("possession_instrument_7")
    end
  },
  {
    message_key = 21,
    activation_condition = function(map)
      return map:get_game():get_value("possession_instrument_5") 
        and map:get_game():get_value("possession_instrument_6") 
        and map:get_game():get_value("possession_instrument_7")
        and map:get_game():get_value("possession_instrument_8")
    end
  },
  {
    message_key = 22,
    activation_condition = function(map)
      local variant_magnifying_lens = map:get_game():get_item("magnifying_lens"):get_variant()
      return map:get_game():is_step_done("wind_fish_egg_opened")
        and variant_magnifying_lens < 10
    end
  },
  {
    message_key = 23,
    activation_condition = function(map)
      local variant_magnifying_lens = map:get_game():get_item("magnifying_lens"):get_variant()
      return map:get_game():is_step_done("wind_fish_egg_opened")
        and variant_magnifying_lens >= 10
    end
  },
  {
    message_key = 24,
    activation_condition = function(map)
      return map:get_game():is_step_done("wind_fish_egg_opened") 
        and map:get_game():get_value("windfish_maze_boss_path_read")
    end
  },
  {
    message_key = 25,
    activation_condition = function(map)
      local variant_sword = map:get_game():get_item("sword"):get_variant()
      return map:get_game():is_step_done("wind_fish_egg_opened") 
        and map:get_game():get_value("windfish_maze_boss_path_read")
        and variant_sword == 2
    end
  },
}