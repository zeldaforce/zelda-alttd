-- This script allows you to show or hide an image on the screen

local preview_picture_manager = {}

local picture_sprite = nil
local draw_picture_sprite = false
local picture_appear = true
local picture_sprite_opacity = 0

function preview_picture_manager:init_map(map)
  
  map:register_event("on_draw", function(map, dst_surface)
    if draw_picture_sprite then
      picture_sprite:set_opacity(picture_sprite_opacity)
      picture_sprite:draw(dst_surface)
      if picture_appear then
        picture_sprite_opacity = picture_sprite_opacity + 20
        if picture_sprite_opacity > 255 then
          picture_sprite_opacity = 255
        end
      else
        picture_sprite_opacity = picture_sprite_opacity - 20
        if picture_sprite_opacity <= 0 then
          draw_picture_sprite = false
        end
      end
    end
  end)
  
end

function preview_picture_manager:show(picture_name)
  
  picture_sprite = sol.sprite.create("pictures/" .. picture_name)
  draw_picture_sprite = true
  
end

function preview_picture_manager:hide()

  draw_picture_sprite = false
  
end

return preview_picture_manager