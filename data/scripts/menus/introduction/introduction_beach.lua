-- Stom cinematic
-- Author: Benjamin Schweitzer

local language_manager = require("scripts/language_manager")
local text_fx_helper = require("scripts/text_fx_helper")
local audio_manager = require("scripts/audio_manager")

local introduction_beach = {}

function introduction_beach:on_started()

  -- Initialize surface.
  self.surface_w = 320
  self.surface_h = 256
  self.surface = sol.surface.create(self.surface_w, self.surface_h)

  introduction_beach.sky = sol.surface.create("menus/title_screen/sky.png")
  introduction_beach.background_mountain = sol.surface.create("menus/title_screen/background_mountain.png")
  introduction_beach.background_trees = sol.surface.create("menus/title_screen/background_trees.png")
  introduction_beach.background_beach = sol.surface.create("menus/title_screen/background_beach.png")
  
  -- Dark surface to make a fade out effect.
  introduction_beach.dark_surface = sol.surface.create(introduction_beach.surface_w, introduction_beach.surface_h)
  introduction_beach.dark_surface:fill_color({0, 0, 0})
  introduction_beach.dark_surface:set_opacity(255)

  -- We don't use a map but rather place manually sprites on the background
  -- and move them manually. The reason is to make this cutscene usable as a
  -- simple menu anywhere and anyhow.
  introduction_beach.clouds = sol.sprite.create("menus/title_screen/clouds")
  introduction_beach.mountain_clouds = sol.sprite.create("menus/title_screen/mountain_clouds")
  introduction_beach.wreck = sol.sprite.create("menus/title_screen/wreck")
  introduction_beach.floating_wood = sol.sprite.create("menus/title_screen/floating_wood")
  introduction_beach.wave_big = sol.sprite.create("menus/title_screen/wave_big")
  introduction_beach.wave_small = sol.sprite.create("menus/title_screen/wave_small")
  introduction_beach.swell = sol.sprite.create("menus/title_screen/swell")
  introduction_beach.clouds_top = sol.surface.create(introduction_beach.surface_w, 64 + introduction_beach.surface_h)
  introduction_beach.clouds_top:fill_color({223, 243, 255})

  -- Configure static seagulls.
  introduction_beach.seagull_1 = sol.sprite.create("entities/animals/seagull")
  introduction_beach.seagull_1:set_animation("stopped")
  introduction_beach.seagull_1:set_direction(0)
    
  introduction_beach.seagull_2 = sol.sprite.create("entities/animals/seagull")
  introduction_beach.seagull_2:set_animation("stopped")
  introduction_beach.seagull_2:set_direction(2)

  -- Configure Marin
  introduction_beach.marin = sol.sprite.create("npc/villagers/marin")
  introduction_beach.marin:set_direction(0)

  -- Configure Link
  introduction_beach.hero = sol.sprite.create("hero/tunic1")
  introduction_beach.hero:set_animation("collapse")
  introduction_beach.hero:set_direction(0)

  -- Modified automatically by this script.
  introduction_beach.elapsed_time = 0 
  introduction_beach.x_offset = 0
  introduction_beach.x_begin = 0
  introduction_beach.x_end = -320
  introduction_beach.total_duration = 800

  -- Preload sounds.
  sol.audio.preload_sounds()

  -- Phases.
  introduction_beach.PHASE_1, introduction_beach.PHASE_2,introduction_beach.PHASE_3, introduction_beach.PHASE_4,introduction_beach.PHASE_5, introduction_beach.PHASE_6, introduction_beach.PHASE_7 = 1, 2, 3, 4, 5, 6, 7   

  -- Launch animation.
  introduction_beach:set_phase(introduction_beach.PHASE_1)
 

end

-- Start the camera animation.
function introduction_beach:move_camera(callback)

  -- Interval between 2 redraw.
  local anim_delta = 1000 / 60 -- ms

  -- Restart the animation.
  introduction_beach.elapsed_time = 0
  if introduction_beach.timer ~= nil then
    introduction_beach.timer:stop()
    introduction_beach.timer = nil
  end

  if sol.menu.is_started(introduction_beach) then
    -- We use a timer called each anim_delta milliseconds.
    -- The timer is called in a loop while the total duration
    -- is below the defined final duration.
    introduction_beach.timer = sol.timer.start(introduction_beach, anim_delta, function()
      -- Elapsed time since launch of animation.
      introduction_beach.elapsed_time = introduction_beach.elapsed_time + anim_delta
      if introduction_beach.elapsed_time < introduction_beach.total_duration then
        -- Keep on updating while time is remaining.
        -- Relaunch the timer once again.
        return true
      else
        -- Call the callback at the end of the animation.
        if callback ~= nil then
          callback()
        end

        -- Stop the timer.
        return false
      end
    end)
  end
end

function introduction_beach:on_draw(dst_surface)

  -- Get the final surface size.
  local width, height = dst_surface:get_size()

  -- Rebuild surface.
  self.surface:clear()

  -- Background.
  local x_offset = math.floor(introduction_beach:get_x_offset(introduction_beach.elapsed_time))
  local x_offset_marin = x_offset
  introduction_beach.x_offset = x_offset
  introduction_beach.sky:draw(introduction_beach.surface, 0, 0)
  local trees_parallax_factor = 1.2
  introduction_beach.background_mountain:draw(introduction_beach.surface, 0 + x_offset, -262.08)
  introduction_beach.background_mountain:draw(introduction_beach.surface, 320 + x_offset, -262.08)
  introduction_beach.background_trees:draw(introduction_beach.surface, 0 + x_offset * trees_parallax_factor, 50.4)
  introduction_beach.background_trees:draw(introduction_beach.surface, 320 + x_offset * trees_parallax_factor, 50.4)
  introduction_beach.background_trees:draw(introduction_beach.surface, 640 + x_offset * trees_parallax_factor, 50.4)
  introduction_beach.background_beach:draw(introduction_beach.surface, 0 + x_offset, 98)
  introduction_beach.background_beach:draw(introduction_beach.surface, 320 + x_offset, 98)

  -- Sprites are placed manually since this is not a map.
  introduction_beach.wreck:draw(introduction_beach.surface, 472 + x_offset, 196)
  introduction_beach.floating_wood:draw(introduction_beach.surface, 528 + x_offset, 232)
  introduction_beach.floating_wood:draw(introduction_beach.surface, 600 + x_offset, 208)
  introduction_beach.wave_big:draw(introduction_beach.surface, 10 + x_offset, 244)
  introduction_beach.wave_big:draw(introduction_beach.surface, 72 + x_offset, 208 )
  introduction_beach.wave_big:draw(introduction_beach.surface, 120 + x_offset, 232)
  introduction_beach.wave_big:draw(introduction_beach.surface, 224 + x_offset, 208)
  introduction_beach.wave_big:draw(introduction_beach.surface, 300 + x_offset, 232)
  introduction_beach.wave_big:draw(introduction_beach.surface, 330 + x_offset, 244)
  introduction_beach.wave_big:draw(introduction_beach.surface, 390 + x_offset, 208)
  introduction_beach.wave_big:draw(introduction_beach.surface, 440 + x_offset, 232)
  introduction_beach.wave_big:draw(introduction_beach.surface, 544 + x_offset, 208)
  introduction_beach.wave_big:draw(introduction_beach.surface, 620 + x_offset, 232)

  introduction_beach.wave_small:draw(introduction_beach.surface, 128 + x_offset, 204)
  introduction_beach.wave_small:draw(introduction_beach.surface, 256 +x_offset, 228)
  introduction_beach.wave_small:draw(introduction_beach.surface, 448 + x_offset, 204)
  introduction_beach.wave_small:draw(introduction_beach.surface, 576 +x_offset, 228)

  -- Static seagulls.
  introduction_beach.seagull_1:draw(introduction_beach.surface, 352 + x_offset, 156)
  introduction_beach.seagull_2:draw(introduction_beach.surface, 502 + x_offset, 168)

  -- Marin
  introduction_beach.marin:draw(introduction_beach.surface, 146 + x_offset - x_offset_marin, 166)

  -- Hero
  introduction_beach.hero:draw(introduction_beach.surface, 480 + x_offset, 166)

  -- Swell
  for i = 1, 20 do
    introduction_beach.swell:draw(introduction_beach.surface, (i - 1) * 32  + x_offset, 184)
  end

  -- Draw surface on destination.
  introduction_beach.surface:draw(dst_surface, (width - introduction_beach.surface_w)/ 2, (height - introduction_beach.surface_h) / 2)

  -- Dark surface.
  introduction_beach.dark_surface:draw(dst_surface, (width - introduction_beach.surface_w)/ 2, (height - introduction_beach.surface_h) / 2)


end

function introduction_beach:on_key_pressed(key)

  if key == "space" or key == "return" then
    if self.timer ~= nil then
      self.timer:stop()
    end
    
    handled = true
    sol.menu.stop(self)
  end

  return false

end

function introduction_beach:on_joypad_button_pressed(button)

  return introduction_beach:on_key_pressed("space")

end

-- We move the camera with a non-linear movement. Since it is not available
-- in Solarus at the time this script was written, the camera movement is
-- made manually, based on a easing function. 
function introduction_beach:get_x_offset(t)

  local delta = introduction_beach.x_end - introduction_beach.x_begin
  if delta == 0 then
    return introduction_beach.x_end
  else
    t = t / introduction_beach.total_duration * 2
    if t < 1 then
      return delta / 2 * math.pow(t, 2) + introduction_beach.x_begin
    else
      return -delta / 2 * ((t - 1) * (t - 3) - 1) + introduction_beach.x_begin
    end
  end
end

-- Change the phase of the cutscene.
-- Possible phases (in order):
-- 1. "begin"
-- 2. "moving"
-- 3. "end"
function introduction_beach:set_phase(phase)

  introduction_beach.phase = phase
  -- Phase 1: wait a bit before launching a vertical scroll.
  if phase == introduction_beach.PHASE_1 then
    introduction_beach.elapsed_time = 0
    introduction_beach.dark_surface:fade_out(100)
    -- Marin configuration
    introduction_beach.marin:set_animation("walking")
    -- Animation parameters.
    introduction_beach.x_begin = 0
    introduction_beach.x_end = -120
    introduction_beach.x_offset = introduction_beach.x_begin
    introduction_beach.total_duration = 10000

    -- Launch animation phase.
    introduction_beach:move_camera(function()
      -- Go to next phase.
      introduction_beach:set_phase(introduction_beach.phase + 1)
    end)
  -- Phase 2: Marin is stopped
  elseif phase == introduction_beach.PHASE_2 then
    -- Marin configuration
    introduction_beach.marin:set_animation("stopped")
    sol.timer.start(introduction_beach, 3000, function()
      introduction_beach:set_phase(introduction_beach.phase + 1)
    end)
  -- Phase 3: scroll to the top of the mountain.
  elseif phase == introduction_beach.PHASE_3 then
    -- Marin configuration
    introduction_beach.marin:set_animation("walking")
    -- Animation parameters.
    introduction_beach.x_begin = -120
    introduction_beach.x_end = -280
    introduction_beach.x_offset = introduction_beach.x_begin
    introduction_beach.total_duration = 6000


    -- Launch animation phase.
    introduction_beach:move_camera(function()
      -- Go to next phase.
      introduction_beach:set_phase(introduction_beach.phase + 1)
    end)
  elseif phase == introduction_beach.PHASE_4 then
    -- Marin configuration
    introduction_beach.marin:set_animation("stopped")
    sol.timer.start(introduction_beach, 3000, function()
      introduction_beach:set_phase(introduction_beach.phase + 1)
    end)
  elseif phase == introduction_beach.PHASE_5 then
    -- Marin configuration
    introduction_beach.marin:set_animation("walking")
    -- Animation parameters.
    introduction_beach.x_begin = -280
    introduction_beach.x_end = -320
    introduction_beach.x_offset = introduction_beach.x_begin
    introduction_beach.total_duration = 4000

    -- Launch animation phase.
    introduction_beach:move_camera(function()
      -- Go to next phase.
      introduction_beach:set_phase(introduction_beach.phase + 1)
    end)
  elseif phase == introduction_beach.PHASE_6 then
    -- Marin configuration
    introduction_beach.marin:set_animation("stopped")
    sol.timer.start(introduction_beach, 7000, function()
      sol.menu.stop(self)
    end)
  end

end

return introduction_beach