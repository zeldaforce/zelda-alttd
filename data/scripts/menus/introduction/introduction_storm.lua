-- Stom cinematic
-- Author: Benjamin Schweitzer

local language_manager = require("scripts/language_manager")
local text_fx_helper = require("scripts/text_fx_helper")
local audio_manager = require("scripts/audio_manager")

local introduction_storm = {}

function introduction_storm:on_started()

  -- Initialize surface.
  self.surface_w = 320
  self.surface_h = 256
  self.surface = sol.surface.create(self.surface_w, self.surface_h)

end

function introduction_storm:on_draw(dst_surface)

  -- Rebuild surface.
  self.surface:clear()

end

function introduction_storm:on_key_pressed(key)

  if key == "space" or key == "return" then
    if self.timer ~= nil then
      self.timer:stop()
    end
    
    handled = true
    sol.menu.stop(self)
  end

  return false

end

function introduction_storm:on_joypad_button_pressed(button)

  return introduction_storm:on_key_pressed("space")

end

return introduction_storm
