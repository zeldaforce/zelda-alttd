-- Title screen.
-- Author: Benjamin Schweitzer

local introduction = {}

local introduction_storm = require("scripts/menus/introduction/introduction_storm")
local introduction_beach = require("scripts/menus/introduction/introduction_beach")
local audio_manager = require("scripts/audio_manager")
local multi_events = require("scripts/multi_events")

function introduction:on_started()
  -- Play music.
  audio_manager:play_music("01_introduction")

  self.finished = false

  -- Register a callback for when the logo is finished.
  -- The logo will end when the user press Space (or any key).
  multi_events:enable(introduction_storm)
  multi_events:enable(introduction_beach)

  local storm_duration = 18000
  local beach_duration = 32000
  sol.timer.start(self, storm_duration, function() -- Todo change this when cinematic is finished
    sol.menu.start(self, introduction_beach, true)
      sol.timer.start(self, beach_duration, function()
        self.finished = true
        sol.menu.stop(self)
      end)
  end)
  sol.menu.start(self, introduction_storm, true)

end

function introduction:on_key_pressed(key)

  if key == "escape" then
    -- Stop the program.
    sol.main.exit()
    
  elseif (key == "space" or key == "return") and not self.finished then
    self.finished = true
    sol.menu.stop(self)
  end

  return handled

end

return introduction

