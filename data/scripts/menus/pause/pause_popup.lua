local submenu = require("scripts/menus/pause/pause_submenu")
local popup = {}

local audio_manager = require("scripts/audio_manager")

local item_names_popup = {
  ocarina = {
    "melody_1",
    "melody_2",
    "melody_3"
  },
  vfx = {
    "vfx_none",
    "vfx_gb",
    "vfx_fsa"
  }
}

function popup:show(context, type, backup_command_action_effect)

  self.context = context
  self.type = type
  self.backup_command_action_effect = backup_command_action_effect
  sol.menu.start(context, self, true)

end

function popup:hide()

  self.game:set_custom_command_effect('action', self.backup_command_action_effect)
  sol.menu.stop(self)

end

function popup:on_started()

 self.game = sol.main.get_game()
 self.sprites_popup = {}
 self.cursor_sprite = sol.sprite.create("menus/pause/cursor")
 self:set_cursor_position(1)
 local i = 1
  for category_name, items in pairs(item_names_popup) do
    for _, item_name in ipairs(items) do
      local item = self.game:get_item(item_name)
      if item then
        local variant = item:get_variant()
        if variant > 0 then
          if not self.sprites_popup[category_name] then
            self.sprites_popup[category_name] = {}
          end
          self.sprites_popup[category_name][item_name] = sol.sprite.create("entities/items")
          self.sprites_popup[category_name][item_name]:set_animation(item_name)
          self.sprites_popup[category_name][item_name]:set_direction(variant - 1)
          i = i + 1
        end
      end
    end
  end

end

function popup:on_finished()
  -- Nothing.
end

function popup:on_draw(dst_surface)


  local width, height = dst_surface:get_size()
  local popup_width = 116
  local popup_height = 52
  local popup_slot_width = 28
  local popup_slot_height = 28
  local popup_img = sol.surface.create("menus/pause/popup_frame.png")
  local popup_x = (width - popup_width) / 2
  local popup_y = (height - popup_height) / 2

  popup_img:draw_region(0, 0, popup_width, popup_height, dst_surface, popup_x, popup_y) 
  for i = 1, 3 do
    local popup_slot = sol.surface.create("menus/pause/popup_slot.png")
    popup_slot:draw_region(0, 0, popup_slot_width, popup_slot_height, dst_surface, popup_x + i * 8 +  (i - 1) * popup_slot_width, popup_y + (popup_height - popup_slot_height) / 2) 
  end
  local i = 1
  for key, name in pairs(item_names_popup[self.type]) do
    local item = self.game:get_item(name)
    if item:get_variant() > 0 then
        self.sprites_popup[self.type][name]:set_direction(item:get_variant() - 1)
        self.sprites_popup[self.type][name]:draw(dst_surface, 14 + popup_x + i * 8 +  (i - 1) * popup_slot_width, 18 + popup_y + (popup_height - popup_slot_height) / 2) 
    end
    i = i + 1
  end

  -- Draw cursor
  self.cursor_sprite:draw(dst_surface, 14 + popup_x + self.cursor_column * 8 + (self.cursor_column - 1) * popup_slot_width, 14 + popup_y + (popup_height - popup_slot_height) / 2)

end

function popup:on_command_pressed(command)

  local handled = false
  if not handled then
    local item_name = self:get_item_name(self.cursor_column)
    local item = self.game:get_item(item_name)
    if command == "action"  then
      if not item:is_assignable() and item.start_effect then
         item:start_effect()
         self:hide()
      else
          self:show_info_message()
      end
      return true
    elseif command == "attack"  then
      self:hide()
      return true
    elseif command == "item_1" and item:is_assignable() then
      if self:is_item_selected() then
        self.context:assign_item(1, item_name)
        handled = true
      end
      self:hide()
      return true
    elseif command == "item_2" and item:is_assignable() then
      if self:is_item_selected() then
        self.context:assign_item(2, item_name)
        handled = true
      end
      self:hide()
      return true
    elseif command == "left"  then
      if self.cursor_column == 1 then
        self.cursor_column = 4
      end
      audio_manager:play_sound("menus/menu_cursor")
      self:set_cursor_position(self.cursor_column - 1)
      handled = true
      return true
    elseif command == "right"  then
      if self.cursor_column == 3 then
        self.cursor_column = 0
      end
      audio_manager:play_sound("menus/menu_cursor")
      self:set_cursor_position(self.cursor_column + 1)
      handled = true
      return true
    end
  end

  return handled

end

function popup:get_item_name(column)

  return item_names_popup[self.type][column]

end

function popup:set_cursor_position(column)
  self.cursor_column = column
  local index
  local item_name

  -- Update the caption text and the action icon.
  local item_name = self:get_item_name(column)
  local item = item_name and self.game:get_item(item_name) or nil
  local variant = item and item:get_variant()
  local item_icon_opacity = 128
  if variant > 0 then
    --submenu:set_caption_key("inventory.caption.item." .. item_name .. "." .. variant)
    if self.type == "vfx" then
      self.game:set_custom_command_effect("action", "action")
    else
      self.game:set_custom_command_effect("action", "info")
    end
    if item:is_assignable() then
      self.game:set_hud_mode("normal")
    else
      self.game:set_hud_mode("pause")
    end
  else
    --self:set_caption_key(nil)
    --self.game:set_custom_command_effect("action", nil)
    self.game:set_hud_mode("pause")
  end

end

-- Shows a message describing the item currently selected.
-- The player is supposed to have this item.
function popup:show_info_message()

  local item_name = self:get_item_name(self.cursor_column)
  local variant = self.game:get_item(item_name):get_variant()
  local dialog_id = "scripts.menus.pause_inventory." .. item_name .. "." .. variant
  self.game:start_dialog(dialog_id)

end

function popup:is_item_selected()

  local item_name = self:get_item_name(self.cursor_column)

  return self.game:get_item(item_name):get_variant() > 0

end


return popup