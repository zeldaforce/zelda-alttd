local submenu = require("scripts/menus/pause/pause_submenu")
local popup = require("scripts/menus/pause/pause_popup")
local audio_manager = require("scripts/audio_manager")

local inventory_submenu = submenu:new()
local item_names_assignable = {
  "shield",
  "mushroom",
  "ocarina",
  "feather",
  "bombs_counter",
  "shovel",
  "hookshot",
  "bow",
  "fire_rod",
  "hammer"
}
local item_names_static = {
  "tunic",
  "sword",
  "power_bracelet",
  "flippers",
  "drug",
  "pegasus_shoes",
  "vfx"
}

function inventory_submenu:on_started()

  submenu.on_started(self)
      
  -- Set title
  self:set_title(sol.language.get_string("inventory.title"))

  self.cursor_sprite = sol.sprite.create("menus/pause/cursor")
  self.sprites_assignables = {}
  self.sprites_static = {}
  self.captions = {}
  self.counters = {}
  if self.game:has_item("magic_powder_counter") and self.game:get_item("magic_powder_counter"):get_amount() > 0 then
    item_names_assignable[2] = "magic_powder_counter"
  else
    item_names_assignable[2] = "mushroom"
  end
  if self.game:get_value("get_boomerang") then
    item_names_assignable[6] = "boomerang"
  end

  -- Initialize the cursor
  local index = self.game:get_value("pause_inventory_last_item_index") or 0
  local row = math.floor(index / 7)
  local column = index % 7
  self:set_cursor_position(row, column)
  
  -- Load Items
  for i,item_name in ipairs(item_names_assignable) do
    local item = self.game:get_item(item_name)
    local variant = item:get_variant()
    self.sprites_assignables[i] = sol.sprite.create("entities/items")
    self.sprites_assignables[i]:set_animation(item_name)
    if item:has_amount() then
      -- Show a counter in this case.
      local amount = item:get_amount()
      local maximum = item:get_max_amount()
      self.counters[i] = sol.text_surface.create{
        horizontal_alignment = "center",
        vertical_alignment = "top",
        text = item:get_amount(),
        font = (amount == maximum) and "green_digits" or "white_digits",
      }
    end
  end
  
  for i,item_name in ipairs(item_names_static) do
    local item = self.game:get_item(item_name)
    local variant = item:get_variant()
    if variant > 0 then
      self.sprites_static[i] = sol.sprite.create("entities/items")
      self.sprites_static[i]:set_animation(item_name)
      self.sprites_static[i]:set_direction(variant - 1)
    end
  end

end

function inventory_submenu:on_finished()
  -- Nothing.
end

function inventory_submenu:on_draw(dst_surface)

  local cell_size = 26
  local cell_spacing = 6

  local width, height = dst_surface:get_size()
  local center_x = width / 2
  local center_y = height / 2
  local menu_x, menu_y = center_x - self.width / 2, center_y - self.height / 2

  -- Draw the background.
  self:draw_background(dst_surface)
  
  -- Draw the cursor caption.
  self:draw_caption(dst_surface)

  -- Draw each inventory static item.
  local y = menu_y + 68
  local k = 0
  for i = 0, 2 do
    local x = menu_x + 26
    if i == 2 then
      x = menu_x + 26 + cell_size + cell_spacing
    end
    y = y + cell_size + cell_spacing
    for j = 0, 2 do
      x = x + cell_size + cell_spacing
      k = k + 1
      if item_names_static[k] ~= nil then
        local item = self.game:get_item(item_names_static[k])
        if item:get_variant() > 0 then
          -- The player has this item: draw it.
          self.sprites_static[k]:draw(dst_surface, x, y)
        end
      end
    end
  end

  -- Draw each inventory assignable item.
  local y = menu_y + 68
  local k = 0
  for i = 0, 2 do
    local x = menu_x + 134
    if i == 2 then
      x = menu_x + 134 + cell_size + cell_spacing
    end
    y = y + cell_size + cell_spacing
    for j = 0, 3 do
      x = x + cell_size + cell_spacing
      k = k + 1
      if item_names_assignable[k] ~= nil then
        local item = self.game:get_item(item_names_assignable[k])
        if item:get_variant() > 0 then
          -- The player has this item: draw it.
          self.sprites_assignables[k]:set_direction(item:get_variant() - 1)
          self.sprites_assignables[k]:draw(dst_surface, x, y)
          if self.counters[k] ~= nil then
            self.counters[k]:draw(dst_surface, x + 8, y)
          end
        end
      end
    end
  end

  -- Draw cursor only when the save dialog is not displayed.
  if not sol.menu.is_started(popup) then
    if not self.dialog_opened then
      local offsetX = 0
      if self.cursor_column > 2 then
        offsetX = 12
      end
      self.cursor_sprite:draw(dst_surface, menu_x + offsetX + 58 + 32 * self.cursor_column, menu_y + 96 + 32 * self.cursor_row)
    end
  end

  -- Draw the item being assigned if any.
  if self:is_assigning_item() then
    self.item_assigned_sprite:draw(dst_surface)
  end

end

function inventory_submenu:on_command_pressed(command)
  local handled = submenu.on_command_pressed(self, command) or sol.menu.is_started(popup)

  if not handled then
    local item_name = self:get_item_name(self.cursor_row, self.cursor_column)
    if command == "action"  then
      if self.game:get_command_effect("action") == nil and self.game:get_custom_command_effect("action") == "info" then
        if item_name == "vfx" then
          handled = true
        --  self:set_caption_key(nil)
          popup:show(self.game, "vfx", self.game:get_custom_command_effect("action"))
        elseif not self.dialog_opened then
          handled = true
          self:show_info_message()
        else
          handled = false
        end
      end

    elseif command == "item_1" then
      if item_name == "ocarina" 
        and self.game:get_item("ocarina"):get_variant() > 0 
        and (self.game:get_item("melody_1"):get_variant() > 0 
            or self.game:get_item("melody_2"):get_variant() > 0  
            or self.game:get_item("melody_2"):get_variant() > 0 
        ) then
        self:set_caption_key(nil)
        popup:show(self, "ocarina", self.game:get_custom_command_effect("action"))
      elseif self:is_item_selected() then
        self:assign_item(1)
        handled = true
      end
      return true
    elseif command == "item_2" then
      if item_name == "ocarina" 
        and self.game:get_item("ocarina"):get_variant() > 0 
        and (self.game:get_item("melody_1"):get_variant() > 0 
            or self.game:get_item("melody_2"):get_variant() > 0  
            or self.game:get_item("melody_2"):get_variant() > 0 
        ) then
        self:set_caption_key(nil)
        popup:show(self, "ocarina", self.game:get_custom_command_effect("action"))
      elseif self:is_item_selected() then
        self:assign_item(2)
        handled = true
      end
      return true
    elseif command == "left"  then
      local limit = 0
      if self.cursor_row == 2 then -- Exception last row
        limit = 1
      end
      if self.cursor_column == limit then
        self:previous_submenu()
      else
        audio_manager:play_sound("menus/menu_cursor")
        if self.cursor_row == 2 and self.cursor_column == 4 then -- Exception last row
          self.cursor_column = self.cursor_column - 2
        end
        self:set_cursor_position(self.cursor_row, self.cursor_column - 1)
      end
      handled = true

    elseif command == "right"  then
      local limit = 6
      if self.cursor_row == 2 then -- Exception last row
        limit = 5
      end
      if self.cursor_column == limit then
        self:next_submenu()
      else
        audio_manager:play_sound("menus/menu_cursor")
        if self.cursor_row == 2 and self.cursor_column == 1 then -- Exception last row
          self.cursor_column = self.cursor_column + 2
        end
        self:set_cursor_position(self.cursor_row, self.cursor_column + 1)
      end
      handled = true

    elseif command == "up" then
      audio_manager:play_sound("menus/menu_cursor")
      if self.cursor_column == 1 or self.cursor_column == 4 or self.cursor_column == 5 then
        self:set_cursor_position((self.cursor_row - 1) % 3, self.cursor_column)
      else
        self:set_cursor_position((self.cursor_row - 1) % 2, self.cursor_column)
      end
      handled = true

    elseif command == "down" then
      audio_manager:play_sound("menus/menu_cursor")
      if self.cursor_column == 1  or self.cursor_column == 4  or self.cursor_column == 5 then
        self:set_cursor_position((self.cursor_row + 1) % 3, self.cursor_column)
      else
        self:set_cursor_position((self.cursor_row + 1) % 2, self.cursor_column)
      end
      handled = true

    end
  end

  return handled

end

-- Shows a message describing the item currently selected.
-- The player is supposed to have this item.
function inventory_submenu:show_info_message()

  local item_name = self:get_item_name(self.cursor_row, self.cursor_column)
  local variant = self.game:get_item(item_name):get_variant()
  local dialog_id = "scripts.menus.pause_inventory." .. item_name .. "." .. variant
  self:show_info_dialog(dialog_id, function()
    -- Re-update the cursor and buttons.
    self:set_cursor_position(self.cursor_row, self.cursor_column)  
  end)
end

function inventory_submenu:set_cursor_position(row, column)
  self.cursor_row = row
  self.cursor_column = column
  local index
  local item_name
  self.game:set_value("pause_inventory_last_item_index", index)

  -- Update the caption text and the action icon.
  local item_name = self:get_item_name(row, column)
  local item = item_name and self.game:get_item(item_name) or nil
  local variant = item and item:get_variant()
  local item_icon_opacity = 128
  if variant > 0 then
    self:set_caption_key("inventory.caption.item." .. item_name .. "." .. variant)
    self.game:set_custom_command_effect("action", "info")
    if item:is_assignable() then
      self.game:set_hud_mode("normal")
    else
      self.game:set_hud_mode("pause")
    end
  else
    self.game:set_custom_command_effect("action", nil)
    self.game:set_hud_mode("pause")
  end

end

function inventory_submenu:get_item_name(row, column)

   if column < 3 then
       index = row * 3 + column + 1
       if index == 8 then
        index = index - 1
       end
       item_name = item_names_static[index]
  else
       index = row * 4 + column - 2
       if index == 10 or index == 11 then
        index = index - 1
       end
       item_name = item_names_assignable[index]
  end

  return item_name

end

function inventory_submenu:is_item_selected()

  local item_name = self:get_item_name(self.cursor_row, self.cursor_column)

  return self.game:get_item(item_name):get_variant() > 0

end

-- Assigns the selected item to a slot (1 or 2).
-- The operation does not take effect immediately: the item picture is thrown to
-- its destination icon, then the assignment is done.
-- Nothing is done if the item is not assignable.
function inventory_submenu:assign_item(slot, item_name)

  if not item_name then
    item_name = self:get_item_name(self.cursor_row, self.cursor_column)
  end
  local item = self.game:get_item(item_name)
  local assignable = false

  -- If this item is not assignable, do nothing.
  if not item:is_assignable() then
    return
  end
  -- If another item is being assigned, finish it immediately.
  if self:is_assigning_item() then
    self:finish_assigning_item()
  end

  if item:is_assignable() and item:get_variant() > 0 then
    -- Memorize this item.
    self.item_assigned = item
    self.item_assigned_sprite = sol.sprite.create("entities/items")
    self.item_assigned_sprite:set_animation(item_name)
    self.item_assigned_sprite:set_direction(item:get_variant() - 1)
    self.item_assigned_destination = slot

    -- Play the sound.
    audio_manager:play_sound("menus/menu_select")

    -- Compute the movement.
    local x1 = 63 + 32 * self.cursor_column
    local y1 = 90 + 32 * self.cursor_row

    local x2 = (slot == 1) and 20 or 72
    local y2 = 24

    self.item_assigned_sprite:set_xy(x1, y1)
    local movement = sol.movement.create("target")
    movement:set_target(x2, y2)
    movement:set_speed(300)
    movement:start(self.item_assigned_sprite, function()
      self:finish_assigning_item()
    end)
  end

end

-- Returns whether an item is currently being thrown to an icon.
function inventory_submenu:is_assigning_item()

  return self.item_assigned_sprite ~= nil

end

-- Stops assigning the item right now.
-- This function is called when we want to assign the item without
-- waiting for its throwing movement to end, for example when the inventory submenu
-- is being closed.
function inventory_submenu:finish_assigning_item()

  -- If the item to assign is already assigned to the other icon, switch both items.
  local slot = self.item_assigned_destination
  local current_item = self.game:get_item_assigned(slot)
  local other_item = self.game:get_item_assigned(3 - slot)

  if other_item == self.item_assigned then
    self.game:set_item_assigned(3 - slot, current_item)
  else
    if current_item and current_item.on_unassigned then
      current_item:on_unassigned()
    end
  end

  self.game:set_item_assigned(slot, self.item_assigned)
  if self.item_assigned.on_assigned then
    self.item_assigned:on_assigned(slot)
  end

  self.item_assigned_sprite:stop_movement()
  self.item_assigned_sprite = nil
  self.item_assigned = nil
end

return inventory_submenu
