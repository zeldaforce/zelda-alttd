-- Animated Zeldaforce logo by Olivier Cléro.
-- Version 1.0

-- Includes scripts
local audio_manager = require("scripts/audio_manager")

local zeldaforce_logo_menu = {}

----------------------------------------------------------

-- Starting the menu.
function zeldaforce_logo_menu:on_started()

  self.stopped = false
  -- Load logo.
  self.logo_w = 96
  self.logo_h = 96
  self.logo = sol.sprite.create("menus/zeldaforce_logo/logo")
  -- Start the final timer.
  sol.timer.start(zeldaforce_logo_menu, 1650, function()
    self.logo:set_animation("stopped")
    sol.audio.play_sound("gb/misc/owl_hoot2")
    sol.timer.start(zeldaforce_logo_menu, 2000, function()
      self.logo:fade_out()
      sol.timer.start(zeldaforce_logo_menu, 1000, function()
        sol.menu.stop(zeldaforce_logo_menu)
      end)
    end)
  end)


end

-- Draws this menu on the quest screen.
function zeldaforce_logo_menu:on_draw(dst_surface)

  -- Simply draws the surface at the center of the screen.
  local dst_w, dst_h = dst_surface:get_size()
  self.logo:draw(dst_surface, (dst_w - self.logo_w) / 2, (dst_h - self.logo_h) / 2)

end

-- Called when a keyboard key is pressed.
function zeldaforce_logo_menu:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
    return true
  elseif not self.stopped then
    return self:skip_menu()
  end
  
  return false
end

-- Mouse pressed: skip menu.
function zeldaforce_logo_menu:on_mouse_pressed(button, x, y)
  if button == "left" or button == "right" then
    return self:skip_menu()
  end
end

function zeldaforce_logo_menu:skip_menu()

  self.stopped = true
  self.logo:set_animation("stopped")
  self.logo:fade_out()
  sol.timer.start(zeldaforce_logo_menu, 1000, function()
    sol.menu.stop(zeldaforce_logo_menu)
  end)

end




----------------------------------------------------------

-- Return the menu to the caller.
return zeldaforce_logo_menu
