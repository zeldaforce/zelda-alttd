-- Flame projectile, mainly used by the flame fountain entity.
-- Shield level 2 protect from it.

local enemy = ...
require("enemies/lib/common_actions").learn(enemy)

-- Global variables
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite = enemy:create_sprite("entities/misc/flame_fountain")
local eighth = math.pi * 0.25

-- Configuration variables
local throwing_speed = 60
local throwing_maximum_distance = 88
local before_sprite_growing_duration = 300
local deflected_speed = 60
local deflected_maximum_distance = 20

-- Throwing behavior.
local function start_throw()

  -- Initial movement
  local movement = sol.movement.create("straight")
  movement:set_angle(6.0 * eighth)
  movement:set_speed(throwing_speed)
  movement:set_smooth(false)
  movement:set_ignore_obstacles(true)
  movement:set_max_distance(throwing_maximum_distance)
  movement:start(enemy)

  function movement:on_finished()
    enemy:start_death()
  end

  -- Sprite evolution
  sprite:set_animation("small")
  sol.timer.start(enemy, before_sprite_growing_duration, function()
    sprite:set_animation("medium")
    sol.timer.start(enemy, before_sprite_growing_duration, function()
      sprite:set_animation("big")
    end)
  end)
end

-- Split and deflect the flame on touching shield
local function on_touching_shield()

  -- Do nothing with the first shield.
  if not hero:is_shield_protecting(enemy) then
    enemy:on_attacking_hero(hero, sprite)
    return
  end
  enemy:stop_all()

  -- Deflect animation.
  enemy:remove_sprite()
  local deflected_sprites = {
    left = enemy:create_sprite("entities/misc/flame_fountain"),
    right = enemy:create_sprite("entities/misc/flame_fountain")
  }
  deflected_sprites["left"]:set_direction(2)
  for sprite_name, sprite in pairs(deflected_sprites) do
    sprite:set_animation("deflected")

    local movement = sol.movement.create("straight")
    movement:set_angle((sprite_name == "left" and 5.0 or 7.0) * eighth)
    movement:set_speed(deflected_speed)
    movement:set_smooth(false)
    movement:set_ignore_obstacles(true)
    movement:set_max_distance(deflected_maximum_distance)
    movement:start(sprite)

    function movement:on_finished()
      enemy:start_death()
    end
  end

  -- Repulse the hero.
  enemy:start_pushing_back(hero, 150, 100)
end

-- Push the hero far away to avoid brute forcing the obstacle. 
local function start_pushing_hero_far_away()

  hero:start_hurt(enemy, sprite, enemy:get_damage())

  local function move_hero_to_south()
    x, y = hero:get_position()
    if not hero:test_obstacles(0, 1) then
      hero:set_position(x, y + 1)
    end
  end

  local total_time = 0
  sol.timer.start(hero, 2, function()
    total_time = total_time + 2
    if total_time < 350 then
      move_hero_to_south()
      return true
    end
  end)
end

-- Also push the hero far away on normal collision. 
enemy:register_event("on_attacking_hero", function(enemy, hero, enemy_sprite)

  if not hero:is_shield_protecting(enemy) and not hero:is_blinking() then
    start_pushing_hero_far_away()
  end
end)

-- Restart settings.
enemy:register_event("on_restarted", function(enemy)

  enemy:set_hero_weapons_reactions({
  	arrow = "ignored",
  	boomerang = "ignored",
  	explosion = "ignored",
  	sword = "ignored",
  	thrown_item = "ignored",
  	fire = "ignored",
  	jump = start_pushing_hero_far_away,
  	hammer = "ignored",
  	hookshot = "ignored",
  	magic_powder = "ignored",
  	shield = on_touching_shield,
  	thrust = start_pushing_hero_far_away
  })

  -- States.
  enemy:set_shield_minimum_level(2)
  enemy:set_obstacle_behavior("flying")
  enemy:set_can_attack(true)
  enemy:set_damage(10)

  start_throw()
end)
