----------------------------------
--
-- Darknut.
--
-- Moves randomly over horizontal and vertical axis, and charge the hero if close enough.
-- Turn his head to the next direction before starting a new random move.
-- May starts as not hunting if the "is_hunting" property is set to "false", which prevent the enemy to chase the hero if too close.
--
-- Methods : enemy:start_hunting()
--
----------------------------------

-- Global variables
local enemy = ...
require("enemies/lib/common_actions").learn(enemy)
require("enemies/lib/weapons").learn(enemy)

local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
local quarter = math.pi * 0.5
local is_hunting = enemy:get_property("is_hunting") ~= "false"
local is_charging = false

-- Configuration variables
local charge_triggering_distance = 80
local charging_speed = 56
local walking_angles = {0, quarter, 2.0 * quarter, 3.0 * quarter}
local walking_speed = 32
local walking_minimum_distance = 16
local walking_maximum_distance = 96
local waiting_duration = 800

-- Start the enemy charge movement.
local function start_charging()

  is_charging = true
  enemy:stop_movement()
  enemy:start_target_walking(hero, charging_speed)
  sprite:set_animation("chase")
end

-- Start the enemy random movement.
local function start_walking(direction)

  direction = direction or math.random(4)
  enemy:start_straight_walking(walking_angles[direction], walking_speed, math.random(walking_minimum_distance, walking_maximum_distance), function()
    local next_direction = math.random(4)
    local waiting_animation = (direction + 1) % 4 == next_direction and "seek_left" or (direction - 1) % 4 == next_direction and "seek_right" or "immobilized"
    sprite:set_animation(waiting_animation)

    sol.timer.start(enemy, waiting_duration, function()
      if not is_charging then
        start_walking(next_direction)
      end
    end)
  end)
end

-- Starts hunting to chase the hero if near enough.
function enemy:start_hunting()
  is_hunting = true
end

-- Passive behaviors needing constant checking.
enemy:register_event("on_update", function(enemy)

  if enemy:is_immobilized() then
    return
  end

  -- Start charging if the hero is near enough
  if is_hunting and not is_charging and enemy:is_near(hero, charge_triggering_distance) then
    start_charging()
  end
end)

-- Initialization.
enemy:register_event("on_created", function(enemy)

  enemy:set_invincible() -- Create as invincible to not be hurt immediately when created over explosions (such as created from a darknut statue), as collisions tests would be triggered by the size and origin initialization.
  enemy:set_life(2)
  enemy:set_size(16, 16)
  enemy:set_origin(8, 13)
  enemy:hold_weapon()
end)

-- Restart settings.
enemy:register_event("on_restarted", function(enemy)

  enemy:set_hero_weapons_reactions({
  	arrow = 2,
  	boomerang = 2,
  	explosion = 2,
  	sword = 1,
  	thrown_item = 2,
  	fire = 2,
  	jump = "ignored",
  	hammer = 2,
  	hookshot = 2,
  	magic_powder = 2,
  	shield = "protected",
  	thrust = 2
  })

  -- States.
  enemy:set_can_attack(true)
  enemy:set_damage(1)
  if is_charging then
    start_charging()
  else
    start_walking()
  end
end)
