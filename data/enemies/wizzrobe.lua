-- Lua script of enemy wizzrobe.
-- This script is executed every time an enemy with this model is created.

-- Variables
local enemy = ...
local map = enemy:get_map()
local hero = map:get_hero()
local children = {}
local is_appear = false
local previous_on_removed = enemy.on_removed

-- Include scripts
local audio_manager = require("scripts/audio_manager")

-- Make the enemy vulnerable and hurtful.
local function set_vulnerable()

  enemy:set_hero_weapons_reactions({
  	arrow = 1,
  	boomerang = "immobilized",
  	explosion = 4,
  	sword = "protected",
  	thrown_item = "protected",
  	fire = 1,
  	jump = "ignored",
  	hammer = "protected",
  	hookshot = "immobilized",
  	magic_powder = "immobilized",
  	shield = "protected",
  	thrust = "protected"
  })
end

local function update_attack()

  if is_appear then
    set_vulnerable()
  else
    enemy:set_invincible()
  end
  enemy:set_can_attack(is_appear)
end

-- The enemy appears: set its properties.
enemy:register_event("on_created", function(enemy)

  enemy:set_life(4)
  enemy:set_damage(4)
  enemy:set_can_attack(false)
  enemy:create_sprite("enemies/" .. enemy:get_breed())
end)

-- Wizzrobe appear
function enemy:appear()

  local sprite = enemy:get_sprite()
  local direction = enemy:get_direction4_to(hero)
  sprite:set_direction(direction)
  enemy:set_visible(true)
  sprite:fade_in(20, function()
    is_appear = true
    update_attack()
    enemy:shoot()
  end)
end

-- Wizzrobe disappear
function enemy:disappear()

  local sprite = enemy:get_sprite()
  sprite:fade_out(20, function()
    is_appear = false
    update_attack()
    sol.timer.start(enemy, 2000, function()
      enemy:appear()
    end)
  end)
end

function enemy:shoot()

  local sprite = enemy:get_sprite()
  sprite:set_animation("walking")
  local hero = map:get_hero()
  if enemy:get_distance(hero) > 200 or not enemy:is_in_same_region(hero) then
    return true 
  end

  local sprite = enemy:get_sprite()
  local x, y, layer = enemy:get_position()
  local direction = sprite:get_direction()

  -- Where to start the beam from.
  local dxy = {
    {  0, -5 },
    {  0, -5 },
    {  0, -5 },
    {  0, -5 },
  }

  local beam = enemy:create_enemy({
    name = (enemy:get_name() or enemy:get_breed()) .. "_beam",
    breed = "projectiles/beam",
    x = dxy[direction + 1][1],
    y = dxy[direction + 1][2],
  })
  children[#children + 1] = beam

  if not map.wizzrobe_recent_sound then
    audio_manager:play_entity_sound(enemy, "enemies/wizzrobe")
    -- Avoid loudy simultaneous sounds if there are several wizzrobes.
    map.wizzrobe_recent_sound = true
    sol.timer.start(map, 200, function()
      map.wizzrobe_recent_sound = nil
    end)
  end
  beam:go(direction)
  sol.timer.start(enemy, 2000, function()
    enemy:disappear()
  end)
end

-- The enemy was stopped for some reason and should restart.
enemy:register_event("on_restarted", function(enemy)

  enemy:set_can_attack(is_appear)
  enemy:set_visible(is_appear)
  if is_appear == false then
    sol.timer.start(enemy, 2000, function()
      enemy:appear()
    end)
  else
    enemy:disappear()
  end
end)

enemy:register_event("on_removed", function(enemy)

  if previous_on_removed then
    previous_on_removed(enemy)
  end

  for _, child in ipairs(children) do
    child:start_death()
  end
end)
