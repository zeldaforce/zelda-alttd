----------------------------------
--
-- Turtle Rock.
--
-- Enemy that starts immobile, invincible and hurtless until enemy:start_awakening() is called from the outside.
-- Start swinging from left to right once woke up and attacks from time to time at the end of a movement.
--
-- Methods : start_awakening([on_finished_callback])
--           start_swinging()
--
----------------------------------

-- Global variables
local enemy = ...
local audio_manager = require("scripts/audio_manager")
local map_tools = require("scripts/maps/map_tools")
local common_actions = require("enemies/lib/common_actions")
common_actions.learn(enemy)

local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
local head_routine_animation
local quarter = math.pi * 0.5
local neck_entities = {}
local initial_position = {}
local step = 1
local is_awake = false
local is_hurt = false
local is_swinging = false

-- Configuration variables
local earthquake_duration = 1500
local shaking_duration = 1000
local awake_duration = 500
local after_positioned_duration = 600
local after_swing_duration = 300
local after_attack_duration = 500
local hurt_duration = 600
local awakening_speed = 22
local swinging_speed = 44
local attacking_speed = 160
local attacking_maximum_distance = 100
local attacking_probability = 0.6
local base_offset = {x = 0, y = 25}
local swinging_offsets = {{x = 50, y = 25}, {x = -50, y = 25}}
local neck_entities_number = 7

-- Start the given animation or the last requested one if not given, taking care of the hurt animation which shouldn't be stopped while hurt.
local function start_head_sprite_animation(animation, on_finished_callback)

  if animation == "hurt" then
    sprite:set_animation(animation)
    return
  end

  if animation then
    head_routine_animation = animation
  end
  if not is_hurt then
    if on_finished_callback then
      sprite:set_animation(head_routine_animation, on_finished_callback)
    else
      sprite:set_animation(head_routine_animation)
    end
  end
end

-- Set the given animation to all neck sprites.
local function start_neck_sprites_animation(animation)
  
  for _, neck_entity in pairs(neck_entities) do
    neck_entity:get_sprite():set_animation(animation)
  end
end

-- Create a neck part
local function create_neck_sub_entity(sprite_suffix_name)

  local sub_enemy = map:create_enemy({
    name = (enemy:get_name() or enemy:get_breed()) .. "_" .. sprite_suffix_name,
    breed = "empty", -- Workaround: Breed is mandatory but a non-existing one seems to be ok to create an empty enemy though.
    x = initial_position.x,
    y = initial_position.y,
    layer = initial_position.layer,
    width = 16,
    height = 16,
    direction = sprite:get_direction()
  })
  sub_enemy:set_drawn_in_y_order(false) -- Display the sub enemy as a flat entity.
  sub_enemy:bring_to_back()
  sub_enemy:create_sprite("enemies/" .. enemy:get_breed() .. "/" .. sprite_suffix_name)

  sub_enemy:set_traversable(false) -- Make neck entity not traversable.
  sub_enemy:set_invincible()
  sub_enemy:set_attacking_collision_mode("touching")
  sub_enemy:set_damage(enemy:get_damage())

  -- Propagate some events and methods from the main enemy.
  enemy:register_event("on_removed", function(enemy)
    if sub_enemy:exists() then
      sub_enemy:remove()
    end
  end)
  enemy:register_event("on_enabled", function(enemy)
    sub_enemy:set_enabled()
  end)
  enemy:register_event("on_disabled", function(enemy)
    sub_enemy:set_enabled(false)
  end)
  enemy:register_event("on_dead", function(enemy)
    if sub_enemy:exists() then
      sub_enemy:remove()
    end
  end)
  enemy:register_event("set_visible", function(enemy, visible)
    sub_enemy:set_visible(visible)
  end)

  return sub_enemy
end

-- Make the enemy sleep and wait for the hero to wake up.
local function start_sleeping()

  enemy:set_invincible()
  enemy:set_can_attack(false)
  enemy:set_traversable(false)
  start_head_sprite_animation("sleeping")
end

-- Make the enemy go to the initial awakened position.
local function start_positioning(speed)

  local distance = enemy:get_distance(initial_position.x + base_offset.x, initial_position.y + base_offset.y)
  local angle = enemy:get_angle(initial_position.x + base_offset.x, initial_position.y + base_offset.y)
  local movement = enemy:start_straight_walking(angle, speed, distance, function()
    sol.timer.start(enemy, after_positioned_duration, function()
      enemy:start_swinging()
    end)
  end)
  movement:set_ignore_obstacles()
end

-- Make the enemy pounce on the hero.
local function start_attacking()

  -- Forbid angles between 0 and math.pi for the attack.
  local angle = enemy:get_angle(hero)
  if angle > 0.0 and angle <= quarter then
    angle = 0.0
  end
  if angle > quarter and angle < math.pi then
    angle = math.pi
  end

  start_head_sprite_animation("attacking")
  local distance = math.min(attacking_maximum_distance, enemy:get_distance(hero))
  local movement = enemy:start_straight_walking(angle, attacking_speed, distance, function()
    sol.timer.start(enemy, after_attack_duration, function()
      start_positioning(swinging_speed)
    end)
  end)
  movement:set_ignore_obstacles()
end

-- Manually hurt the enemy to not trigger to hurt or death built-in behavior.
local function hurt(damage)

  if is_hurt then
    return
  end
  is_hurt = true

  -- Custom die if no more life.
  if enemy:get_life() - damage < 1 then

    -- Wait a few time, start 2 sets of explosions close from the enemy, wait a few time again and finally make the final explosion and enemy die.
    enemy:start_death(function()
      for _, neck_entity in pairs(neck_entities) do
        neck_entity:stop_all()
      end
      start_neck_sprites_animation("hurt")
      start_head_sprite_animation("hurt")
      sol.timer.start(enemy, 3000, function()
        enemy:start_close_explosions(32, 2500, "entities/explosion_boss", 0, -13, "enemies/moldorm_segment_explode", function()
          sol.timer.start(enemy, 1000, function()
            enemy:start_brief_effect("entities/explosion_boss", nil, 0, -13)
            audio_manager:play_sound("enemies/boss_explode")
            finish_death()
          end)
        end)
        sol.timer.start(enemy, 200, function()
          enemy:start_close_explosions(32, 2300, "entities/explosion_boss", 0, -13, "enemies/moldorm_segment_explode")
        end)
      end)
      audio_manager:play_sound("enemies/boss_die")
    end)
    return
  end

  -- Manually hurt the enemy to only stop a running attack but not the swinging movement, so we can't restart the enemy.
  enemy:set_life(enemy:get_life() - damage)
  if not is_swinging then
    local movement = enemy:get_movement()
    if movement then
      movement:stop()
    end
    sol.timer.stop_all(enemy)
    start_positioning(swinging_speed)
  end
  start_neck_sprites_animation("hurt")
  start_head_sprite_animation("hurt")
  if enemy.on_hurt then
    enemy:on_hurt()
  end

  sol.timer.start(enemy, hurt_duration, function()
    is_hurt = false
    start_neck_sprites_animation("default")
    start_head_sprite_animation()
  end)
end

-- Make the enemy move to the step offset and randomly attack at the end of the movement.
function enemy:start_swinging()

  is_swinging = true

  -- Go to the next step position.
  local x, y = enemy:get_position()
  local target_x, target_y = initial_position.x + swinging_offsets[step].x, initial_position.y + swinging_offsets[step].y
  local angle = enemy:get_angle(target_x, target_y)
  local distance = enemy:get_distance(target_x, target_y)

  start_head_sprite_animation("awake")
  local movement = enemy:start_straight_walking(angle, swinging_speed, distance, function()
    step = (step % #swinging_offsets) + 1
    sol.timer.start(enemy, after_swing_duration, function()
      if math.random() < attacking_probability then
        is_swinging = false
        start_attacking()
      else
        enemy:start_swinging()
      end
    end)
  end)
  movement:set_ignore_obstacles()
end

-- Make the enemy wake up and move a bit forward.
function enemy:start_awakening(on_finished_callback)

  is_awake = true

  -- Start earthquake then awake animation.
  hero:freeze()
  map_tools.start_earthquake({duration = earthquake_duration, amplitude = 4, speed = 90})
  sol.timer.start(map, earthquake_duration, function()
    hero:unfreeze()
    start_head_sprite_animation("resuscitating", function()
      start_head_sprite_animation("shaking")
      sol.timer.start(enemy, shaking_duration, function()
        start_head_sprite_animation("awakening", function()
          start_head_sprite_animation("awake")
          sol.timer.start(enemy, awake_duration, function()
          start_head_sprite_animation("back_to_sleep", function()
            start_head_sprite_animation("awakening")
              start_head_sprite_animation("awake")
              sol.timer.start(enemy, awake_duration, function()
                enemy:restart()
                start_head_sprite_animation("awake") -- Workaround: The restart automatically set back the animation to "walking"
                if on_finished_callback then
                  on_finished_callback()
                end

                start_positioning(awakening_speed)
              end)
            end)
          end)
        end)
      end)
    end)
  end)
end

-- Update neck entities position on position changed.
enemy:register_event("on_position_changed", function(enemy, x , y, layer)

  -- Workaround: This event seems to be called before on_created(), ensure it doesn't.
  if not initial_position.x then
    return
  end

  -- Place neck entities.
  -- The Y position is placed every 20px of the base-to-head line, and the X one somewhere on the power 2 curve depending on the y position.
  local distance_x = x - initial_position.x
  local distance_y = y - initial_position.y
  if distance_y ~= 0 then
    local step_y = math.sin(enemy:get_angle(initial_position.x, initial_position.y)) * 20
    for index, neck in ipairs(neck_entities) do
      local neck_y = math.max(initial_position.y, y - index * step_y)
      local neck_x = initial_position.x + math.pow(1.0 / distance_y * (neck_y - initial_position.y), 2) * distance_x
      neck:set_position(neck_x , neck_y)
    end
  end
end)

-- Remove neck entities on dead.
enemy:register_event("on_dead", function(enemy)

  for _, neck in ipairs(neck_entities) do
    neck:remove()
  end
end)

-- Initialization.
enemy:register_event("on_created", function(enemy)

  enemy:set_life(16)
  enemy:set_damage(2)
  enemy:set_size(32, 64)
  enemy:set_origin(16, 30)
  enemy:set_hurt_style("boss")
  initial_position.x, initial_position.y, initial_position.layer = enemy:get_position()

  -- Create neck parts
  for i = 1, neck_entities_number, 1 do
    neck_entities[i] = create_neck_sub_entity("neck")
    common_actions.learn(neck_entities[i])
  end
end)

-- Restart settings.
enemy:register_event("on_restarted", function(enemy)

  enemy:set_hero_weapons_reactions({
  	arrow = "protected",
  	boomerang = "protected",
  	explosion = "ignored",
  	sword = function() hurt(1) end,
  	thrown_item = "protected",
  	fire = "protected",
  	jump = "ignored",
  	hammer = "protected",
  	hookshot = "protected",
  	magic_powder = "ignored",
  	shield = "protected",
  	thrust = "protected"
  })

  -- States.
  enemy:set_can_attack(true)
  enemy:set_traversable(true)
  if not is_awake then
    start_sleeping()
  end
end)
