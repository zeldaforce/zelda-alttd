-- Checking that grabbing a pickable with the hookshot works as expected.
require("scripts/tools/test_utils")
local map = ...
local game = map:get_game()

function map:on_started()

  local hookshot = game:get_item("hookshot")
  hookshot:set_variant(1)
  game:set_item_assigned(1, hookshot)
  game:set_max_money(99)
end

function map:do_after_transition()

  local hookshot = game:get_item("hookshot")
  sol.timer.start(map, 100, function()
    assert_equal(game:get_money(), 0)
    game:simulate_command_pressed("item_1")
    sol.timer.start(map, 1000, function()
      assert_equal(game:get_money(), 5)
      sol.main.exit()
    end)
  end)
end
      