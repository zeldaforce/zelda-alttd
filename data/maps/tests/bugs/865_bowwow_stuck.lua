require("scripts/tools/test_utils")

local map = ...
local game = map:get_game()

function map:on_started()

  game:set_step_done("bowwow_joined")
end

function map:on_opening_transition_finished()

  assert_equal(enemy:exists(), true)
end

enemy:register_event("on_removed", function(enemy)

  game:simulate_command_pressed("right")
  sol.timer.start(map, 2000, function()
    -- Make sure Bowwow is following the hero again.
    assert(companion_bowwow:get_distance(hero) < 100)
    sol.main.exit()
  end)
end)
