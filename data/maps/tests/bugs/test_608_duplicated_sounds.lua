require("scripts/tools/test_utils")

local map = ...
local game = map:get_game()

local sound_count = 0
local builtin_play_sound = sol.audio.play_sound

function sol.audio.play_sound(sound_id)
  builtin_play_sound(sound_id)

  if sound_id == "snes/enemies/enemy_hit" then
    sound_count = sound_count + 1
  end
end

function map:on_opening_transition_finished()
  game:get_item("sword"):set_variant(1)

  sol.timer.start(map, 10, function()
    game:simulate_command_pressed("attack")
    sol.timer.start(map, 1000, function()
      assert_equal(sound_count, 1)
      sol.main.exit()
    end)
  end)
end
