local map = ...
local game = map:get_game()

function map:on_opening_transition_finished()
  game:simulate_command_pressed("action")
  sol.timer.start(map, 2000, function()
    assert(hero:get_state() == "custom")
    game:simulate_command_released("action")
    game:simulate_command_pressed("left")
  end)
end
