-- Checking that bombs cannot be used while carrying something.
require("scripts/tools/test_utils")
local map = ...
local game = map:get_game()

function map:on_started()

  game:get_item("bombs_bag"):set_variant(1)
  local bombs_counter = game:get_item("bombs_counter")
  bombs_counter:set_amount(10)
  game:set_item_assigned(1, bombs_counter)
end

function map:do_after_transition()

  local bombs_counter = game:get_item("bombs_counter")
  sol.timer.start(map, 100, function()
    assert_equal(bombs_counter:get_amount(), 10)
    game:simulate_command_pressed("item_1")
    sol.timer.start(map, 100, function()
      assert_equal(bombs_counter:get_amount(), 9)
      sol.timer.start(map, 100, function()
        local carriable = hero:get_facing_entity()
        assert(carriable ~= nil)
        assert_equal(carriable:get_model(), "bomb")
        game:simulate_command_pressed("action")
        assert_equal(hero:get_state(), "frozen")
        sol.timer.start(map, 1000, function()
          assert_equal(hero:get_state(), "custom")  -- Custom carrying state.
          game:simulate_command_pressed("item_1")
          sol.timer.start(map, 100, function()
            assert_equal(bombs_counter:get_amount(), 9)
            sol.main.exit()
          end)
        end)
      end)
    end)
  end)
end
      