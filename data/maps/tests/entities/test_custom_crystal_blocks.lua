local map = ...
local game = map:get_game()

function map:on_opening_transition_finished()

  game:set_ability("sword", 1)
  local feather = game:get_item("feather")
  feather:set_variant(1)
  game:set_item_assigned(1, feather)
  game:simulate_command_pressed("down")
end

function sword_sensor:on_activated()

  game:simulate_command_pressed("attack")
  game:simulate_command_released("attack")
end

function sword_sensor_2:on_activated()

  game:simulate_command_pressed("attack")
  game:simulate_command_released("attack")
  game:simulate_command_released("down")
  game:simulate_command_pressed("right")
  sol.timer.start(map, 3000, function()
    game:simulate_command_released("right")
    game:simulate_command_pressed("up")
  end)
end

function feather_sensor:on_activated()

  game:simulate_command_pressed("item_1")
end

function the_end_sensor:on_activated()
  sol.main.exit()
end
