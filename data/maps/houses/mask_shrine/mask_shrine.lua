local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local light_manager = require("scripts/maps/light_manager")
local door_manager = require("scripts/maps/door_manager")
local audio_manager = require("scripts/audio_manager")
local separator_manager = require("scripts/maps/separator_manager")
local parchment = require("scripts/menus/parchment")
local slab_sprite = sol.sprite.create("pictures/south_shrine_slab")
local draw_slab_sprite = false
local slab_sprite_appear = true
local slab_sprite_opacity = 0

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(map, destination)

  light_manager:init(map)
  audio_manager:play_music("58_southern_shrine")

  -- Doors
  map:set_doors_open("door_boss_")

  -- Boss
  if game:get_value("armos_knight_killed") then 
    armos_knight:remove()
  else
    door_manager:open_when_enemies_dead(map, "armos_knight", "door_boss_")
    armos_knight:register_event("on_dead", function(armos_knight)
      sol.audio.stop_music()
      audio_manager:play_music("58_southern_shrine")
    end)
  end

  -- Separators
  separator_manager:init(map)

end)

map:register_event("on_draw", function(map, dst_surface)
  if draw_slab_sprite then
    slab_sprite:set_opacity(slab_sprite_opacity)
    slab_sprite:draw(dst_surface)
    if slab_sprite_appear then
      slab_sprite_opacity = slab_sprite_opacity + 20
      if slab_sprite_opacity > 255 then
        slab_sprite_opacity = 255
      end
    else
      slab_sprite_opacity = slab_sprite_opacity - 20
      if slab_sprite_opacity <= 0 then
        draw_slab_sprite = false
      end
    end
  end
end)

-- Discussion with Slab
function map:talk_to_slab() 

  if light_manager:check_is_light_active(map, "torch_group_") then
    map:launch_cinematic_1()
  else
    game:start_dialog("maps.houses.mask_shrine.mask_shrine.slab_1")
  end

end


-- NPCS events
function slab_1:on_interaction()

  map:talk_to_slab()
end

function slab_2:on_interaction()

  map:talk_to_slab()

end

-- Sensors events
function boss_sensor:on_activated()

  if game:get_value("armos_knight_killed") then
    return
  end

  boss_sensor:set_enabled(false)
  map:close_doors("door_boss_")
  sol.audio.stop_music()
  audio_manager:play_music("21_mini_boss_battle")

  function armos_knight:on_woke_up()
    local line_1 = sol.language.get_dialog("maps.houses.mask_shrine.mask_shrine.boss_name").text
    local line_2 = sol.language.get_dialog("maps.houses.mask_shrine.mask_shrine.boss_description").text
    parchment:show(map, "boss", "top", 1500, line_1, line_2)
  end
end

-- Cinematics
-- This is the cinematic in which the hero watches the south shrine slab
function map:launch_cinematic_1()
  
  torch_group_1:set_property("timer", nil)
  torch_group_2:set_property("timer", nil)
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {}
    }
    map:set_cinematic_mode(true, options)
    draw_slab_sprite = true
    wait(3000)
    dialog("maps.houses.mask_shrine.mask_shrine.slab_2")
    wait(3000)
    slab_sprite_appear = false
    wait(2000)
    torch_group_1:set_property("timer", "true")
    torch_group_2:set_property("timer", "true")
    map:set_cinematic_mode(false, options)
    game:set_value("south_shrine_slab_showed", true)
  end)

end