-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local preview_picture_manager = require("scripts/maps/preview_picture_manager")

map:register_event("on_started", function(map, destination)

  -- Music
 map:init_music()
 -- Preview picture manager
 preview_picture_manager:init_map(map)

end)

-- Initialize the music of the map
function map:init_music()
  
  audio_manager:play_music("27_mr_write_house")

end

-- Discussion with mr Write
function map:talk_to_mr_write() 

  local direction4 = mr_write:get_direction4_to(hero)
  local mr_write_sprite = mr_write:get_sprite()
  mr_write_sprite:set_direction(direction4)
  mr_write_sprite:set_animation("stopped")
  local item = game:get_item("magnifying_lens")
  local variant = item:get_variant()
  if variant < 9 then
      game:start_dialog("maps.houses.west_mt_tamaranch.mr_write_house.mr_write_1", function()
        mr_write_sprite:set_direction(3)
        mr_write_sprite:set_animation("waiting")
      end)
  elseif variant >= 10 then
     game:start_dialog("maps.houses.west_mt_tamaranch.mr_write_house.mr_write_5", function()
      mr_write_sprite:set_direction(3)
      mr_write_sprite:set_animation("waiting")
     end)
  else
    game:start_dialog("maps.houses.west_mt_tamaranch.mr_write_house.mr_write_2", function()
      map:launch_cinematic_1()
    end)
  end

end

-- Discussion with mr Write 2
function map:talk_to_mr_write_2()
  
  game:start_dialog("maps.houses.west_mt_tamaranch.mr_write_house.mr_write_4", function(answer)
    if answer == 1 then
      hero:start_treasure("magnifying_lens", 10, nil,  function()
        game:set_hud_enabled(true)
        game:set_pause_allowed(true)
        hero:unfreeze()
      end)
    else
      map:talk_to_mr_write_2()
    end
  end)

end

-- NPCs events
function mr_write:on_collision_fire()

  return false

end

function mr_write:on_interaction()

  map:talk_to_mr_write()

end

function mr_write_invisible:on_interaction()

  map:talk_to_mr_write()

end



-- Cinematics
-- This is the cinematic in which the hero give the letter to Mr Write
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {}
    }
    map:set_cinematic_mode(true, options)
    wait(1000)
    preview_picture_manager:show("peach")
    wait(4000)
    preview_picture_manager:hide()
    wait(1000)
    game:start_dialog("maps.houses.west_mt_tamaranch.mr_write_house.mr_write_3", function(answer)
      if answer == 1 then
        hero:start_treasure("magnifying_lens", 10, nil, function()
          mr_write_sprite:set_direction(3)
          mr_write_sprite:set_animation("waiting")
        end)
      else
        map:talk_to_mr_write_2()
      end
      map:set_cinematic_mode(false, options)
    end)
  end)
end

for wardrobe in map:get_entities("wardrobe") do
  function wardrobe:on_interaction()
    game:start_dialog("maps.houses.wardrobe_1", game:get_player_name())
  end
end
