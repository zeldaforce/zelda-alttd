-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()

end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("12_house")
end

-- Discussion with Merchant
function merchant:on_interaction()

  if not game:get_value("is_raft_paid") then
    game:start_dialog("maps.houses.east_mt_tamaranch.rafting_house.merchant_1", function(answer)
      if answer == 1 then
        local money = game:get_money()
        if money >= 100 then
          game:start_dialog("maps.houses.east_mt_tamaranch.rafting_house.merchant_2", function()
            game:remove_money(100)
            game:set_value("is_raft_paid", true)
          end)
        else
          game:start_dialog("maps.houses.east_mt_tamaranch.rafting_house.merchant_3")
        end
      end
    end)
  else
    game:start_dialog("maps.houses.east_mt_tamaranch.rafting_house.merchant_2")
  end
end
