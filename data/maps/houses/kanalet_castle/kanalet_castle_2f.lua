-- Variables
local map = ...
local game = map:get_game()
local quarter = math.pi * 0.5

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local enemy_manager = require("scripts/maps/enemy_manager")
local separator_manager = require("scripts/maps/separator_manager")
local treasure_manager = require("scripts/maps/treasure_manager")
local door_manager = require("scripts/maps/door_manager")

-- Create a darknut as invincible, start a little walk to get away from overlapping obstacles then restart.
local function create_darknut_from_statue(statue)

  local statue_name = statue:get_name()
  local x, y, layer = statue:get_position()
  local darknut = map:create_enemy({
    name = "darknut_from_" .. statue_name,
    breed = "darknut",
    x = x,
    y = y,
    layer = layer,
    direction = 3,
    properties = {
      { key = "is_hunting", value = "false" }
    }
  })
  local sprite = darknut:get_sprite()

  -- Stop movement initialized by the starts of the enemy.
  sol.timer.stop_all(darknut)
  darknut:stop_movement()
  darknut:set_drawn_in_y_order(false) -- Draw the enemy below the explosion that woke him up.
  darknut:set_invincible() -- Set invincible again as the restart() of the enemy make it vulnerable to explosions.
  sprite:set_animation("shaking")
  sprite:set_direction(darknut:get_direction4_to(game:get_hero()))

  -- Make the darknut walks to the south for some time then restarts normally.
  sol.timer.start(darknut, 1000, function()
    local movement = darknut:start_straight_walking(3 * quarter, 56, 16, function()
      darknut:set_drawn_in_y_order()
      darknut:start_hunting()
      darknut:restart()
    end)
    movement:set_ignore_obstacles()
  end)

  -- Make a golden leaf appear when the right darknut is defeated.
  if statue_name == "darknut_statue_2" then
    treasure_manager:appear_pickable_when_enemies_dead(map, "darknut_from_darknut_statue_2", "pickable_golden_leaf_4")
  end
end

-- Create a darknut enemy when darknut statue is exploded.
local function initialize_darknut_statues()

  for statue in map:get_entities("darknut_statue_") do
    statue:add_collision_test("overlapping", function(statue, entity)
      if entity:get_type() == "explosion" or (entity:get_type() == "custom_entity" and entity:get_model() == "explosion") then
        create_darknut_from_statue(statue)
        statue:get_sprite():set_animation("opened")
        statue:clear_collision_tests()
      end
    end)
  end
end

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Doors
  door_manager:open_when_carried_object_break(map, "door_group_1_")
  door_manager:open_when_enemies_dead(map,  "enemy_group_3_",  "door_group_1_")
  initialize_darknut_statues()
  -- Enemies
  if game:get_value("golden_leaf_5") then
    enemy_group_3_1:remove()
    map:set_doors_open("door_group_1", true)
  end
  -- Pickables
  treasure_manager:disappear_pickable(map, "pickable_golden_leaf_4")
  -- Separators
  separator_manager:init(map)
  
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("32_kanalet_castle")

end

-- Sensors events
function close_door_sensor:on_activated()

  door_manager:close_if_enemies_not_dead(map, "enemy_group_3_", "door_group_1_")

end

