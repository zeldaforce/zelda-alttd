-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities(destination)

end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("12_house")

end

-- Initializes entities based on player's progress
function map:init_map_entities()
 
  if game:is_step_done("walrus_awakened") and not game:get_value("dungeon_6_instrument") then
    rabbit:set_enabled(false)
  end

end

-- Discussion with rabbit
function map:talk_to_rabbit()

  if game:get_value("dungeon_6_instrument") then
    game:start_dialog("maps.houses.yarna_desert.rabbits_house.rabbit_2")
  else
    game:start_dialog("maps.houses.yarna_desert.rabbits_house.rabbit_1")
  end

end

-- NPCs events
function rabbit:on_interaction()

  map:talk_to_rabbit()

end
