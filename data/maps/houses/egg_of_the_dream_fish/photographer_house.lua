-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local effect_manager = require('scripts/maps/effect_manager')
local preview_picture_manager = require("scripts/maps/preview_picture_manager")
local gb = require('scripts/maps/gb_effect')

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
 -- Preview picture manager
 preview_picture_manager:init_map(map)
  
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("15_trendy_game")

end

-- Discussion with Photographer
function map:talk_to_photographer() 

  if game:get_value("photographer_quest_is_started") then
    local amount = 12 - game:get_item("photos_counter"):get_amount()
    if amount == 0 then
      if game:get_value("mode") ~= "gb" then
        game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_7", function()
          game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_8", function(answer)
           if answer == 1 then
            effect_manager:set_effect(game, gb)
            game:set_value("mode", "gb")
            audio_manager:refresh_music()
           end
          end)
        end)
       else
        game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_9", function(answer)
         if answer == 1 then
          effect_manager:set_effect(game, nil)
          game:set_value("mode", "snes")
          audio_manager:refresh_music()
         end
        end)
       end
    else
      game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_6", amount)
    end
  else
    if game:get_value("photographer_wait_link") then
      game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_2")
    else
      if map:companion_exist() then
        game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_10")
      else
        game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_1", function(answer)
          if answer == 1 then
            game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_2")
            game:set_value("photographer_wait_link", true)
          else
            -- Todo Make cinematic when photographer want to take a picture absolutly
          end
        end)
      end
    end
  end

end


-- NPCs events
function photographer:on_interaction()

  map:talk_to_photographer()

end


-- Sensor events
sensor_1:register_event("on_activated", function()

  if game:get_value("photographer_wait_link") then
    map:launch_cinematic_1()
  end

end)

function sensor_2:on_activated()

  if game:get_value("photographer_wait_link") then
    game:start_dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_5", function()
      hero:set_direction(2)
      hero:walk("2222")
   end)
  end

end

-- Pictures
for picture in map:get_entities("picture") do
  local picture_number = tonumber(picture:get_property("picture"))
  if game:get_value("photographer_quest_picture_" .. picture_number) then
    picture:get_sprite():set_animation("full")
  end
  function picture:on_interaction()
    if game:get_value("photographer_quest_picture_" .. picture_number) then
      game:start_dialog("scripts.meta.map.picture_full", function(answer)
        if answer == 1 then
          local suffix = picture_number
          if picture_number == 1 then
            suffix = "1_a"
          end
          map:launch_cinematic_2("photo_" .. suffix)
        end
      end)
    else
      game:start_dialog("scripts.meta.map.picture_empty")
    end
   

  end
end

-- Cinematics
-- This is the cinematic in which the photographer take a good picture
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, photographer}
    }
    map:set_cinematic_mode(true, options)
    hero:set_animation("walking")
    local m = sol.movement.create("target")
    m:set_speed(40)
    m:set_target(hero_placeholder)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, hero)
    hero:set_animation("stopped")
    wait(1000)
    hero:set_direction(3)
    wait(1000)
    photographer:get_sprite():set_animation("walking")
    local m = sol.movement.create("straight")
    m:set_speed(40)
    m:set_angle(math.pi)
    m:set_max_distance(58)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    local m = sol.movement.create("target")
    m:set_speed(40)
    m:set_target(photographer_placeholder_1)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    photographer:get_sprite():set_animation("waiting")
    dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_3")
    -- Launch flash effect
    wait(1000)
    photographer:get_sprite():set_animation("walking")
    local m = sol.movement.create("target")
    m:set_speed(40)
    m:set_target(photographer_placeholder_2)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    photographer:get_sprite():set_animation("waiting")
    photographer:get_sprite():set_direction(3)
    wait(2000)
    picture_1:get_sprite():set_animation("full")
    photographer:get_sprite():set_animation("walking")
    local m = sol.movement.create("target")
    m:set_speed(40)
    m:set_target(photographer_placeholder_3)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    photographer:get_sprite():set_animation("waiting")
    photographer:get_sprite():set_direction(2)
    dialog("maps.houses.egg_of_the_dream_fish.photographer_house.photographer_4")
    game:set_value("photographer_wait_link", false)
    game:set_value("photographer_quest_is_started", true)
    game:set_value("photographer_quest_picture_1", true)
    game:get_item("photos_counter"):add_amount(1)
    map:set_cinematic_mode(false, options)

  end)

end

-- This is the cinematic in which the picture is shown
  function map:launch_cinematic_2(picture)
    
    map:start_coroutine(function()
      map:set_cinematic_mode(true)
      wait(1000)
      preview_picture_manager:show(picture)
      wait(4000)
      preview_picture_manager:hide()
      wait(1000)
      map:set_cinematic_mode(false, options)
    end)
  end
