-- Variables
local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local menu_map = require("scripts/menus/title_screen/title_background")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  
end)

-- Initialize the music of the map
function map:init_music()

  if game:is_step_last("shield_obtained") then
    audio_manager:play_music("07_koholint_island")
  else
    audio_manager:play_music("12_house")
  end

end

-- Initializes Entities based on player's progress
function map:init_map_entities()

    -- Secret book
  book_9:set_enabled(false)
  if game:get_value("get_secret_book") then
    book_9:set_enabled(true)
    book_secret:set_enabled(false)
  end
  collision_book:add_collision_test("facing", function(entity, other, entity_sprite, other_sprite)
    if other:get_type() == 'hero' and hero:get_state() == "custom" and hero:get_state_object():get_description()=="running"  and game:get_value("get_secret_book") == nil then
      sol.timer.start(map, 250, function()      
        map:launch_cinematic_1()
      end)
    end
  end)

end

-- Discussion with Book
function map:open_book(book)

  game:start_dialog("maps.houses.mabe_village.library.book_"..book..".question", function(answer)
    if answer == 1 then
      local entity = map:get_entity("book_"..book)
      local sprite = entity:get_sprite()
      sprite:set_animation("reading")
      if book == 8 then
        local item = game:get_item("magnifying_lens")
        local variant_lens = item:get_variant()
        if variant_lens == 14 then
          local index_path = game:get_value("windfish_maze_boss_path_index") or 1
          game:start_dialog("maps.houses.mabe_village.library.book_"..book..".true_content_" .. index_path, function()
            sprite:set_animation("normal")
            game:set_value("windfish_maze_boss_path_read", true)
          end)
        else
          game:start_dialog("maps.houses.mabe_village.library.book_"..book..".content", function()
            sprite:set_animation("normal")
          end)
        end
      elseif book == 7 then
        game:set_value("menu_pause_screen", 2)
        sprite:set_animation("normal")
        game:set_paused(true)
      else
        game:start_dialog("maps.houses.mabe_village.library.book_"..book..".content", function()
          sprite:set_animation("normal")
        end)
      end
    end
  end)

end

-- NPCs events
function book_1_interaction:on_interaction()

  map:open_book(1)

end

function book_2_interaction:on_interaction()

  map:open_book(2)

end

function book_3_interaction:on_interaction()

  map:open_book(3)

end

function book_4_interaction:on_interaction()

  map:open_book(4)

end

function book_5_interaction:on_interaction()

  map:open_book(5)

end

function book_6_interaction:on_interaction()

  map:open_book(6)

end

function book_7_interaction:on_interaction()

  map:open_book(7)

end

function book_8_interaction:on_interaction()

  map:open_book(8)

end

function book_9:on_interaction()

  map:open_book(9)

end

-- Cinematics
-- This is the cinematic in which the hero retrieves the secret book
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, book_secret}
    }
    map:set_cinematic_mode(true, options)
    wait(250)
    local x, y = hero:get_position()
    local x_book_secret, y_book_secret = book_secret:get_position()
    local movement1 = sol.movement.create("target")
    movement1:set_speed(100)
    movement1:set_target(x, y_book_secret + 48)
    movement1:set_ignore_suspend(true)
    movement(movement1, hero)
    local movement2 = sol.movement.create("jump")
    movement2:set_speed(100)
    movement2:set_distance(32)
    movement2:set_direction8(6)
    movement2:set_ignore_suspend(true)
    movement2:set_ignore_obstacles(true)
    movement(movement2, book_secret)
    audio_manager:play_sound("jump")
    game:set_value("get_secret_book", true)
    book_9:set_enabled(true)
    book_secret:set_enabled(false)
    map:set_cinematic_mode(false, options)
  end)

end

-- Wardrobes
for wardrobe in map:get_entities("wardrobe") do
  function wardrobe:on_interaction()
    game:start_dialog("maps.houses.wardrobe_2", game:get_player_name())
  end
end

