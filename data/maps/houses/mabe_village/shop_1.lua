-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local claw_manager = require("scripts/maps/claw_manager")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()

end)


-- Initialize the music of the map
function map:init_music()

  if game:is_step_last("shield_obtained") then
    audio_manager:play_music("07_koholint_island")
  else
    audio_manager:play_music("15_trendy_game")
  end

end

-- NPCs events
function merchant:on_interaction()

  -- Don't make the hero pay again if the mini-game is already started.
  if merchant.playing then
    return
  end
 
  if map:get_entities_count("game_item") == 0 then
      game:start_dialog("maps.houses.mabe_village.shop_1.merchant_4")
  else 
    game:start_dialog("maps.houses.mabe_village.shop_1.merchant_1", function(answer)
      if answer == 1 then
        local money = game:get_money()
        if money >= 10 then
          game:start_dialog("maps.houses.mabe_village.shop_1.merchant_3", function()
            game:remove_money(10)
            merchant.playing = true
          end)
        else
          game:start_dialog("maps.houses.mabe_village.shop_1.merchant_2")
        end
      end
    end)
  end

end

-- Animates the merchant when he gets grabbed by Marin's claw.
merchant:register_event("on_position_changed", function(merchant, x, y, layer)

  local sprite = merchant:get_sprite()
  -- Detect that the merchant was just grabbed by checking the layer change.
  if layer == claw_crane:get_layer() and sprite:get_animation() ~= "crane" then
    sprite:set_animation("crane")
  end

end)

function console:on_interaction()

  if not merchant.playing then
    return
  end

  hero:freeze()
  local claw_menu = claw_manager:create_minigame(map)
  sol.menu.start(map, claw_menu)
  function claw_menu:on_finished()
    if merchant ~= nil then
      merchant.playing = false
      hero:unfreeze()
    end
  end

end

-- Special cutscene where Marin plays instead of Link and grabs the merchant.
local function start_marin_cutscene()

  map:start_coroutine(function()
    hero:freeze()
    hero:set_direction(2)
    dialog("maps.houses.mabe_village.shop_1.merchant_3")
    local m = sol.movement.create("target")
    m:set_speed(hero:get_walking_speed())
    m:set_ignore_obstacles(true)
    m:set_target(marin_target)
    movement(m, companion_marin)
    companion_marin:get_sprite():set_direction(1)

    hero:set_direction(1)
    local claw_menu = claw_manager:create_minigame(map)
    claw_menu:set_target(merchant)
    menu(map, claw_menu)
    local merchant_sprite = merchant:get_sprite()
    merchant_sprite:set_animation("sitting")
    m = sol.movement.create("straight")
    m:set_speed(24)
    m:set_angle(3 * math.pi / 2)
    m:set_max_distance(96)
    movement(m, merchant)
    merchant_sprite:set_animation("walking")
    merchant_sprite:set_direction(0)
    dialog("maps.houses.mabe_village.shop_1.marin_3")
    shop_1_2_B:on_activated()
  end)
end

function marin_sensor:on_activated()

  if companion_marin ~= nil then
    game:start_dialog("maps.houses.mabe_village.shop_1.marin_1", function(answer)
      if answer == 1 then
        start_marin_cutscene()
      else
        game:start_dialog("maps.houses.mabe_village.shop_1.marin_2", start_marin_cutscene)
      end
    end)
  end
end
