-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  -- Flying rooster awakening
  local item = game:get_item("melody_3")
  function item:on_played()
    if not game:get_value("flying_rooster_awakened") and hero:get_distance(flying_rooster) < 32 then
      map:launch_cinematic_1()
      game:set_value("flying_rooster_awakened", true)
    end
  end
  
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("18_cave")

end

-- Initializes Entities based on player's progress
function map:init_map_entities()
  
  -- Flying rooster
  if game:get_value("flying_rooster_awakened") then
    flying_rooster:set_enabled(false)
  end
  
end

-- This is the cinematic in which the flying rooster wakes up
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {}
    }
    map:set_cinematic_mode(true, options)
    wait(4000)
    -- Todo cinematic
    flying_rooster:set_enabled(false)
    game:init_companions(map)
    map:set_cinematic_mode(false, options)

  end)

end
