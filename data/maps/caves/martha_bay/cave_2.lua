-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local separator_manager = require("scripts/maps/separator_manager")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("18_cave")

end

-- Initializes entities based on player's progress
function map:init_map_entities()
 
  map:reset_enemies()

end

function map:reset_enemies()

    --Invisible things: only visible with the Lens
  local item = game:get_item("magnifying_lens")
  local variant_lens = item:get_variant()
  for entity in map:get_entities("invisible_entity") do
    if variant_lens == 14 then
      entity:get_sprite():set_opacity(255)
    else
      entity:get_sprite():set_opacity(0)
    end
  end
  for entity in map:get_entities("enemy_shadow") do
    if variant_lens == 14 then
      entity:get_sprite():set_opacity(255)
    else
      entity:get_sprite():set_opacity(0)
    end
  end

end

--Invisible things: only visible with the Lens
function map:on_obtained_treasure(item, variant, treasure_savegame_variable)
  
  if item:get_name() == "magnifying_lens" and item:get_variant() == 14 then
    map:reset_enemies()
  end
  
end

-- Separators
separator_manager:init(map)