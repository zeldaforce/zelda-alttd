-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Variables
local map = ...
local game = map:get_game()
local note_offset = {
  [wart] = {
    [0] = {x = 40, y = -32},
    [2] = {x = -40, y = -32}
  },
  [wart_brother_1] = {
    [0] = {x = 24, y = -8},
    [2] = {x = -24, y = -8}
  },
  [wart_brother_2] = {
    [0] = {x = 24, y = -8},
    [2] = {x = -24, y = -8}
  }
}

-- Start a standalone note effect on the given position, that will be removed once animation finished.
local function start_note_effect(entity, direction)

  local x, y, layer = entity:get_position()
  local entity = map:create_custom_entity({
      sprite = "entities/symbols/notes",
      x = x + note_offset[entity][direction].x,
      y = y + note_offset[entity][direction].y,
      layer = layer + 1,
      width = 24,
      height = 32,
      direction = direction
  })
  entity:set_drawn_in_y_order()

  entity:get_sprite():set_animation("normal", function()
    if entity:exists() then
      entity:remove()
    end
  end)

  return entity
end

-- Start a sprite animation and direction.
local function start_song_animation(entity, animation, direction, note_effect)

  local sprite = entity:get_sprite()
  sprite:set_animation(animation)
  sprite:set_direction(direction)

  if note_effect then
    start_note_effect(entity, direction)
  end
end

-- Start the Wart song cinematic.
local function start_wart_song(on_finished_callback)

  hero:freeze()
  hero:set_direction(hero:get_direction4_to(wart))
  audio_manager:stop_music()

  map:start_coroutine(function()
    map:set_cinematic_mode(true)
    game:set_suspended(false) -- Workaround: Don't use the game suspension of the cinematic mode.

    -- Introduction.
    wait(1500)
    audio_manager:play_music("56_frogs_song_of_soul")
    start_song_animation(wart, "singing_stopped", 0, true)
    wait(1000)
    start_song_animation(wart_brother_1, "singing", 0, true)
    wait(1000)
    start_song_animation(wart_brother_2, "singing", 0, true)
    wait(1750)

    for i = 0, 1, 1 do
      -- Verse.
      start_song_animation(wart, "singing", 0, true)
      start_song_animation(wart_brother_1, "waiting", 0)
      start_song_animation(wart_brother_2, "waiting", 0)
      wait(900)
      start_song_animation(wart_brother_1, "singing", 0, true)
      start_note_effect(wart, 2)
      wait(500)
      start_song_animation(wart_brother_1, "waiting", 0)
      wait(430)
      for i = 0, 6, 1 do
        start_song_animation(wart_brother_1, "singing", (i + 1) % 2 * 2, true)
        start_song_animation(wart_brother_2, "singing", i % 2 * 2, true)
        start_note_effect(wart, i % 2 * 2)
        wait(500)
        if i == 6 then
          sol.timer.start(wart, 200, function()
            wart:get_sprite():set_paused() -- Pause the Wart sprite before the Chorus.
          end)
        end
        start_song_animation(wart_brother_1, "waiting", 0)
        start_song_animation(wart_brother_2, "waiting", 0)
        wait(430)
      end

      -- Chorus.
      start_song_animation(wart, "singing_final", 0, true)
      start_song_animation(wart_brother_1, "singing", 0, true)
      start_song_animation(wart_brother_2, "singing", 2, true)
      start_note_effect(wart, 2)
      wait(500)
      start_song_animation(wart_brother_2, "waiting", 0)
      wait(430)
      start_song_animation(wart_brother_2, "singing", 0, true)
      wait(1970)
    end

    -- Song finished.
    audio_manager:play_music("18_cave")
    wait(500)
    start_song_animation(wart, "waiting", 0)
    start_song_animation(wart_brother_1, "waiting", 0)
    start_song_animation(wart_brother_2, "waiting", 0)
    map:set_cinematic_mode(false)
    on_finished_callback()
  end)
end

-- Start Wart dialog.
local function start_wart_dialog()

  if game:has_item("ocarina") then
    game:start_dialog("maps.caves.south_prairie.caves_5.wart_ocarina", function(answer)
      local money = game:get_money()
      if answer == 1 and money >= 300 then
        start_wart_song(function()
          game:start_dialog("maps.caves.south_prairie.caves_5.wart_ocarina_played", function()
            hero:start_treasure("melody_3", nil, nil, function()
              hero:set_direction(hero:get_direction4_to(wart))
              game:start_dialog("maps.caves.south_prairie.caves_5.wart_done", function()
                game:remove_money(300)
              end)
            end)
          end)
        end)
      else
        game:start_dialog("maps.caves.south_prairie.caves_5.wart_refused") -- Both no money or refuse the song.
      end
    end)
  else
    game:start_dialog("maps.caves.south_prairie.caves_5.wart_no_ocarina")
  end
end

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("18_cave")
end

-- Initializes Entities based on player's progress
function map:init_map_entities()

  if game:has_item("melody_3") then
    wart_brother_1:set_enabled(false)
    wart_brother_2:set_enabled(false)
    wart:set_enabled(false)
    wart_wall:set_enabled(false)
  end
end

-- Start Wart interaction on approaching.
function wart_sensor:on_activated()

  -- Don't interact if the song is already possessed.
  if game:has_item("melody_3") then
    return
  end

  start_wart_dialog()
end

-- Start Wart dialog on manual interaction.
function wart:on_interaction()

  if game:has_item("melody_3") then
    game:start_dialog("maps.caves.south_prairie.caves_5.wart_done")
    return
  end

  start_wart_dialog()
end