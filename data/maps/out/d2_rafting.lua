-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Set the force of the river flow.
local function set_river_flow(speed)

  for entity in map:get_entities() do
    if entity:get_type() == "custom_entity" and entity:get_model() == "stream" then
      entity:set_speed(speed)
    end
  end
end

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Digging
  map:set_digging_allowed(true)
  -- Stream speed
  set_river_flow(120)
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("10_overworld")
end