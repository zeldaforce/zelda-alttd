-- Variables
local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Include scripts
require("scripts/multi_events")
local travel_manager = require("scripts/maps/travel_manager")
local audio_manager = require("scripts/audio_manager")
local parchment = require("scripts/menus/parchment")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  -- Digging
  map:set_digging_allowed(true)
 
end)

-- Initialize the music of the map
function map:init_music()

  local x_hero, y_hero = hero:get_position()
  if y_hero < 384 then
    audio_manager:play_music("46_tal_tal_mountain_range")
  else
    audio_manager:play_music("10_overworld")
  end

end

-- Initializes Entities based on player's progress
function map:init_map_entities()
  
  -- Travel
  travel_transporter:set_enabled(false)

  -- Turtlerock
  if game:get_value("turtlerock_awakened") then
    turtlerock:set_enabled(false)
  else
    local item = game:get_item("melody_3")
    function item:on_played()
      if turtlerock and hero:get_distance(turtlerock) < 96 then
        turtlerock:start_awakening(function()
          local line_1 = sol.language.get_dialog("maps.out.west_mt_tamaranch.boss_name").text
          local line_2 = sol.language.get_dialog("maps.out.west_mt_tamaranch.boss_description").text
          parchment:show(map, "boss", "top", 1500, line_1, line_2)
        end)

        turtlerock:register_event("on_dead", function(turtlerock)
          map:init_music()
        end)

        sol.timer.start(map, 500, function()
          -- Wait a few time before starting the music to be synchronized with the animation.
          audio_manager:play_music("68_turtle_head")
        end)
        game:set_value("turtlerock_awakened", true)
      end
    end
  end

  -- Owl slab
  if game:get_value("travel_2") then
    owl_slab:get_sprite():set_animation("activated")
  end
  if game:get_value("travel_5") then
    owl_slab_2:get_sprite():set_animation("activated")
  end

end

--Doors events
function weak_door_1:on_opened()
  
  audio_manager:play_sound("misc/secret1")
  
end

-- Sensors events
function travel_sensor:on_activated()

  travel_manager:init(map, 2)

end

function travel_sensor_2:on_activated()

  travel_manager:init(map, 5)

end