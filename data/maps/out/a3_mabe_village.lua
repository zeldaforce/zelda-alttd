-- Variables
local map = ...
local game = map:get_game()
local ball
local ball_shadow
local hero_is_alerted = false
local marin_alert_link_chicken = 0

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local preview_picture_manager = require("scripts/maps/preview_picture_manager")

-- Map events
map:register_event("on_started", function(map, destination)

   -- Music
   map:init_music()
  -- Preview manager
  preview_picture_manager:init_map(map)
  -- Digging
  map:set_digging_allowed(true)
  -- Entities
  map:init_map_entities()
    
end)

function map:on_opening_transition_finished(destination)
  
  -- Kids scared
  local x_hero, y_hero = hero:get_position()
  if game:is_step_last("dungeon_1_completed") or game:is_step_last("bowwow_dognapped") then
    map:init_music()
    if destination == library_2_A then
      if not hero_is_alerted then
        hero_is_alerted = true
      end
    elseif destination == nil and hero:get_direction() == 1 and x_hero < 160 and y_hero > 760 then
      map:launch_cinematic_1(kids_alert_position_hero_2)
      hero_is_alerted = true
    else
      if hero_is_alerted then
        hero_is_alerted = false
      end
    end
  end
end

-- Initialize the music of the map
function map:init_music()
  
  if marin ~= nil and marin:is_sing() then
    return
  end
  if game:is_step_last("shield_obtained") then
    audio_manager:play_music("07_koholint_island")
  elseif game:is_step_last("dungeon_1_completed") and hero:get_distance(kids_alert_position_center) < 160 or game:is_step_last("bowwow_dognapped") and hero:get_distance(kids_alert_position_center) < 160 then
    audio_manager:play_music("26_bowwow_dognapped")
  else
    audio_manager:play_music("11_mabe_village")
  end

end

-- Initializes Entities based on player's progress
function map:init_map_entities()
 
  -- Bowwow
  if game:is_step_done("dungeon_1_completed") and not game:is_step_done("bowwow_returned")  then
    bowwow:set_enabled(false)
  end

local item = game:get_item("magnifying_lens")
  local variant_lens = item:get_variant()
  -- Tarin
  tarin:set_enabled(false)
  -- Marin
  if not game:get_value("marin_is_gone_mountain") and (not game:is_step_done("sword_obtained") or game:is_step_done("started_looking_for_marin")) then
    marin:set_enabled(false)
  else
    marin:get_sprite():set_animation("waiting")
  end
  -- Window
  window:set_enabled(false)
  if game:get_value("photographer_quest_is_started")
    and not game:get_value("photographer_quest_picture_5") then
    window:set_enabled(true)
  end
  -- Photographer
  photographer:set_enabled(false)
  -- Kid 5
  if not game:is_step_last("started_looking_for_marin")  then
    kid_5:set_enabled(false)
  end  
  -- Grand ma
  if game:get_value("dungeon_5_instrument") then
    grand_ma:set_enabled(false)
  else
    if variant_lens == 10 then
      grand_ma:get_sprite():set_animation("nobroom")
    else 
      grand_ma:get_sprite():set_animation("walking")
    end
  end
   -- Kids
  if not game:is_step_last("dungeon_1_completed") and not game:is_step_last("bowwow_dognapped") then
    map:create_ball(kid_1, kid_2)
    map:play_ball(kid_1, kid_2)
  else
    kid_1:get_sprite():set_animation("scared")
    kid_2:get_sprite():set_animation("scared")
    kid_1:get_sprite():set_ignore_suspend(true)
    kid_2:get_sprite():set_ignore_suspend(true)
    map:repeat_kids_scared_direction_check()
  end
  -- Thief detect
  local hero_is_thief_message = game:get_value("hero_is_thief_message")
  if hero_is_thief_message then
    game:start_dialog("maps.out.mabe_village.thief_message", function()
      game:set_value("hero_is_thief_message", false)
      if game:get_value("photographer_quest_is_started")
        and not game:get_value("photographer_quest_picture_7") then
          game:set_value("photographer_quest_picture_7", true)
          game:get_item("photos_counter"):add_amount(1)
      end
    end)
  end
  -- Chickens
  if game:is_step_last("marin_joined") then
    for enemy in map:get_entities_by_type("enemy") do
      if enemy:get_breed() == "chicken" then
        enemy:register_event("on_hurt", function(enemy, attack)
          if hero:get_state() == "sword swinging" then
            sol.timer.start(map, 500, function()
              if marin_alert_link_chicken < 5 then
                local symbol = companion_marin:create_symbol_exclamation(true)
                game:start_dialog("maps.out.mabe_village.marin_11", function()
                  symbol:remove()
                  marin_alert_link_chicken = marin_alert_link_chicken + 1
                end)
              else 
                game:start_dialog("maps.out.mabe_village.marin_12", function()
                  marin_alert_link_chicken = 0
                end)
              end
            end)
          end
        end)
      end
    end
  end
  -- Kids scared
  if game:is_step_last("dungeon_1_completed") or game:is_step_last("bowwow_dognapped") then
    sol.timer.start(map, 500, function()
      map:init_music()
      return true
    end)  
  end
  if game:get_value("mabe_village_weathercook_statue_pushed") then
      push_weathercook_sensor:set_enabled(false)
      weathercock:set_enabled(false)
      weathercook_statue_1:set_position(616,232)
      weathercook_statue_2:set_position(616,248)
  end

end

-- Kid's ball creation
function map:create_ball(player_1, player_2)

  local x_1,y_1, layer_1 = player_1:get_position()
  local x_2,y_2, layer_2 = player_2:get_position()
  local x_ball_shadow = x_1 
  local y_ball_shadow = y_1 + 8
  ball = map:create_custom_entity{
    name = "ball",
    x = x_1 + 8,
    y = y_1,
    width = 16,
    height = 24,
    direction = 0,
    layer = layer_1 + 1 ,
    sprite = "entities/misc/ball"
  }
  ball_shadow = map:create_custom_entity{
    name = "ball_shadow",
    x = x_ball_shadow,
    y = y_ball_shadow,
    width = 16,
    height = 24,
    direction = 0,
    layer = layer_1,
    sprite = "entities/shadows/ball"
  }
  
end

-- Kid's ball playing
function map:play_ball(player_1, player_2)
  
  ball:get_sprite():set_animation("thrown")
  player_1:get_sprite():set_animation("playing_1")
  player_2:get_sprite():set_animation("playing_2")
  local x_1,y_1, layer_1 = player_1:get_position()
  local x_2,y_2, layer_2 = player_2:get_position()
  local y_ball_shadow = y_1 + 8
  local distance = math.abs(x_2 - x_1) - 16
  local direction8 = 0
  if x_1 > x_2 then
    direction8 = 4
  end
  local movement = sol.movement.create("jump")
  movement:set_direction8(direction8)
  movement:set_distance(distance)
  movement:set_ignore_obstacles(true)
  movement:start(ball)
  function  movement:on_position_changed()
    local ball_x, ball_y, ball_layer = ball:get_position()
    ball_shadow:set_position(ball_x, y_ball_shadow)
  end
  function movement:on_finished()
    movement:stop()
    ball:get_sprite():set_animation("stopped")
    sol.timer.start(player_1, 500, function() 
      map:play_ball(player_2, player_1)
    end)
  end
  
end

-- Discussion with Fishman
function map:talk_to_fishman() 

  local fishman_sprite = fishman:get_sprite()
  local direction4 = fishman:get_direction4_to(hero)
  fishman_sprite:set_animation("stopped")
  fishman_sprite:set_direction(direction4)
  game:start_dialog("maps.out.mabe_village.fishman_1", function(answer)
    if answer == 1 then
      if game:get_money() >= 10 then
        game:remove_money(10)
        game:start_dialog("maps.out.mabe_village.fishman_2", function()
          fishman_sprite:set_animation("walking")
          fishman_sprite:set_direction(2)
          map:get_hero():teleport("sideviews/mabe_village/sideview_1", "pond_side")
        end)
      else
        game:start_dialog("maps.out.mabe_village.fishman_not_enough_money", function()
          fishman_sprite:set_animation("walking")
          fishman_sprite:set_direction(2)
        end)
      end
    else
      game:start_dialog("maps.out.mabe_village.fishman_3", function()
        fishman_sprite:set_animation("walking")
        fishman_sprite:set_direction(2)
      end)
    end
  end)

end

-- Discussion with Marin
function map:talk_to_marin() 

  local item_ocarina = game:get_item("ocarina")
  local item_melody_1 = game:get_item("melody_1")
  local variant_ocarina = item_ocarina:get_variant()
  local variant_melody_1 = item_melody_1:get_variant()
  if not game:is_step_done("tarin_saved") then
    game:start_dialog("maps.out.mabe_village.marin_1", game:get_player_name(), function()
      marin:sing_start()
    end)
  elseif not game:is_step_done("dungeon_2_completed") then
    game:start_dialog("maps.out.mabe_village.marin_2", game:get_player_name(), function()
      marin:sing_start()
    end)
  elseif variant_ocarina == 1 and variant_melody_1 == 0 then
    game:start_dialog("maps.out.mabe_village.marin_4", function()
      marin:launch_cinematic_marin_singing_with_hero(map)
    end)
  elseif game:get_value("marin_is_gone_mountain") and not game:get_value("possession_instrument_8") then
    game:start_dialog("maps.out.mabe_village.marin_9", game:get_player_name())
  elseif game:get_value("marin_is_gone_mountain") then
    game:start_dialog("maps.out.mabe_village.marin_10", game:get_player_name())
  elseif game:is_step_done("dungeon_3_completed") then
    game:start_dialog("maps.out.mabe_village.marin_8", function()
      marin:sing_start()
    end)
  else
    game:start_dialog("maps.out.mabe_village.marin_3", game:get_player_name(), function()
      marin:sing_start()
    end)
  end

end

-- Discussion with Grand ma
function  map:talk_to_grand_ma()

  local item = game:get_item("magnifying_lens")
  local variant_lens = item:get_variant()
  if variant_lens == 10 then
    game:start_dialog("maps.out.mabe_village.grand_ma_3", function(answer)
      if answer == 1 then
        map:launch_cinematic_2()
      else
        game:start_dialog("maps.out.mabe_village.grand_ma_4")
      end
    end)
  elseif variant_lens > 10 then
    game:start_dialog("maps.out.mabe_village.grand_ma_6")
  elseif not game:is_step_last("dungeon_1_completed") and not game:is_step_last("bowwow_dognapped") then  
    game:start_dialog("maps.out.mabe_village.grand_ma_1", function()
      grand_ma:get_sprite():set_direction(3)
    end)
  else
    game:start_dialog("maps.out.mabe_village.grand_ma_2", function()
      grand_ma:get_sprite():set_direction(3)
    end)
  end

end

-- Discussion with Kids
function map:talk_to_kids() 

  local rand = math.random(4)
  game:start_dialog("maps.out.mabe_village.kids_" .. rand)

end

-- Discussion with Kid 5
function map:talk_to_kid_5() 

  game:start_dialog("maps.out.mabe_village.kid_5_1")

end

function map:repeat_kids_scared_direction_check()

  local x_hero, y_hero = hero:get_position()
  local x_kid_1, y_kid_1 = kid_1:get_position()
  local x_kid_2, y_kid_2 = kid_2:get_position()
  local direction_kid_1 = 1
  local direction_kid_2 = 1
  if y_hero > y_kid_1 then
    direction_kid_1 = 3
  end
  if y_hero > y_kid_2 then
    direction_kid_2 = 3
  end
  kid_1:get_sprite():set_direction(direction_kid_1)
  kid_2:get_sprite():set_direction(direction_kid_2)
  sol.timer.start(map, 100, function() 
    map:repeat_kids_scared_direction_check()
  end)

end

-- Sensors events
function marin_sensor_1:on_activated()

  if marin:is_sing() then
    marin:sing_stop()
    map:init_music()    
  end

end

function marin_sensor_2:on_activated()

  if marin:is_sing() then
    marin:sing_stop()
    map:init_music()    
  end

end

function photographer_sensor_1:on_activated()

  if game:is_step_last("marin_joined") and game:get_value("photographer_quest_is_started")
    and not game:get_value("photographer_quest_picture_4") then
    map:launch_cinematic_4()
  end

end

function photographer_sensor_2:on_activated()

  if (game:is_step_last("bowwow_joined") or game:is_step_last("dungeon_2_completed") or game:is_step_last("bowwow_returned")) 
    and game:get_value("photographer_quest_is_started")
    and not game:get_value("photographer_quest_picture_6") then
    map:launch_cinematic_5()
  end

end

function push_weathercook_sensor:on_activated_repeat()
  
    if hero:get_animation() == "pushing" and hero:get_direction() == 1 and game:get_ability("lift") == 2 then
      hero:freeze()
      hero:get_sprite():set_animation("pushing")
      push_weathercook_sensor:set_enabled(false)
      weathercock:set_enabled(false)
      audio_manager:play_sound("hero_pushes")
        local weathercook_x,weathercook_y = map:get_entity("weathercook_statue_1"):get_position()
        local weathercook_x_2,weathercook_y_2 = map:get_entity("weathercook_statue_2"):get_position()
        local i = 0
        sol.timer.start(map,50,function()
          i = i + 1
          weathercook_y = weathercook_y - 1
          weathercook_statue_1:set_position(weathercook_x, weathercook_y)
          weathercook_y_2 = weathercook_y_2 - 1
          weathercook_statue_2:set_position(weathercook_x_2, weathercook_y_2)
          if i < 32 then return true end
          audio_manager:play_sound("misc/secret2")
          hero:unfreeze()
          game:set_value("mabe_village_weathercook_statue_pushed",true)
        end)
    end
    
end

-- NPCs events
function grand_ma:on_interaction()

  map:talk_to_grand_ma()

end

function kid_1:on_interaction()

  if game:is_step_last("bowwow_dognapped") then
    game:start_dialog("maps.out.mabe_village.kids_alert_moblins")
  else
    map:talk_to_kids()
  end

end

function kid_2:on_interaction()

  if game:is_step_last("bowwow_dognapped") then
    game:start_dialog("maps.out.mabe_village.kids_alert_moblins")
  else
    map:talk_to_kids()
  end
  
end

function kid_3:on_interaction()

  map:talk_to_kids()

end

function kid_4:on_interaction()

  map:talk_to_kids()
end

function kid_5:on_interaction()

  map:talk_to_kid_5()

end

function marin:on_interaction()

  if marin:is_sing() == false then
    map:talk_to_marin()
  end

end

function fishman:on_interaction()

  map:talk_to_fishman()

end

function window:on_interaction()

  if game:get_value("photographer_quest_is_started")
    and not game:get_value("photographer_quest_picture_5") then
    map:launch_cinematic_3()
  end

end

-- Cinematics
-- This is the cinematic that kids are scared
function map:launch_cinematic_1(destination)
  
  local hero = map:get_hero()
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, kid_1, kid_2}
    }
    map:set_cinematic_mode(true, options)
    hero:set_animation("walking")
    if destination ~= nil then
      local m = sol.movement.create("target")
      m:set_target(destination)
      m:set_speed(40)
      m:set_ignore_obstacles(true)
      m:set_ignore_suspend(true)
      movement(m, hero)
    end
    hero:set_animation("stopped")
    local symbol = hero:create_symbol_exclamation(true)
    wait(2000)
    symbol:remove()
    dialog("maps.out.mabe_village.kids_alert_moblins")
    hero:set_animation("scared")
    wait(1000)
    game:set_step_done("bowwow_dognapped")
    map:set_cinematic_mode(false)
  end)

end

-- This is the cinematic in which grand ma retrieve broom
function map:launch_cinematic_2()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {grand_ma}
    }
    map:set_cinematic_mode(true, options)
    grand_ma:get_sprite():set_animation("brandish")
    local x_grand_ma, y_grand_ma, layer_grand_ma = grand_ma:get_position()
    local broom_entity = map:create_custom_entity({
      name = "brandish_broom",
      sprite = "entities/items",
      x = x_grand_ma,
      y = y_grand_ma - 24,
      width = 16,
      height = 16,
      layer = layer_grand_ma + 1,
      direction = 0
    })
    broom_entity:get_sprite():set_animation("magnifying_lens")
    broom_entity:get_sprite():set_direction(9)
    audio_manager:play_sound("items/fanfare_item")
    wait(2000)
    broom_entity:remove()
    grand_ma:get_sprite():set_animation("walking")
    map:set_cinematic_mode(false, options)
    game:start_dialog("maps.out.mabe_village.grand_ma_5", function()
      hero:start_treasure("magnifying_lens", 11)
    end)
  end)

end

-- This is the cinematic in which link look in the grand pa's window
function map:launch_cinematic_3()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {photographer}
    }
    map:set_cinematic_mode(true, options)
    photographer:set_enabled(true)
    local x,y = hero:get_position()
    local x_photographer, y_photographer = photographer:get_position()
    local m = sol.movement.create("target")
    m:set_target(x + 16, y)
    m:set_speed(80)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    wait(1000)
    hero:set_direction(0)
    dialog("maps.out.mabe_village.photographer_1")
    hero:set_direction(1)
    photographer:get_sprite():set_direction(1)
    dialog("maps.out.mabe_village.photographer_2", game:get_player_name())
    wait(1000)
    preview_picture_manager:show("photo_5")
    wait(4000)
    preview_picture_manager:hide()
    local m = sol.movement.create("target")
    m:set_target(x_photographer, y_photographer)
    m:set_speed(80)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    movement(m, photographer)
    photographer:set_enabled(false)
    game:set_value("photographer_quest_picture_5", true)
    game:get_item("photos_counter"):add_amount(1)
    window:set_enabled(false)
    map:set_cinematic_mode(false, options)

  end)

end

-- This is the cinematic in which the photographer take a picture with, link, marin and tarin
function map:launch_cinematic_4()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, marin, photographer, tarin, tarin_invisible}
    }
    map:set_cinematic_mode(true, options)
    wait(1000)
    local x_companion, y_companion = companion_marin:get_position()
    companion_marin:set_enabled(false)
    -- Photographer movement
    photographer:set_enabled(true)
    local x_photographer, y_photographer = photographer:get_position()
    local m = sol.movement.create("target")
    m:set_target(photographer_4_photographer_position)
    m:set_speed(80)
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    m:start(photographer)
    -- Marin movement
    marin:set_position(x_companion, y_companion)
    marin:get_sprite():set_animation("walking")
    marin:set_enabled(true)
    local m1 = sol.movement.create("target")
    m1:set_target(photographer_4_marin_position)
    m1:set_speed(40)
    m1:set_ignore_obstacles(true)
    m1:set_ignore_suspend(true)
    movement(m1, marin)
    marin:get_sprite():set_animation("stopped")
    marin:get_sprite():set_direction(3)
    -- Hero movement
    hero:set_animation("walking")
    local m2 = sol.movement.create("target")
    m2:set_target(photographer_4_hero_position)
    m2:set_speed(40)
    m2:set_ignore_obstacles(true)
    m2:set_ignore_suspend(true)
    movement(m2, hero)
    hero:set_animation("stopped")
    hero:set_direction(3)
    -- Tarin movement
    local x_tarin, y_tarin = tarin:get_position()
    tarin:get_sprite():set_animation("run_bee")
    tarin:get_sprite():set_direction(2)
    tarin:set_enabled(true)
    local m3 = sol.movement.create("target")
    m3:set_target(photographer_4_tarin_position)
    m3:set_speed(100)
    m3:set_ignore_obstacles(true)
    m3:set_ignore_suspend(true)
    function m3:on_position_changed()
      local x_tarin,y_tarin = tarin_invisible:get_position()
      tarin:set_position(x_tarin, y_tarin)
    end
    movement(m3, tarin_invisible)
    tarin:get_sprite():set_animation("stopped")
    tarin:get_sprite():set_direction(1)
    -- Tarin talk
    dialog("maps.out.mabe_village.tarin_1", game:get_player_name())
    -- Tarin movement 2
    tarin:get_sprite():set_animation("walking")
    local m4 = sol.movement.create("straight")
    m4:set_angle(math.pi / 2)
    m4:set_speed(40)
    m4:set_ignore_obstacles(true)
    m4:set_ignore_suspend(true)
    m4:set_max_distance(40)
    movement(m4, tarin)
    tarin:get_sprite():set_animation("stopped")
    tarin:get_sprite():set_direction(3)
    -- Hero movement 2
    local m5 = sol.movement.create("straight")
    m5:set_angle(0)
    m5:set_speed(120)
    m5:set_ignore_obstacles(true)
    m5:set_ignore_suspend(true)
    m5:set_max_distance(8)
    movement(m5, hero)
    hero:set_direction(3)
    -- Marin movement 2
    local m6 = sol.movement.create("straight")
    m6:set_angle(math.pi)
    m6:set_speed(120)
    m6:set_ignore_obstacles(true)
    m6:set_ignore_suspend(true)
    m6:set_max_distance(8)
    movement(m6, marin)
    marin:get_sprite():set_direction(3)
    local symbol = marin:create_symbol_exclamation()
    dialog("maps.out.mabe_village.photographer_3")
    wait(2000)
    symbol:remove()
    -- Todo add flash effect
    wait(2000)
    dialog("maps.out.mabe_village.photographer_4")
    local m7 = sol.movement.create("target")
    m7:set_target(x_photographer, y_photographer)
    m7:set_speed(80)
    m7:set_ignore_obstacles(true)
    m7:set_ignore_suspend(true)
    m7:start(photographer)
    local m8 = sol.movement.create("target")
    m8:set_target(x_tarin, y_tarin)
    m8:set_speed(80)
    m8:set_ignore_obstacles(true)
    m8:set_ignore_suspend(true)
    movement(m8, tarin)
    local x_marin, y_marin = marin:get_position()
    companion_marin:set_position(x_marin, y_marin)
    marin:set_enabled(false)
    photographer:set_enabled(false)
    tarin:set_enabled(false)
    companion_marin:set_enabled(true)
    game:set_value("photographer_quest_picture_4", true)
    game:get_item("photos_counter"):add_amount(1)
    map:set_cinematic_mode(false, options)

  end)

end

-- This is the cinematic in which the photographer take a picture with, link and bowwow
function map:launch_cinematic_5()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {photographer}
    }
    map:set_cinematic_mode(true, options)
    wait(4000)
    game:set_value("photographer_quest_picture_6", true)
    game:get_item("photos_counter"):add_amount(1)
    map:set_cinematic_mode(false, options)

  end)

end