-- Variables
local map = ...
local game = map:get_game()
local no_cinematic_photographer = false

-- Include scripts
require("scripts/multi_events")
local owl_manager = require("scripts/maps/owl_manager")
local audio_manager = require("scripts/audio_manager")
local preview_picture_manager = require("scripts/maps/preview_picture_manager")

-- Disable a ghini or giant ghini positioned on a grave, then wake him up when the hero touches the grave or its stairs.
local function initialize_graves()

  for grave in map:get_entities("grave_") do
    grave:set_size(24, 24) -- Workaround : No way to set the correct size to the bloc directly on the editor, so do it here.
    grave:set_origin(12, 21)
    for enemy in map:get_entities_by_type("enemy") do
      if (enemy:get_breed() == "ghini" or enemy:get_breed() == "ghini_giant") and enemy:overlaps(grave) then

        -- Create a custom entity on the grave entity to add a collision test on it.
        local x, y, layer = grave:get_position()
        local width, height = grave:get_size()
        local trigger = map:create_custom_entity({
          x = x,
          y = y,
          layer = layer,
          width = width,
          height = height,
          direction = 0
        })
        trigger:set_origin(width / 2.0, height - 3)
        trigger:set_position(grave:get_position()) -- Set the position again that have changed with the set_origin()

        -- Disable the ghini and wake him up when the grave is faced.
        enemy:set_enabled(false)
        trigger:add_collision_test("facing", function(trigger, entity)
          if entity:get_type() == "hero" then
            trigger:remove()
            enemy:wake_up()
          end
        end)
      end
    end
  end
end

-- Create another enemy on dead, to make it infinitely respawn.
local function make_enemy_undead(enemy)

  enemy:register_event("on_dead", function(enemy)
    local area = enemy:get_property("area")
    local properties = {
      area and {key = "area", value = area} or nil
    }
    local x, y, layer = enemy:get_position()
    local new_enemy = map:create_enemy({
      name = enemy:get_name() or enemy:get_breed(),
      breed = enemy:get_breed(),
      x = x,
      y = y,
      layer = layer,
      direction = 0,
      properties = properties
    })
    make_enemy_undead(new_enemy)
  end)
end

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Preview manager
  preview_picture_manager:init_map(map)
  -- Entities
  map:init_map_entities()
  -- Digging
  map:set_digging_allowed(true)

  -- Initialize grave connected to ghinis.
  initialize_graves()

  -- Make all zombies of this map undead.
  for enemy in map:get_entities_by_type("enemy") do
    if enemy:get_breed() == "zombie" then
      make_enemy_undead(enemy)
    end
  end

  -- Make lower area invisible.
  graveyard_pit_1:set_visible(false)

  --Dungeon 9 opened
  if game:get_value("dungeon_9_opened") then
    grave_pushable_3:set_position(grave_place:get_position())
    sensor_check_dungeon_9_entrance:set_enabled(false)
    map:set_entities_enabled("dungeon_9_entrance_stairs",true)
  end
end)

-- Initializes Entities based on player's progress
function map:init_map_entities()
  
  -- Ghost
  ghost:set_enabled(false)
  -- Owl
  owl_9:set_enabled(false)
  -- Photographer
  photographer:set_enabled(false)

end

-- Initialize the music of the map
function map:init_music()
  
  audio_manager:play_music("10_overworld")

end

-- Sensors events

function sensor_1:on_activated()
  
  
  if game:is_step_last("ghost_house_visited") then
    map:launch_cinematic_1()
    no_cinematic_photographer = true
  end
  
end

function photographer_sensor_1:on_activated()
  
  
  if game:is_step_done("ghost_returned_to_tomb")
    and game:get_value("photographer_quest_is_started")
    and not game:get_value("photographer_quest_picture_11")
    and not no_cinematic_photographer then
    map:launch_cinematic_2()
  end
  
end


-- This is the cinematic in which the ghost comes home
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {ghost, companion_ghost}
    }
    map:set_cinematic_mode(true, options)
    local x, y, layer = companion_ghost:get_position()
    ghost:set_position(x,y)
    ghost:set_enabled(true)
    companion_ghost:set_enabled(false)
    ghost:get_sprite():set_direction(1)
    ghost:get_sprite():set_animation("walking")
    local movement1 = sol.movement.create("target")
    movement1:set_speed(32)
    movement1:set_target(position_ghost)
    movement1:set_ignore_suspend(true)
    movement1:set_ignore_obstacles(true)
    movement(movement1, ghost)
    ghost:get_sprite():set_direction(3)
    ghost:get_sprite():set_animation("goodbye")
    wait(2000)
    dialog("maps.out.graveyard.ghost_1")
    wait(2000)
    ghost:set_enabled(false)
    if not game:get_value("possession_intrument_5") then
      owl_manager:appear(map, 9, function()
        map:init_music()
      end)
    else
      map:set_cinematic_mode(false, options)
    end
    game:set_step_done("ghost_returned_to_tomb")
  end)

end

--DUNGEON 9 OPENING
local order = 0
function dungeon_9_entrance_switch_1:on_activated()
  if order == 0 then order = order + 1
  else order = 0 end
end
function dungeon_9_entrance_switch_2:on_activated()
  if order == 1 then order = order + 1
  else order = 0 end
end
function dungeon_9_entrance_switch_3:on_activated()
  if order == 2 then order = order + 1
  else order = 0 end
end
function dungeon_9_entrance_switch_4:on_activated()
  if order == 3 then order = order + 1
  else order = 0 end
end
function sensor_check_dungeon_9_entrance:on_activated_repeat()

  if order == 4 and hero:get_sprite():get_animation() == "pushing" then
    
    --You can't push the grave if Bowwow or Marin is with you
    if not game:is_step_done("bowwow_returned") or game:is_step_last("marin_joined") then
      game:start_dialog("maps.out.graveyard.grave")
      return
    end

    sensor_check_dungeon_9_entrance:set_enabled(false)
    hero:freeze()
    local movement = sol.movement.create("target")
    movement:set_speed(30)
    movement:set_ignore_obstacles(true)
    movement:set_target(grave_place)
    movement:start(grave_pushable_3)
    audio_manager:play_sound("misc/rock_push")
    function movement:on_finished()
      hero:unfreeze()
      audio_manager:play_sound("misc/secret2")
      game:set_value("dungeon_9_opened",true)
      map:set_entities_enabled("dungeon_9_entrance_stairs",true)
    end
  end
end

-- This is the cinematic in which the photographer take a picture with Link and ghost
function map:launch_cinematic_2()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {photographer}
    }
    map:set_cinematic_mode(true, options)
    wait(1000)
    -- Hero movement
    hero:set_animation("walking")
    local m1 = sol.movement.create("target")
    m1:set_target(photographer_11_hero_position_1)
    m1:set_speed(40)
    m1:set_ignore_obstacles(true)
    m1:set_ignore_suspend(true)
    movement(m1, hero)
    hero:set_animation("stopped")
    hero:set_direction(1)
    wait(2000)
    -- Photographer movement
    local x_photographer, y_photographer = photographer:get_position()
    photographer:set_enabled(true)
    local m2 = sol.movement.create("target")
    m2:set_target(photographer_11_photographer_position_1)
    m2:set_speed(40)
    m2:set_ignore_obstacles(true)
    m2:set_ignore_suspend(true)
    movement(m2, photographer)
    local m3 = sol.movement.create("target")
    m3:set_target(photographer_11_photographer_position_2)
    m3:set_speed(40)
    m3:set_ignore_obstacles(true)
    m3:set_ignore_suspend(true)
    movement(m3, photographer)
    photographer:get_sprite():set_direction(1)
    hero:set_direction(3)
    dialog("maps.out.graveyard.photographer_1", game:get_player_name())
    wait(1000)
    dialog("maps.out.graveyard.photographer_2")
    hero:set_animation("walking")
    local m4 = sol.movement.create("target")
    m4:set_target(photographer_11_hero_position_2)
    m4:set_speed(40)
    m4:set_ignore_obstacles(true)
    m4:set_ignore_suspend(true)
    movement(m4, hero)
    hero:set_animation("stopped")
    hero:set_direction(3)
    dialog("maps.out.graveyard.photographer_3")
    wait(1000)
    -- Todo add flash effect
    wait(1000)
    preview_picture_manager:show("photo_11")
    wait(4000)
    preview_picture_manager:hide()
    local m5 = sol.movement.create("target")
    m5:set_target(photographer_11_photographer_position_1)
    m5:set_speed(40)
    m5:set_ignore_obstacles(true)
    m5:set_ignore_suspend(true)
    movement(m5, photographer)
    local m6 = sol.movement.create("target")
    m6:set_target(x_photographer, y_photographer)
    m6:set_speed(40)
    m6:set_ignore_obstacles(true)
    m6:set_ignore_suspend(true)
    movement(m6, photographer)
    photographer:set_enabled(false)
    game:set_value("photographer_quest_picture_11", true)
    game:get_item("photos_counter"):add_amount(1)
    map:set_cinematic_mode(false, options)

  end)

end