-- Variables
local map = ...
local game = map:get_game()
local hero = map:get_hero()

-- Include scripts
require("scripts/multi_events")
local travel_manager = require("scripts/maps/travel_manager")
local owl_manager = require("scripts/maps/owl_manager")
local audio_manager = require("scripts/audio_manager")


-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  -- Digging
  map:set_digging_allowed(true)
  -- Instruments
  for instrument in map:get_entities("instrument_") do
    local num = instrument:get_property("num")
    instrument:get_sprite():set_animation("instrument_" .. num)
    instrument:set_enabled(false)
  end
   -- Wind fish egg
  local item = game:get_item("melody_1")
  function item:on_played()
    if not game:is_step_done("wind_fish_egg_opened") and hero:get_distance(egg_door_1) < 32 then
      map:launch_cinematic_1()
    end
  end
  -- Disable dungeon 2 teleporter when ghost is with the hero
  if game:is_step_last("ghost_joined") 
    or game:is_step_last("ghost_saw_his_house")
    or game:is_step_last("ghost_house_visited")
    or game:is_step_last("marin_joined") then
        dungeon_2_1_A:set_enabled(false)
  end

end)

map:register_event("on_opening_transition_finished", function(map, destination)

  if destination == dungeon_2_2_A and game:is_step_last("marin_joined") then
    game:start_dialog("scripts.meta.map.companion_marin_dungeon_out", game:get_player_name())
  end

end)

-- Initialize the music of the map
function map:init_music()
  
  local x_hero, y_hero = hero:get_center_position()
  if y_hero <= 384 then
    audio_manager:play_music("46_tal_tal_mountain_range")
  else
    audio_manager:play_music("10_overworld")
  end
end

-- Initializes Entities based on player's progress
function map:init_map_entities()
  
    -- Owl
  owl_6:set_enabled(false)
  owl_15:set_enabled(false)
  -- Remove the big stone if you come from the secret cave
  if destination == stair_arrows_upgrade then
    secret_stone:set_enabled(false)
  end
  -- Windfish Egg
  if game:get_value("dream_fish_egg_opened") then
    for egg_door in map:get_entities("egg_door_") do 
      egg_door:set_enabled(false)
    end
  end

end


-- Sensors events
function owl_6_sensor:on_activated()

  if game:get_value("owl_6") ~= true then
    owl_manager:appear(map, 6, function()
      map:init_music()
    end)
  end

end

function sensor_companion:on_activated()

  if map:get_game():is_step_last("ghost_joined") 
    or map:get_game():is_step_last("ghost_saw_his_house")
    or map:get_game():is_step_last("ghost_house_visited") then
        game:start_dialog("scripts.meta.map.companion_ghost_dungeon_in")
  elseif game:is_step_last("marin_joined") then
    game:start_dialog("scripts.meta.map.companion_marin_dungeon_in", game:get_player_name(), function()
      dungeon_2_1_A:set_enabled(true)
    end)
  end

end


-- Handle boulders spawning depending on activated sensor.
for sensor in map:get_entities("sensor_activate_boulder_") do
  sensor:register_event("on_activated", function(sensor)
    spawner_boulder_1:start()
    spawner_boulder_2:start()
  end)
end
for sensor in map:get_entities("sensor_deactivate_boulder_") do
  sensor:register_event("on_activated", function(sensor)
    spawner_boulder_1:stop()
    spawner_boulder_2:stop()
  end)
end


-- Remove spawned boulders when too far of the mountain.
for spawner in map:get_entities("spawner_boulder_") do
  spawner:register_event("on_enemy_spawned", function(spawner, enemy)
    enemy:register_event("on_position_changed", function(enemy)
      local _, y, _ = enemy:get_position()
      if y > 500 then
        enemy:remove()
      end
    end)
  end)
end

-- This is the cinematic in which the hero want to open wind fish egg
function map:launch_cinematic_1()
  
  audio_manager:stop_music()
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, instrument_1, instrument_2, instrument_4, instrument_5, instrument_6, instrument_7, instrument_8}
    }
    map:set_cinematic_mode(true, options)
    local error_instrument = false  
    local num_instruments = 0                                                                 
    for num = 1, 8 do
      if game:get_value("possession_instrument_" .. num) then
        audio_manager:play_sound("misc/ground_crumble")
        local instrument = map:get_entity("instrument_" .. num)
        instrument:set_enabled(true)
        if not error_instrument then
          num_instruments = num_instruments + 1
        end
        wait(1000)
      else
        error_instrument = true
      end
    end
    hero:sing_start_animation(map)
    local music = "73_the_ballad_of_the_wind_fish"
    if num_instruments <= 2 then
      music = "99_wind_fish_song_2"
    elseif num_instruments <= 7 then
      music = "99_wind_fish_song_" .. num_instruments
    end
    audio_manager:play_music(music)
    wait(37000)
    hero:sing_stop_animation(map)
    for instrument in map:get_entities("instrument_") do
      instrument:set_enabled(false)
    end
    if not error_instrument then
      game:set_step_done("wind_fish_egg_opened")
      local camera = map:get_camera()
      local shake_config = {
          duration = 700,
          amplitude = 2,
          speed = 90
      }
      wait_for(camera.shake,camera,shake_config)
      for egg_door in map:get_entities("egg_door_") do 
        egg_door:set_enabled(false)
      end
      wait_for(owl_manager.appear, owl_15, map, 15)
    end
    map:init_music()
    map:set_cinematic_mode(false, options)

  end)

end