-- Variables
local map = ...
local game = map:get_game()
local draw_gauge_sprite = false
local gauge_timer = false
local gauge_height = 0
local gauge_sprite = sol.sprite.create("entities/misc/shell_mansion_bar")
local num_max_seashells = 20
local num_seashells = game:get_item("seashells_counter"):get_amount()
if num_seashells > num_max_seashells then
  num_seashells = num_max_seashells
end
local treasures = {
  seashell_25 = {
    goal = 5,
    treasure = "seashell",
    variant = 1,
  },
  seashell_26 = {
    goal = 10,
    treasure = "seashell",
    variant = 1,
  },
  possession_sword = {
    goal = 20,
    treasure = "sword",
    variant = 2,
  }
}

gauge_sprite:set_animation("step")

-- Include scripts
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Sideview
  map:set_sideview(true)
  
end)

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("20_sidescrolling")

end

map:register_event("on_draw", function(map, dst_surface)
  if draw_gauge_sprite then
    local x,y = gauge:get_position()
    local offset = 0
    for i = 0, gauge_height do
      gauge_sprite:draw(dst_surface, x, y - i)
    end
  end  
end)

-- Sensors events
function sensor:on_activated()

  sensor:set_enabled(false)
  map:launch_cinematic_1()

end

-- This is the cinematic in which the hero's shell count is tested.
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {gauge, thunder, light_5_1, light_5_2, light_10_1, light_10_2, light_20_1, light_20_2, shadow_sword}
    }
    map:set_cinematic_mode(true, options)
    draw_gauge_sprite = true
    local max_height = num_seashells / num_max_seashells * 160 - 4
    for i = gauge_height, max_height do
      gauge_height = gauge_height + 1
      wait(5)
    end
    wait(1000)
    local lights = false
    for savegame, treasure in pairs(treasures) do
      if not lights and treasure.goal == num_seashells then
        lights = true
        for entity in map:get_entities("light_" .. num_seashells .. "_") do
          entity:set_enabled(true)
        end
      end
    end
    wait(1000)
    if lights and num_seashells == num_max_seashells then
      draw_gauge_sprite = false
      gauge:set_enabled(true)
      dialog("maps.sideviews.east_prairie.quest_finished")
      for entity in map:get_entities("light_") do
        entity:set_enabled(false)
      end
      thunder:set_enabled(true)
      wait(4000)
      thunder:get_sprite():set_animation("second_step")
      wait(3000)
      thunder:get_sprite():set_animation("third_step")
      wait(3000)
      thunder:set_enabled(false)
      shadow_sword:set_enabled(true)
      animation(shadow_sword:get_sprite(), "appearing")
      shadow_sword:set_enabled(false)
      local x,y,layer = shadow_sword:get_position()
      local pickable = map:create_pickable({
        x = x,
        y = y,
        layer = layer,
        treasure_name = "sword",
        treasure_variant = 2
      })
      pickable:get_sprite():set_direction(5)
      map:start_gravity(pickable)
      local item_sword = pickable:get_treasure()
      function item_sword:on_obtained()
        map:launch_cinematic_2()
      end

    end
    map:set_cinematic_mode(false, options)

  end)
end


function map:launch_cinematic_2()
  local x_hero, y_hero, layer_hero = hero:get_position()
  local effect_entity_1 = map:create_custom_entity({
      name = "effect",
      sprite = "entities/effects/sparkle_small",
      x = x_hero + 8,
      y = y_hero - 24,
      width = 16,
      height = 16,
      layer = layer_hero,
      direction = 0
    })
  local effect_entity_2 = map:create_custom_entity({
      name = "effect",
      sprite = "entities/effects/sparkle_big",
      x = x_hero + 8,
      y = y_hero - 24,
      width = 16,
      height = 16,
      layer = layer_hero + 1,
      direction = 0
    })
  effect_entity_1:set_enabled(false)
  effect_entity_2:set_enabled(false)
  map:start_coroutine(function()
      local options = {
        entities_ignore_suspend = {hero, effect_entity_1, effect_entity_2}
      }
      map:set_cinematic_mode(true, options)
      animation(hero,"pulling_sword_2")
      hero:get_sprite():set_animation("pulling_sword_wait_2")
      effect_entity_1:set_enabled(true)
      effect_entity_2:set_enabled(true)
      wait(3000)
      effect_entity_1:set_enabled(false)
      effect_entity_2:set_enabled(false)
      local map = game:get_map()
      dialog("_treasure.sword.2")
      game:get_item("sword"):set_variant(2)
      map:set_cinematic_mode(false, options)
  end)
end
