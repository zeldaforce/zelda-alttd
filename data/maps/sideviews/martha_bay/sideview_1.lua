-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Map events
map:register_event("on_started", function(map, destination)

  -- Music
  map:init_music()
  -- Entities
  map:init_map_entities()
  -- Sideview
  map:set_sideview(true)

end)

-- Initialize the music of the map
function map:init_music()
  
  audio_manager:play_music("52_fishing_under_the_bridge")

end

-- Initializes entities based on player's progress
function map:init_map_entities()

  magnifying_lens:get_sprite():set_animation("magnifying_lens")
  magnifying_lens:get_sprite():set_direction(11)
  magnifying_lens:set_enabled(false)

end

-- Discussion with Fishman
function  map:talk_to_fishman() 
  fishman:get_sprite():set_animation("talking")
  local item = game:get_item("magnifying_lens")
  local variant = item:get_variant()
  if variant == 11 then
    game:start_dialog("maps.sideviews.martha_bay.sideview_1.fishman_2", function(answer)
      if answer == 1 then
        map:launch_cinematic_1()
      else 
        game:start_dialog("maps.sideviews.martha_bay.sideview_1.fishman_4")
      end
    end)
  elseif variant > 11 then
    game:start_dialog("maps.sideviews.martha_bay.sideview_1.fishman_6")
  else  
    game:start_dialog("maps.sideviews.martha_bay.sideview_1.fishman_1")
  end

end

-- NPCs events
function fishman:on_interaction()

  map:talk_to_fishman()

end

-- Cinematics

-- This is the cinematic in which the hero open dungeon 3 with tail key
function map:launch_cinematic_1()

  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {boat, fishman, hero, magnifying_lens}
    }
    map:set_cinematic_mode(true, options)
    fishman:get_sprite():set_animation("walking")
    dialog("maps.sideviews.martha_bay.sideview_1.fishman_3")
    wait(3000)
    fishman:get_sprite():set_animation("fishing")
    wait(3000)
    dialog("maps.sideviews.martha_bay.sideview_1.fishman_5")
    wait(1000)
    magnifying_lens:set_enabled(true)
    audio_manager:play_sound("hero/jump")
    local movement1 = sol.movement.create("circle")
    movement1:set_center(fishman)
    movement1:set_radius(32)
    movement1:set_radius_speed(100)
    movement1:set_angle_from_center(math.pi)
    movement1:set_ignore_suspend(true)
    movement1:set_clockwise(true)
    movement1:set_duration(350)
    movement(movement1, magnifying_lens)
    magnifying_lens:set_enabled(false)
    hero:start_treasure("magnifying_lens", 12, "magnifying_lens_12")
    map:set_cinematic_mode(false, options)
    map:init_music()
  end)

end
