-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")

-- Variables
local map = ...
local game = map:get_game()
local manbo_pals = { manbo_pal_1, manbo_pal_2, manbo_pal_3, manbo_pal_4, manbo_pal_5, manbo_pal_6, manbo_pal_7 }
local oh_offset = {
  [hero] = {x = 16, y = -16},
  [manbo_pal_1] = {x = -16, y = -8},
  [manbo_pal_2] = {x = -16, y = -8},
  [manbo_pal_3] = {x = -16, y = -8},
  [manbo_pal_4] = {x = -16, y = -8},
  [manbo_pal_5] = {x = -16, y = -8},
  [manbo_pal_6] = {x = -16, y = -8},
  [manbo_pal_7] = {x = -16, y = -8}
}

-- Start a standalone oh effect on the given position, that will be removed once duration reached.
local function start_oh_effect(entity, duration)

  local x, y, layer = entity:get_position()
  local entity = map:create_custom_entity({
      sprite = "entities/symbols/oh",
      x = x + oh_offset[entity].x,
      y = y + oh_offset[entity].y,
      layer = layer + 1,
      width = 24,
      height = 32,
      direction = 0
  })
  entity:set_drawn_in_y_order()

  entity:get_sprite():set_animation("normal")
  sol.timer.start(entity, duration or 300, function()
    if entity:exists() then
      entity:remove()
    end
  end)

  return entity
end

-- Start a sprite animation and direction.
local function start_pals_animation(animation, direction)

  for _, pal in ipairs(manbo_pals) do
    local sprite = pal:get_sprite()
    sprite:set_animation(animation)
    sprite:set_direction(direction)
  end
end

-- Starts Manbo's mambo.
local function start_manbos_mambo(on_finished_callback)

  local manbo_sprite = manbo:get_sprite()
  hero:freeze()
  hero:set_direction(hero:get_direction4_to(manbo))
  audio_manager:stop_music()

  map:start_coroutine(function()
    map:set_cinematic_mode(true)
    game:set_suspended(false) -- Workaround: Don't use the game suspension of the cinematic mode.
    hero:get_sprite():set_animation("stopped_swimming_scroll")

    -- Introduction.
    wait(1500)
    audio_manager:play_music("49_manbo_mambo")
    start_pals_animation("facing", 0)
    wait(3400)
    manbo_sprite:set_animation("angry")
    start_oh_effect(hero, 600)
    wait(600)

    -- Dancing left.
    manbo_sprite:set_animation("default")
    start_pals_animation("dancing", 2)
    wait(4200)
    manbo_sprite:set_animation("rolling_eyes", function()
      manbo_sprite:set_animation("default")
    end)
    start_pals_animation("facing", 2)
    wait(300)

    -- Dancing right.
    start_pals_animation("dancing", 0)
    wait(5000)
    manbo_sprite:set_animation("rolling_eyes", function()
      manbo_sprite:set_animation("default")
    end)
    start_pals_animation("facing", 0)
    wait(300)

    -- Dancing left and double transition.
    start_pals_animation("dancing", 2)
    wait(5000)
    manbo_sprite:set_animation("rolling_eyes", function()
      manbo_sprite:set_animation("default")
    end)
    start_pals_animation("facing", 0)
    wait(300)
    start_pals_animation("dancing", 0)
    wait(300)
    start_pals_animation("facing", 0)
    wait(300)
    start_pals_animation("dancing", 0)
    wait(300)

    -- Dancing left again.
    start_pals_animation("dancing", 2)
    wait(2900)

    -- Finish
    manbo_sprite:set_animation("rolling_eyes", function()
      manbo_sprite:set_animation("angry")
    end)
    start_pals_animation("turning", 0)
    wait(1200)
    start_oh_effect(hero, 700)
    for _, pal in ipairs(manbo_pals) do
      start_oh_effect(pal, 700)
    end
    start_pals_animation("fins_up", 0)
    wait(700)

    audio_manager:play_music("18_cave")
    manbo_sprite:set_animation("default")
    start_pals_animation("default", 2)
    map:set_cinematic_mode(false)
    on_finished_callback()
  end)
end

-- Starts Manbo dialog.
local function start_manbo_dialog()

  game:start_dialog("maps.sideviews.manbos_cave.manbo_ocarina", function(answer)
    if answer == 1 then
      if game:has_item("ocarina") then
        game:start_dialog("maps.sideviews.manbos_cave.manbo_accepted", function()
          start_manbos_mambo(function()
            hero:start_treasure("melody_2")
          end)
        end)
      else
        game:start_dialog("maps.sideviews.manbos_cave.manbo_no_ocarina")
      end
    else
      game:start_dialog("maps.sideviews.manbos_cave.manbo_refused")
    end
  end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

  -- Sideview
  map:set_sideview(true)

  -- Music
  audio_manager:play_music("18_cave")
end

-- Start Manbo interaction on approaching.
function manbo_sensor:on_activated()

  -- Don't interact if the song is already possessed.
  if game:has_item("melody_2") then
    return
  end

  start_manbo_dialog()
end

-- Talk to Manbo after ocarina played.
function manbo:on_interaction()

  if game:has_item("melody_2") then
    game:start_dialog("maps.sideviews.manbos_cave.manbo_ocarina_played")
  else
    start_manbo_dialog()
  end
end