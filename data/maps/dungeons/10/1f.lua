-- Variables
local map = ...
local game = map:get_game()
local is_small_boss_active = false -- Todo small boss implementation
local is_boss_active = false -- Todo boss implementation
local flow_states = {lava = true, gel = true}

-- Identifiers
local map_chest = "dungeon_10_map_chest"
local map_chest_variable = "dungeon_10_map"
local compass_chest = "dungeon_10_compass_chest"
local compass_chest_variable = "dungeon_10_compass"
local boss_key_chest = ""
local small_key_1_chest = ""
local small_key_2_chest = ""
local small_key_3_chest = "dungeon_10_small_key_chest_3"
local small_key_3_chest_variable = "dungeon_10_small_key_chest_3"
local evil_tile_group = "evil_tile_group"
local lava_lever_name = "lava_lever"
local gel_lever_name = "gel_lever"
local solid_lava_tile_name = "tile_solid_lava"

-- Include scripts
require("scripts/multi_events")
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local enemy_manager = require("scripts/maps/enemy_manager")
local light_manager = require("scripts/maps/light_manager")
local separator_manager = require("scripts/maps/separator_manager")
local switch_manager = require("scripts/maps/switch_manager")
local treasure_manager = require("scripts/maps/treasure_manager")
local flying_tile_manager = require("scripts/maps/flying_tile_manager")

---Map init
---@param destination solarus.entity.destination
function map:on_started(destination)
  -- Doors
  door_manager:open_when_torches_lit(map, "torch_1", "door_ghost_1")
  door_manager:open_when_enemies_dead(map, "keese_1",  "door_keese_1")
  door_manager:open_when_enemies_dead(map, "maskass_1",  "door_maskass_1")
  door_manager:open_when_enemies_dead(map, "blob_1", "door_blob_1")

  -- Enemies
  enemy_manager:set_weak_boo_buddies_on_torch_lit(map, "torch_1", "boo_buddies_1")

  -- Light
  light_manager:init(map)

  -- Music
  game:play_dungeon_music()

  -- Puzzles
  map:init_all_puzzles()
  
  -- Separators
  separator_manager:init(map)

  -- Treasures
  treasure_manager:appear_chest_when_enemies_dead(map, "hardhat_1", compass_chest)
  treasure_manager:appear_chest_when_enemies_dead(map, "helmasaur_1", map_chest)
  treasure_manager:appear_chest_when_flying_tiles_dead(map, evil_tile_group .. "_enemy", small_key_3_chest)
end

---Init all puzzles in the dungeon
---@private
function map:init_all_puzzles()
  map:init_doors()
  map:init_pipes()
  map:init_chests()
  map:handle_solid_lava()
end

---Init all doors in the dungeon
---@private
function map:init_doors()
  map:set_doors_open("door_keese_1")
  map:set_doors_open("door_horse_puzzle")
  map:set_doors_open("door_miniboss")
end

---Init pipe puzzle in the dungeon
---@private
function map:init_pipes()
  -- Default values
  if game:get_value("dungeon_10_lava_is_flowing") == nil then
    game:set_value("dungeon_10_lava_is_flowing", true)
  end
  if game:get_value("dungeon_10_gel_is_flowing") == nil then
    game:set_value("dungeon_10_gel_is_flowing", true)
  end

  if game:get_value("dungeon_10_lava_is_flowing") ~= true then
    map:pipe_disable_flow("lava")
    map:get_entity(lava_lever_name):get_sprite():set_direction(1)
  else
    map:pipe_enable_flow("lava")
    map:get_entity(lava_lever_name):get_sprite():set_direction(0)
  end

  if game:get_value("dungeon_10_gel_is_flowing") ~= true then
    map:pipe_disable_flow("gel")
    map:get_entity(gel_lever_name):get_sprite():set_direction(1)
  else
    map:pipe_enable_flow("gel")
    map:get_entity(gel_lever_name):get_sprite():set_direction(0)
  end
end

function map:init_chests()
  if game:get_value(compass_chest_variable) ~= true then
    map:get_entity(compass_chest):set_enabled(false)
  end
  if game:get_value(map_chest_variable) ~= true then
    map:get_entity(map_chest):set_enabled(false)
  end
  if game:get_value(small_key_3_chest_variable) ~= true then
    map:get_entity(small_key_3_chest):set_enabled(false)
  end
end

---
---@private
function map:handle_solid_lava()
  if flow_states["gel"] == true and flow_states["lava"] == true then
    for wall in map:get_entities("solid_lava_wall") do
      wall:set_enabled(false)
    end 
    map:get_entity(solid_lava_tile_name):set_enabled(true)
  else
    for wall in map:get_entities("solid_lava_wall") do
      wall:set_enabled(true)
    end 
    map:get_entity(solid_lava_tile_name):set_enabled(false)
  end
end

---Enable pipe flow of the given type
---@private
function map:pipe_enable_flow(type)
  for pipe in map:get_entities("pipe_" .. type) do
    pipe:get_sprite():set_paused(false)
  end

  flow_states[type] = true

  for pond in map:get_entities(type .. "_pond") do
    pond:set_enabled(true)
  end
  for fall in map:get_entities(type .. "_fall") do
    fall:set_enabled(true)
  end

  map:get_game():set_value("dungeon_10_" .. type .. "_is_flowing", true)
  map:handle_solid_lava()
end

---Disable pipe flow of the given type
---@private
function map:pipe_disable_flow(type)
  for pipe in map:get_entities("pipe_" .. type) do
    pipe:get_sprite():set_paused(true)
  end

  flow_states[type] = false

  for pond in map:get_entities(type .. "_pond") do
    pond:set_enabled(false)
  end
  for fall in map:get_entities(type .. "_fall") do
    fall:set_enabled(false)
  end

  map:get_game():set_value("dungeon_10_" .. type .. "_is_flowing", false)
  map:handle_solid_lava()
end

lava_lever:register_event("on_activated", function()
  if map:get_entity(lava_lever_name):get_sprite():get_direction() == 0 then
    map:pipe_enable_flow("lava")
  else
    map:pipe_disable_flow("lava")
  end
end)

gel_lever:register_event("on_activated", function()
  if gel_lever:get_sprite():get_direction() == 0 then
    pipe_enable_flow("gel")
  else
    pipe_disable_flow("gel")
  end
end)

sensor_keese_1:register_event("on_activated", function()
  door_manager:close_if_enemies_not_dead(map, "keese_1", "door_keese_1")
end)

sensor_blob_1:register_event("on_activated", function()
  door_manager:close_if_enemies_not_dead(map, "blob_1", "door_blob_1")
end)

sensor_open_blob_1:register_event("on_activated", function()
  map:set_doors_open("door_blob_1")
end)

switch_1:register_event("on_activated", function()
  map:open_doors("door_switch_1")
  audio_manager:play_sound("misc/secret1")
end)

sensor_switch_1:register_event("on_activated", function()
  if not switch_1:is_activated() then
    map:close_doors("door_switch_1")
  end
end)

sensor_open_switch_1:register_event("on_activated", function()
  map:set_doors_open("door_switch_1")
end)

sensor_6:register_event("on_activated", function()
  map:set_doors_open("door_group_6_")
end)

sensor_7:register_event("on_activated", function()
  map:close_doors("door_group_7_")
end)

flying_tile_sensor:register_event("on_activated", function()
  if flying_tile_manager.is_launch == false then
    flying_tile_manager:launch(map, evil_tile_group)
  end
end)

flying_tile_sensor_2:register_event("on_activated", function()
  flying_tile_manager:reset(map, evil_tile_group)
end)

miniboss_sensor:register_event("on_activated", function()
  map:close_doors("door_miniboss")
end)

miniboss_switch:register_event("on_activated", function()
  map:open_doors("door_miniboss")
  miniboss_sensor:set_enabled(false)
end)