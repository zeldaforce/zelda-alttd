-- Variables
local map = ...
local game = map:get_game()

-- Include scripts
local audio_manager = require("scripts/audio_manager")

-- Map events
function map:on_started()

  -- Music
  map:init_music()
  -- Sideview
  map:set_sideview(true)

  if game:get_value("dungeon_4_small_key_5_appeared") and pickable_small_key_1 ~= nil then 
      pickable_small_key_1:set_enabled(true)
  end
end

-- Initialize the music of the map
function map:init_music()

  audio_manager:play_music("20_sidescrolling")

end