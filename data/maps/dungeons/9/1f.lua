-- Variables
local map = ...
local separator = ...
local game = map:get_game()
local is_small_boss_active = false
local is_boss_active = false

-- Include scripts
local audio_manager = require("scripts/audio_manager")
local door_manager = require("scripts/maps/door_manager")
local enemy_manager = require("scripts/maps/enemy_manager")
local separator_manager = require("scripts/maps/separator_manager")
local switch_manager = require("scripts/maps/switch_manager")
local treasure_manager = require("scripts/maps/treasure_manager")
require("scripts/multi_events")

-- Map events
function map:on_started()

  --Chests
  treasure_manager:appear_chest_if_savegame_exist(map, "chest_beak_of_stone",  "dungeon_9_beak_of_stone")
  treasure_manager:appear_chest_when_enemies_dead(map, "enemy_group_6_", "chest_beak_of_stone")
  treasure_manager:appear_chest_if_savegame_exist(map, "chest_map",  "dungeon_9_map")
  treasure_manager:appear_chest_if_savegame_exist(map, "chest_compass",  "dungeon_9_compass")
  treasure_manager:appear_chest_when_enemies_dead(map, "enemy_group_7_", "chest_map")
  treasure_manager:appear_chest_when_switch_eyes_activated(map, "switch_eye_group_1_", "chest_compass")
  -- Doors
  map:set_doors_open("door_group_1", true)
  map:set_doors_open("door_group_2", true)
  map:set_doors_open("door_group_3", true)
  map:set_doors_open("door_group_5", true)
  map:set_doors_open("door_group_6", true)
  map:set_doors_open("door_group_7", true)
  map:set_doors_open("door_group_8", true)
  map:set_doors_open("door_group_small_boss", true)
  map:set_doors_open("door_group_boss", true)
  door_manager:open_when_enemies_dead(map,  "enemy_group_1_",  "door_group_1")
  door_manager:open_when_enemies_dead(map,  "enemy_group_2_",  "door_group_2")
  door_manager:open_when_enemies_dead(map,  "enemy_group_3_",  "door_group_3")
  door_manager:open_when_enemies_dead(map,  "enemy_group_5_",  "door_group_5")
  door_manager:open_when_enemies_dead(map,  "enemy_group_9_",  "door_group_8")
  door_manager:open_weak_wall_if_savegame_exist(map, "weak_wall_group_1_", "dungeon_9_weak_wall_group_1_")
  door_manager:open_when_switch_eyes_activated(map,  "switch_eye_group_2_",  "door_group_7_")
  -- Separators
  separator_manager:init(map)
  -- Music
  game:play_dungeon_music()
  -- Pickables
  treasure_manager:disappear_pickable(map, "pickable_small_key_1")
  treasure_manager:disappear_pickable(map, "pickable_small_key_3")
  treasure_manager:appear_pickable_when_enemies_dead(map, "enemy_group_8_", "pickable_small_key_3")
  treasure_manager:appear_pickable_when_switch_eyes_activated(map, "switch_eye_group_3_", "pickable_small_key_1")
  treasure_manager:disappear_pickable(map, "heart_container")
  --"Miniboss"
  if game:get_value("dungeon_9_miniboss_2") then sensor_9:set_enabled(false) end
  --Great Fairy gone if we already have the tunic (?)
  if game:get_value("get_tunic") then
    map:set_entities_enabled("great_fairy",false)
    sensor_6:set_enabled(false)
  end
  -- Skeletons
  if game:get_value("dungeon_9_is_opened") then
    local skeleton_1_x, skeleton_1_y = skeleton_1:get_position()
    skeleton_1:set_position(skeleton_1_x - 16, skeleton_1_y)
    local skeleton_2_x, skeleton_2_y = skeleton_2:get_position()
    skeleton_2:set_position(skeleton_2_x + 16, skeleton_2_y)
  end

end

-- Discussion with Skeleton
function  map:talk_to_skeleton(skeleton) 

  if not game:get_value("dungeon_9_is_opened") then
    game:start_dialog("maps.dungeons.9.skeleton_question", function(answer)
      print(answer)
      print(skeleton)
      if answer and skeleton == 2 or not answer and skeleton == 1 then
        game:start_dialog("maps.dungeons.9.skeleton_answer_success")
        map:launch_cinematic_1()
      else
        game:start_dialog("maps.dungeons.9.skeleton_answer_error")
      end
    end)
  end

end


-- Doors
weak_wall_group_1:register_event("on_opened", function()
    
  door_manager:destroy_wall(map, "weak_wall_group_1_")
  
end)

-- Sensors events

sensor_1:register_event("on_activated", function()
  
  door_manager:close_if_enemies_not_dead(map, "enemy_group_1_", "door_group_1")
  
end)

sensor_2:register_event("on_activated", function()
  
  door_manager:close_if_enemies_not_dead(map, "enemy_group_2_", "door_group_2")
  
end)

sensor_3:register_event("on_activated", function()
  
  door_manager:close_if_enemies_not_dead(map, "enemy_group_3_", "door_group_3")
  
end)

sensor_4_1:register_event("on_activated", function()
  
  map:open_doors("door_group_4")
  
end)

sensor_4_2:register_event("on_activated", function()
  
  map:close_doors("door_group_4")
  
end)

sensor_5:register_event("on_activated", function()
  
  map:close_doors("door_group_5")
  
end)

sensor_6:register_event("on_activated", function()
  
  map:close_doors("door_group_6")
  audio_manager:play_music("72_great_fairy_fountain")
  
end)

sensor_7:register_event("on_activated", function()
  
   door_manager:open_if_switch_eyes_activated(map,  "switch_eye_group_2_" , "door_group_7_")
  
end)

sensor_8:register_event("on_activated", function()

    if is_small_boss_active == false then
      is_small_boss_active = true
      enemy_manager:launch_small_boss_if_not_dead(map)
    end

end)

sensor_9:register_event("on_activated", function()
  sensor_9:set_enabled(false)
  enemy_group_9_1:set_enabled(true)
  audio_manager:play_music("21_mini_boss_battle")
  map:close_doors("door_group_8")
  
end)
enemy_group_9_1:register_event("on_dead", function()
  audio_manager:play_music("71_color_dungeon")
end)

sensor_10:register_event("on_activated", function()

  if is_boss_active == false then
    is_boss_active = true
    enemy_manager:launch_boss_if_not_dead(map)
  end

end)

-- Switchs events

function switch_5:on_activated()
  
  audio_manager:play_sound("misc/secret1")
  
  map:open_doors("door_group_5")
  
end

-- NPCs events
function skeleton_1:on_interaction()

  map:talk_to_skeleton(1)

end

function skeleton_2:on_interaction()

  map:talk_to_skeleton(2)

end


--Great Fairy
local tunic_answer
local function fairy_dialog()
  game:start_dialog("maps.dungeons.9.great_fairy.sure",function(answer)
    if answer == 1 then
      hero:start_treasure("tunic",tunic_answer + 1,"get_tunic",function()
        game:start_dialog("maps.dungeons.9.great_fairy.closing_eyes",function()
            local opacity = 0
            local white_surface =  sol.surface.create(320, 256)
            local camera = map:get_camera()
            local surface = camera:get_surface()
            white_surface:fill_color({255, 255, 255})
            map:register_event("on_draw",function()
              white_surface:set_opacity(opacity)
              white_surface:draw(surface)
              opacity = opacity + 1
              if opacity > 255 then
                opacity = 255
              end
            end)
            sol.timer.start(3000, function()
                game:start_dialog("maps.dungeons.9.great_fairy.end", function()
                  hero:teleport("out/b2_graveyard","dungeon_9_exit","fade")
                end)
            end)
        end)
      end)
    else
      game:start_dialog("maps.dungeons.9.great_fairy.repeat",function(answer)
        if answer == 1 then tunic_answer = 1 else tunic_answer = 2 end
        fairy_dialog()
      end)
    end
  end)
end

function great_fairy:on_interaction()
  great_fairy:set_enabled(false)
  game:start_dialog("maps.dungeons.9.great_fairy.welcome", game:get_player_name(), function(answer)
    if answer == 1 then tunic_answer = 1 else tunic_answer = 2 end
      fairy_dialog()
  end)
end


-- Cinematics
-- This is the cinematic in which the dungeon is opened
function map:launch_cinematic_1()
  
  map:start_coroutine(function()
    local options = {
      entities_ignore_suspend = {hero, skeleton_1, skeleton_2}
    }
    map:set_cinematic_mode(true, options)
    wait(1000)
    game:set_value("dungeon_9_is_opened")
    
  end)

end
