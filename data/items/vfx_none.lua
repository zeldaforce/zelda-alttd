-- Lua script of item "console".
-- This script is executed only once for the whole game.

-- Variables
local item = ...

-- Include scripts
local audio_manager = require("scripts/audio_manager")
local effect_manager = require('scripts/maps/effect_manager')

-- Event called when the game is initialized.
function item:on_created()

  item:set_savegame_variable("possession_vfx_none")
  item:set_sound_when_brandished(nil)
end

function item:on_obtaining()

  audio_manager:play_sound("items/fanfare_item_extended")
end

function item:start_effect()

  if item:get_variant() == 0 then
    return
  end
  effect_manager:set_effect(item:get_game(), gb)
  item:get_game():set_value("mode", "snes")
  audio_manager:refresh_music()

end
