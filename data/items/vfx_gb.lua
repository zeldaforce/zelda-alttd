-- Lua script of item "console".
-- This script is executed only once for the whole game.

-- Variables
local item = ...
local game = item:get_game()

-- Include scripts
local audio_manager = require("scripts/audio_manager")
local effect_manager = require('scripts/maps/effect_manager')

-- Event called when the game is initialized.
function item:on_created()

  item:set_savegame_variable("possession_vfx_gb")
  item:set_sound_when_brandished(nil)

end

function item:on_obtaining()
  
  -- Sound
  audio_manager:play_sound("items/fanfare_item_extended")
  game:get_item("vfx_none"):set_variant(1)
        
end

function item:start_effect()

  if item:get_variant() == 0 then
    return
  end
  local gb = require('scripts/maps/gb_effect') -- Workaround: Require here to avoid creating shaders at launch, which would make tests fail because of the --no-video option
  effect_manager:set_effect(item:get_game(), gb)
  item:get_game():set_value("mode", "gb")
  audio_manager:refresh_music()

end
